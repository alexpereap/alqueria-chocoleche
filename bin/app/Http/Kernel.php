<?php

namespace chocoleche\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \chocoleche\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \chocoleche\Http\Middleware\VerifyCsrfToken::class,
        \chocoleche\Http\Middleware\ActiveSeasonMiddleware::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \chocoleche\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \chocoleche\Http\Middleware\RedirectIfAuthenticated::class,
        'select_combo' => \chocoleche\Http\Middleware\GameComboMiddleware::class,
        'user_has_combo' => \chocoleche\Http\Middleware\UserHasCombo::class,
        'pending_gamecode' => \chocoleche\Http\Middleware\PendingGameCode::class,
        'demo_already_played' => \chocoleche\Http\Middleware\DemoMiddleware::class,
        'prev_season_combo' => \chocoleche\Http\Middleware\PreviousSeasonCombo::class,
        'statistics_auth' => \chocoleche\Http\Middleware\statisticsAuth::class
    ];
}
