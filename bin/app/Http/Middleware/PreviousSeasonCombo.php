<?php

namespace chocoleche\Http\Middleware;

use Closure;
use chocoleche\Http\Controllers\SupportController;
use Auth;

class PreviousSeasonCombo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $last_season_user_combos = SupportController::getUserPreviousSeasonCombo( Auth::user()->id );

        if( $last_season_user_combos !== NULL ){

            if( $request->session()->has('go_to_previous_season_combo_selection') === false ){
                $request->session()->put('go_to_previous_season_combo_selection', 'yes');
            }

            // the user has a selection of previous combos
            if( $request->session()->get('go_to_previous_season_combo_selection') == 'yes'  ){

                return redirect( route('select-previous-combo') ) ;

            }
        }

        return $next($request);
    }
}
