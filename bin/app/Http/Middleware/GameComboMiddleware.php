<?php

namespace chocoleche\Http\Middleware;

use Closure;
use chocoleche\models\CurrentSeason;
use chocoleche\Http\Controllers\SupportController;
use Auth;

class GameComboMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $user_combos = SupportController::getCurrentUserCombo( Auth::user()->id );

        // if user hadn't selected the combos
        if( empty($user_combos) ){
            return redirect()->route('select-combo');
        }

        return $next($request);
    }
}
