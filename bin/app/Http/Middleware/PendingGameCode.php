<?php

namespace chocoleche\Http\Middleware;
use chocoleche\models\VwGameCode;
use Closure;
use Auth;
use Crypt;
use chocoleche\Http\Controllers\SupportController;

class PendingGameCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $pending_code_to_use = VwGameCode::whereNested(function($query){

            $query->where('user_id', '=',  Auth::user()->id );
            $query->where('game_id', '=', NULL);

        })->first();

        if( count( $pending_code_to_use ) > 0 ){

            $room_id = SupportController::getRoomId( $pending_code_to_use->code_id );

            return redirect( route('game-start-up', [ $room_id ]) )
            ->with('code_id', $pending_code_to_use->code_id );
        }

        return $next($request);
    }
}
