<?php

namespace chocoleche\Http\Middleware;

use Closure;
use chocoleche\Http\Controllers\SupportController;
use Auth;

class UserHasCombo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_combos = SupportController::getCurrentUserCombo( Auth::user()->id );

        // user already selected combos
        if( !empty( $user_combos ) ){

            return redirect()->route('game-index');
        }

        return $next($request);
    }
}
