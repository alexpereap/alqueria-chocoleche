<?php

namespace chocoleche\Http\Middleware;
session_start();

use Closure;

class statisticsAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !isset( $_SESSION['statistics_access'] ) ){

            return redirect(route('pass-phrase-statistics'));
        }

        return $next($request);
    }
}
