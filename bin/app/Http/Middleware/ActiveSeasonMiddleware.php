<?php

namespace chocoleche\Http\Middleware;

use Closure;
use chocoleche\Http\Controllers\SupportController;

class ActiveSeasonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    protected $except_urls;

    function __construct(){
        $split_url[] = explode('/',route('no-season-landing'));
        $split_url[] = explode('/',route('terms'));
        $split_url[] = explode('/',route('habeas'));
        $split_url[] = explode('/',route('weekly-winners'));
        $split_url[] = explode('/',route('prizes'));

        foreach( $split_url as $s ){
            $this->except_urls[] = end( $s );
        }
    }

    public function handle($request, Closure $next)
    {
        // there is no season activated, so redirects to info landing
        if( empty( SupportController::getCurrentSeason() ) && !in_array( $request->path(),  $this->except_urls )  ){

            return redirect( route('no-season-landing') );
        }

        return $next($request);
    }
}
