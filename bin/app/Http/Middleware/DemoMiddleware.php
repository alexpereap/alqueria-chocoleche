<?php

namespace chocoleche\Http\Middleware;

use Closure;
use Request;
use chocoleche\models\DemoIp;

class DemoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $ip = Request::getClientIp();
        $demo_ip = DemoIp::find($ip);

        if( $demo_ip === NULL ){

            $DemoIp = new DemoIp;
            $DemoIp->id = $ip;
            $DemoIp->save();
        } else {

            return redirect( route('home') );
        }

        return $next($request);
    }
}
