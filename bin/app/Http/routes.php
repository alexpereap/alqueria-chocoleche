<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', array(
    'as'   => 'home',
    'uses' => 'LandingController@index'
));


Route::get('registro', array(
    'as'   => 'register',
    'uses' => 'Auth\AuthController@index'
));

Route::get('rodolfo', array(
    'as'   => 'register-rodolfo',
    'uses' => 'Auth\AuthController@index'
));

Route::post('registro', array(
    'as'         => 'handle_register',
    'uses'       => 'Auth\AuthController@handleRegister',
));

Route::get('iniciar-sesion', array(
    'as'   => 'login',
    'uses' => 'Auth\AuthController@login'
));

Route::post('iniciar-sesion', array(
    'as'   => 'handle_login',
    'uses' => 'Auth\AuthController@handleLogin'
));

Route::post('recuperar-contrasena', [
    'as'   => 'pass-recover',
    'uses' => 'Auth\AuthController@passRecover'
]);

Route::get('cambio-de-temporada', array(
    'as'   => 'no-season-landing',
    'uses' => 'LandingController@noSeason'
));

Route::get('contacto', [

    'as'   => 'contact',
    'uses' => 'LandingController@contact'
]);

Route::post('contacto', [

    'as'   => 'handle-contact',
    'uses' => 'LandingController@handleContact'
]);

Route::get('ranking', [

    'as'   => 'ranking',
    'uses' => 'LandingController@ranking'
]);

// @middleware demo_already_played => if user already played the demo redirects to game index
Route::get('demo', [
    'as'         => 'demo',
    'uses'       => 'Game\GameController@demo',
    'middleware' => 'demo_already_played'
]);

Route::get('game-core-demo', [

    'as'   => 'game-core-demo',
    'uses' => 'Game\GameController@gameCoreDemo'
]);

Route:get('thegame_demo/game/prizes', function(){
    return redirect( route('prizes') );
});

Route::get('como-jugar',[
    'as'   => 'mechanics',
    'uses' => 'LandingController@mechanics'
]);

Route::get('show-composite/{user_id}', [
    'as' => 'show-composite',
    'uses' => 'Game\GameController@showComposite',
]);

Route::group(array('middleware' => 'auth' ), function(){

    Route::post('thegame/game/game-save',[
        'as'         => 'game-save',
        'uses'       => 'Game\GameController@gameSave',
    ]);

    Route::get('thegame/game/score-saved',function(){

        return redirect( route('game-result') );
    });
});

Route::get('premios',[
    'as'   => 'prizes',
    'uses' => 'LandingController@prizes'
]);

Route::get('ganadores-semanales',[
    'as'   => 'weekly-winners',
    'uses' => 'LandingController@WeeklyWinners'
]);

Route::get('terminos',[
    'as'   => 'terms',
    'uses' => 'LandingController@terms'
]);

Route::get('habeas-data',[
    'as'   => 'habeas',
    'uses' => 'LandingController@habeas'
]);

Route::get('registro-exitoso',[
    'as'   => 'thanku-page',
    'uses' => 'Game\GameController@thanksForParticipating'
]);

Route::get('statistics',[
    'as'         => 'statistics',
    'uses'       => 'LandingController@statistics',
    'middleware' => 'statistics_auth'
]);

Route::get('pass-phrase-statistics', [
    'as'   => 'pass-phrase-statistics',
    'uses' => 'LandingController@passPhraseStatistics'
]);

route::post('statistics',[
    'as'   => 'post-statistics',
    'uses' => 'LandingController@postStatistics'
]);

/*
 * Game Routes - only auth allowed
 */
Route::group(array('prefix' => 'juego', 'middleware' => 'auth' ), function(){


    /*
     * @Middleware: select_combo => prevents user to access the game if they hadn't selected a combo
     */
    Route::group(['middleware' => 'select_combo'], function(){

        /*
         * @Middleware: pending_gamecode => if the user inserted a code but yet hadn't played with it redirects to game startup
         */
        Route::group(['middleware' => 'pending_gamecode'], function(){

            Route::get('/', array(
                'as'   => 'game-index',
                'uses' => 'Game\GameController@index'
            ));

            Route::post('ingresar-codigo',[
                'as'   => 'insert-code',
                'uses' => 'Game\GameController@handleCodeInsert'
            ]);

            Route::get('bienvenido', [
                'as'   => 'login-welcome',
                'uses' => 'Game\GameController@loginWelcome'
            ]);

        });

        Route::get('inicio-de-juego/{room_id}', array(
            'as'   => 'game-start-up',
            'uses' => 'Game\GameController@gameStartUp'
        ));

        Route::post('game-validation-bridge', [
            'as'   => 'game-validation-bridge',
            'uses' => 'Game\GameController@gameValidationBridge'
        ]);

        Route::get('juego-en-curso', [
            'as' => 'game-on',
            'uses' => 'Game\GameController@gameOn'
        ]);


        Route::get('test-composite',[
            'as'   => 'test-composite',
            'uses' => 'Game\GameController@testComposite'
        ]);

        Route::get('resultado-de-juego',[
            'as'   => 'game-result',
            'uses' => 'Game\GameController@gameResult'
        ]);

        Route::get('game-core', [

            'as'   => 'game-core',
            'uses' => 'Game\GameController@gameCore'
        ]);

        Route::get('mis-premios',[
            'as'   => 'my-prizes',
            'uses' => 'Game\GameController@myPrizes'
        ]);

        Route::get('node-control-comunicator', [
            'as'   => 'node-control-comunicator',
            'uses' => 'Game\GameController@nodeControlComunicator'
        ]);

    });

    /*
     * @Middleware: user_has_combo => if user has combo for the season redirect to game index
     */
    Route::group(['middleware' => 'user_has_combo'], function(){

        /*
         * @Middleware: prev_season_combo => if user has combo for prev season redirects to
         * select-previous-combo where it has the option for selecting the same prev season combo
         */
        Route::group(['middleware' => 'prev_season_combo'], function(){

            Route::get('seleccionar-combo', [
                'as'         => 'select-combo',
                'uses'       => 'Game\GameController@selectGameCombo',
            ]);

            Route::post('seleccionar-combo', [
                'as'   => 'save-game-combo',
                'uses' => 'Game\GameController@saveGameCombo'
            ]);
        });

        Route::post('set-previous-combo', [
            'as'   => 'set-previous-combo',
            'uses' => 'Game\GameController@setPreviousCombo'
        ]);

        Route::get('seleccionar-combo-anterior', [
            'as'   => 'select-previous-combo',
            'uses' => 'Game\GameController@selectPreviousCombo'
        ]);
    });



    Route::get('logout', [
        'as'   => 'logout',
        'uses' => 'Auth\AuthController@getLogout'
    ]);

});

Route::group(array('prefix' => 'markup'), function()
{
    Route::get('/', function() {
        return View('site.mobile.index');
    });

    Route::get('login', function() {
        return View('site.mobile.login');
    });

    Route::get('register', function() {
        return View('site.mobile.register');
    });

    Route::get('contact', function() {
        return View('site.mobile.contact');
    });

    Route::get('ranking', function() {
        return View('site.mobile.ranking');
    });

    Route::get('mechanics', function() {
        return View('site.mobile.mechanics');
    });

    Route::get('selectprize', function() {
        return View('site.mobile.selectprize');
    });

    Route::get('prize', function() {
        return View('site.mobile.prize');
    });

    Route::get('allprize', function() {
        $directoryAventure = './assets/mobile/img/prize/aventure/';
        $directoryMusic = './assets/mobile/img/prize/music/';
        $directoryTech = './assets/mobile/img/prize/tech/';

        $data = array(
            "aventure" => File::allFiles( $directoryAventure ),
            "music" => File::allFiles( $directoryMusic ),
            "tech" => File::allFiles( $directoryTech ),
        );
        return View('site.mobile.allprize', compact('data'));
    });

    Route::get('code', function() {
        return View('site.mobile.code');
    });

    Route::get('ready', function() {
        return View('site.mobile.ready');
    });

    Route::get('endgame', function() {
        return View('site.mobile.endgame');
    });

    Route::get('week', function() {
        return View('site.mobile.week');
    });

    Route::get('winners', function() {
        return View('site.mobile.winners');
    });

    Route::get('terms_prev', function() {
        return View('site.mobile.terms');
    });

    Route::get('typage', function() {
        return View('site.mobile.typage');
    });

    Route::get('endtemp', function() {
        return View('site.mobile.endtemp');
    });
});


Route::group(array('prefix' => 'desktop'), function()
{
    Route::get('register', function() {
        return View('site.desktop.register');
    });

    Route::get('contacto', function() {
        return View('site.desktop.contacto');
    });

     Route::get('codigos', function() {
        return View('site.desktop.codigos');
    });

     Route::get('ranking', function() {
        return View('site.desktop.ranking');
    });

     Route::get('comojugar', function() {
        return View('site.desktop.comojugar');
    });

      Route::get('terminos', function() {
        return View('site.desktop.terminos');
    });

      Route::get('ganadores', function() {
        return View('site.desktop.ganadores');
    });
       Route::get('horajugar', function() {
        return View('site.desktop.horajugar');
    });
        Route::get('premios', function() {
        return View('site.desktop.premios');
    });

        Route::get('login', function() {
        return View('site.desktop.login');
    });

        Route::get('gameover', function() {
        return View('site.desktop.gameover');
    });

Route::get('premioslog', function() {
        return View('site.desktop.premioslog');
    });

Route::get('siguientesemana', function() {
        return View('site.desktop.siguientesemana');
    });


Route::get('tanks', function() {
        return View('site.desktop.tanks');
    });
Route::get('fueratemporada', function() {
        return View('site.desktop.fueratemporada');
    });

Route::get('habeas', function() {
        return View('site.desktop.habeasdata');
    });

});