<?php

namespace chocoleche\Http\Controllers\Auth;

use chocoleche\User;
use Validator;
use chocoleche\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

use chocoleche\Http\Requests;
use Auth;
use Jenssegers\Agent\Agent;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->Agent = new Agent();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function index(){

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return view('site.mobile.register');
        } else{
            return view('site.desktop.register');
        }
    }

    public function handleRegister(Request $request){

        $referer = explode("/",$_SERVER['HTTP_REFERER']);
        $referer = end($referer);

        try{

            // looks for existent username
            $user = User::where('username','=', $request->input('username') )->first();

            if( !empty( $user ) ){

                $result = array(
                    'success' => false,
                    'msg'     => 'Nombre de usuario no disponible',
                    'error'   => 'unavailable-username'
                );

                return response()->json( $result );
            }

            // looks for existent email
            $user = User::where('tutor_email','=', $request->input('tutor_email') )->first();

            if( !empty( $user ) ){

                $result = array(
                    'success' => false,
                    'msg'     => 'Este email ya se encuentra registrado.',
                    'error'   => 'unavailable-email'
                );

                return response()->json( $result );
            }

            // checks empty & valid birthdate
            if( strtotime($request->input('birthdate')) === false ){
                $result = array(
                    'success' => false,
                    'msg'     => 'La fecha de nacimiento es invalida',
                    'error'   => 'invalid-birthdate'
                );

                return response()->json( $result );
            }

            // if doesn't find a user - creates a new one
            $user = new User;

            $user->name = $request->input('name');
            $user->lastname = $request->input('lastname');
            $user->birthdate = $request->input('birthdate');
            $user->tutor_email = $request->input('tutor_email');
            $user->tutor_phone = $request->input('tutor_phone');
            $user->username = trim($request->input('username'));

            if( $referer == 'rodolfo' ){
                $user->from_school = 'rodolfo';
            }

            $user->save();



        } catch( \Illuminate\Database\QueryException $e ){

            $result = array(
                'success' => false,
                'msg'     => 'Un error ha ocurrido, por favor intenta de nuevo más tarde',
                'error_msg'   => $e,
                'error' => 'unknown'
            );

            return response()->json( $result );
        }

        try{
            // autenticates recently created user
            Auth::login($user);

            $result = array(
                'success'  => true,
                'game_uri' => route('thanku-page')
            );

            return response()->json( $result );
        } catch(Exception $e) {

            $result = array(
                'success' => false,
                'msg'     => 'Un error ha ocurrido, por favor intenta de nuevo más tarde',
                'error'   => $e
            );

            return response()->json( $result );
        }


    }

    public function login(){

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.login');
        } else {
            return View('site.desktop.login');
        }
    }

    public function handleLogin(){

        $data = (object) $_POST;

        $user = User::whereNested(function($query) use ($data){

            $query->where('username' , '=', trim($data->username));
            $query->where('birthdate' , '=', trim($data->birthdate));

        })->first();

        if( count($user) > 0 ){

            Auth::login($user);
            // return redirect( route('game-index') );
            return redirect( route('login-welcome') );
        }

        return redirect( route('login') )->with('status','Nombre de usuario o contraseña incorrectos');

    }

    public function getLogout(Request $request){

        Auth::logout();
        $request->session()->flush();
        return redirect( route('home') );
    }

    public function passRecover(Request $request){

        $user = User::where('tutor_email', '=', $request->input('email') )->first();

        if( $user === NULL ){

            $result = array(
                'success'    => false,
                'feedback'   => 'No hemos encontrado este correo eléctronico registrado'
            );
            return response()->json( $result );
        }

        $mail_data = array(
            'username'  => $user->username,
            'birthdate' => $user->birthdate,
            'user_mail' => $user->tutor_email
        );

        $r = Mail::send( 'emails.recover_pass', ['data' => (object) $mail_data] , function($m) use ($mail_data){
            $m->to( $mail_data['user_mail'] )
            ->from('noreply@alqueria.com', 'Alqueria Mezcla Perfecta')
            ->subject('Alqueria chocoleche mezcla perfecta - Recuperación de contraseña');
        });

        $result = array(
            'success'    => true,
            'feedback'   => 'Te hemos envíado un correo eléctronico con tus datos. Revisa tu bandeja de entrada y vuelve para seguir participando por fabulosos premios!'
        );
        return response()->json( $result );
    }

}