<?php

namespace chocoleche\Http\Controllers\Game;

use Illuminate\Http\Request;

use chocoleche\Http\Requests;
use chocoleche\Http\Controllers\Controller;
use chocoleche\Http\Controllers\SupportController;
use chocoleche\models\GamePrize;
use chocoleche\models\UserCombo;
use chocoleche\models\GameCode;
use chocoleche\models\Game;
use chocoleche\User;
use Auth;
use Crypt;
use DB;
use Jenssegers\Agent\Agent;
use QrCode;
use File;
use Response;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    private $Agent;
    private $max_shoot_score = 700;
    private $max_shoots = 94; // 19 magazines per 5 shots each one (minus one shot for hitting the recharge target)

    function __construct(){
        $this->Agent = new Agent();

    }

    public function index(){
        // select demo or game

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.code');
        } else {
            return View('site.desktop.codigos');
        }

    }

    // every season the game combo changes
    public function selectGameCombo(){

      // gets a previous season combo if exists

      $last_season_user_combos = SupportController::getUserPreviousSeasonCombo( Auth::user()->id );

      if( !empty( $last_season_user_combos ) ){

          $data['allow_set_previous_combo'] = true;
          // route : set-previous-combo
      }

       $data['tech_prizes'] = GamePrize::whereNested(function($query){

            $query->where('active', '=', 'yes');
            $query->where('category', '=', 'tech');
       })->orderBy('order','asc')->get();

       $data['adventure_prizes'] = GamePrize::whereNested(function($query){

            $query->where('active', '=', 'yes');
            $query->where('category', '=', 'adventure');
       })->orderBy('order','asc')->get();

       $data['music_prizes'] = GamePrize::whereNested(function($query){

            $query->where('active', '=', 'yes');
            $query->where('category', '=', 'music');
       })->orderBy('order','asc')->get();

       $data['user_has_combo'] = SupportController::userHasPrize(Auth::user()->id);

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.selectprize', $data);
        } else {
            return View('site.desktop.premios', $data);
        }

    }

    public function saveGameCombo(Request $request){

      if( SupportController::userHasPrize(Auth::user()->id) === TRUE ){

          return redirect( route('game-index') );
      }


        // User selected combos bulk insertion
        $data = array(
            array('fk_user_id' => Auth::user()->id, 'fk_prize_id' => $request->input('tech_prize'), 'created_at' => date('Y-m-d H:i:s') ),
            array('fk_user_id' => Auth::user()->id, 'fk_prize_id' => $request->input('adventure_prize'), 'created_at' => date('Y-m-d H:i:s') ),
            array('fk_user_id' => Auth::user()->id, 'fk_prize_id' => $request->input('music_prize'), 'created_at' => date('Y-m-d H:i:s') ),
        );

        UserCombo::insert($data);

        // saves img composite
        $file_name = SupportController::createPrizesCompositeImages( Auth::user()->id );
        $User = User::find( Auth::user()->id );

        $User->combo_composite_img = $file_name;
        $User->save();
        return redirect( route('game-index') );

    }

    public function selectPreviousCombo(){

        $combo = array();
        foreach( SupportController::getUserPreviousSeasonCombo( Auth::user()->id ) as $c ){
            $combo[] = GamePrize::find($c->fk_prize_id)->name;
        }

        $combo = implode(', ', $combo);

        $data = array(
            'current_season' => SupportController::getCurrentSeason(),
            'combo'          => $combo
        );


        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.week', $data);
        } else {
            return View('site.desktop.siguientesemana', $data);
        }
    }

    public function setPreviousCombo(Request $request){

      /*
       * no = keep combo -- auto set combo
       * yes = change combo, go to combo page selection
       */

      if( $request->input('set_prev_combo') == 'no' ){

          $request->session()->put('go_to_previous_season_combo_selection', 'no');

          $last_season_user_combos = SupportController::getUserPreviousSeasonCombo( Auth::user()->id );

          if( $last_season_user_combos === NULL ){

              return redirect( route('select-combo') );
          }

          $data = array();

          foreach( $last_season_user_combos as $c ){

              $data[] = array('fk_user_id' => Auth::user()->id, 'fk_prize_id' => $c->fk_prize_id , 'created_at' => date('Y-m-d H:i:s') );
          }

          UserCombo::insert($data);

          return redirect( route('game-index') );

      } else if( $request->input('set_prev_combo') == 'yes' ){

          $request->session()->put('go_to_previous_season_combo_selection', 'no');
          return redirect( route('select-combo') );
      }


    }

    public function handleCodeInsert(Request $request){

      // validates that the hour is a valid hour
      if( strtotime($request->input('hour')) === false ){
          return redirect( route('game-index') )->with('status','Por favor ingresa una fecha valida');
      }

      if( checkdate($request->input('month'), $request->input('day'), $request->input('year')) === false ){
        return redirect( route('game-index') )->with('status','Por favor ingresa una fecha valida');
      }

      $the_code_date = date('Y-m-d H:i:s', strtotime( $request->input('year') . '-' . $request->input('month') . '-' . $request->input('day') . ' ' .
        $request->input('hour') ));


        $filter_data = arraY(
            'request' => $request,
            'the_code_date' => $the_code_date
        );

        // checks if code exists for this user
        /*$game_code = GameCode::whereNested( function($query) use ($filter_data){

            $the_code_date = $filter_data['the_code_date'];
            $request = $filter_data['request'];

            $query->where('code_date', '=', $the_code_date );
            $query->where('code_batch', '=', trim($request->input('code_batch')) );
            $query->where('fk_user_id', '=', Auth::user()->id );

        })->first();

        if( !empty( $game_code ) ){

            return redirect( route('game-index') )->with('status','Ya has utilizado este código');
        }*/

        $GameCode = new GameCode;
        $GameCode->fk_user_id = Auth::user()->id;
        $GameCode->code_batch = trim($request->input('code_batch'));
        $GameCode->code_date = $the_code_date;

        $GameCode->save();
        $room_id = SupportController::getRoomId( $GameCode->id );

        return redirect( route('game-start-up', [ $room_id ]) )->with('code_id', $GameCode->id );
    }

    public function gameStartUp( $room_id ){

        if( session('code_id') === NULL ){

            return redirect( route('game-index') );
        }

        $node_domain = SupportController::getNodeDomain();

        $data = array(
            'qrcode'               => QrCode::size(155)
                                      ->margin(0)
                                      ->generate( $node_domain . '?id=' . $room_id ),
            'room_id'              => $room_id,
            'node_domain'          => $node_domain,
            'node_comunicator_url' => $node_domain . 'cross-domain-comunicator?id=' . $room_id
        );


        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){

            return response()->view('site.mobile.ready')
                             ->header('Cache-Control', 'private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0' )
                             ->header('Pragma','no-cache')
                             ->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');
        } else {

            return response()->view('site.desktop.horajugar', $data)
                             ->header('Cache-Control', 'private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0' )
                             ->header('Pragma','no-cache')
                             ->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');

        }
    }

    public function gameValidationBridge(Request $request){

        $code_id = Crypt::decrypt( $request->input('code_id') );

        // checks code exists
        $game_code = GameCode::find($code_id);

        if( $game_code === NULL ){

            return redirect( route('game-index') );
        }

        // check the code id hadn't be used for another game
        $existent_game = Game::where('fk_game_code_id', '=', $code_id )->first();

        if( !empty( $existent_game ) ){

           return redirect( route('game-index') );
        }

        // saves game attempt
        $Game = new Game;

        $Game->fk_user_id      = Auth::user()->id;
        $Game->fk_game_code_id = $code_id;
        $Game->save();

        switch( $request->input('type_of_gameplay') ){

            case 'mouse-control':
            default:
              // redirect to game page with temporal data to allow gaming
              return redirect( route('game-on') )->with('game_id', $Game->id );
              break;

            case 'cellphone-control':

              $rank = SupportController::getUserRanking( Auth::user()->id )->rank;
              $rank = $rank > 100 ? '100+' : $rank;
              $nickname = Auth::user()->username;
              $prizes_combo = route('show-composite', [ Auth::user()->id ] );

              $request->session()->put('current_game_id', $Game->id );
              return redirect( SupportController::getNodeDomain() . '/game?id=' . $request->input('room_id')
                . '&nickname=' . $nickname . '&rank=' . $rank . '&prizes_combo=' . $prizes_combo
                . '&comunicator_url=' . route('node-control-comunicator')
              );
              break;

        }


    }

    public function gameOn(Request $request){

      // the game hasn't a game id setted. redirects to game index
      if( session( 'game_id' ) === NULL ){

          return redirect( route('game-index') );
      }
      $data = array(
          'is_ios' => !$this->Agent->isTablet() && $this->Agent->isMobile() && $this->Agent->isSafari()
      );

      $request->session()->put('current_game_id', session( 'game_id' ) );

      return response()->view('site.thegame_iosfixed', $data)
                             ->header('Cache-Control', 'private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0' )
                             ->header('Pragma','no-cache')
                             ->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');

    }

    public function gameSave(Request $request){
        // game save db interaction

      $headers = getallheaders();

       if( strtolower($headers['X-Requested-With']) == 'xmlhttprequest' && $request->input('score') <= $this->getMaxPossibleScore() ){

          // retrieves and deletes session current game id value
          $game_id = $request->session()->pull('current_game_id');

          $Game = Game::find( $game_id );

          if( $Game->score_updated == 'no' ){

              $Game->score = intval( $request->input('score') );
              $Game->score_updated = 'yes';
              $Game->perfect_hits = intval( $request->input('perfect_hits') );
              $Game->ip_number = $_SERVER['REMOTE_ADDR'];

              if( $this->Agent->isMobile() ){
                  $Game->mobile_device = $this->Agent->device();
              }

              $Game->save();
          }

          return "ok";

      } else {

          return "good try!";
      }

    }

    public function gameResult(Request $request){

        $data = array(
            'user_rank' => SupportController::getUserRanking( Auth::user()->id )
        );

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.endgame', $data);
        } else {
            return View('site.desktop.gameover', $data);
        }
    }

    public function gameCore(){

        $rank = SupportController::getUserRanking( Auth::user()->id )->rank;
        $rank = $rank > 100 ? '100+' : $rank;

        $data = array(
            'nickname'     => Auth::user()->username,
            'rank'         => $rank,
            'prizes_combo' => asset('assets/composites/' . Auth::user()->combo_composite_img )
        );

        return view('site.gamecore', $data);
    }

    public function demo(){

        return view('site.thegame_demo');
    }

    public function gameCoreDemo(){
        return view('site.gamecoredemo');
    }

    public function getMaxPossibleScore(){
        return $this->max_shoot_score * $this->max_shoots;
    }

    public function myPrizes(){


      $prizes =  SupportController::getCurrentUserCombo( Auth::user()->id );
      $data['prizes'] = array();

      foreach( $prizes as $p ){
        $data['prizes'][] = GamePrize::find( $p->fk_prize_id );
      }

      if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.prize', $data);
        } else {
            return View('site.desktop.premioslog', $data);
        }
    }

    public function thanksForParticipating(Request $request){

        // prevents the user visit this page twice generating fake statistics
        // commented because client need to check the conversion codes by checking source code
        // wich generates a new server request and a distinct response

        /*if( $request->session()->has('show-thanks') === FALSE ){
            $request->session()->put('show-thanks', 1 );
        } else {

            return redirect( route('game-index') );
        }*/

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return SupportController::responseWithNoCache('site.mobile.typage' );
        } else {
            return SupportController::responseWithNoCache('site.desktop.tanks' );
        }
    }

    public function loginWelcome(Request $request){

        $current_season = SupportController::getCurrentSeason();
        $remaining_time = strtotime( $current_season->end_date ) - time();
        $remaining_days = floor($remaining_time / 3600 / 24);

        $message = $remaining_days > 0 ? "¡Hola! faltan $remaining_days días para que se acabe la semana." : "¡Hola! Hoy es el último día de la semana.";

        $data = array(
            'current_season' => $current_season,
            'message'        => $message
        );

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.login_welcome', $data);
        } else {
            return View('site.desktop.login_welcome', $data);
        }
    }

    public function showComposite( $user_id ){

        $user = User::find( $user_id );

        $path = dirname(__FILE__) . '/../../../../public/assets/composites/' . $user->combo_composite_img;


        if( File::exists($path) ){

            $filetype = File::type( $path );

            $response = Response::make( File::get( $path ) , 200 );

            $response->header('Content-Type', $filetype)
            ->header('Access-Control-Allow-Origin','*');

            return $response;
        }
    }

    public function nodeControlComunicator(Request $request){

        return View('site.node-controller/control-comunicator');
    }
}