<?php

namespace chocoleche\Http\Controllers;

use Illuminate\Http\Request;

use chocoleche\Http\Requests;
use chocoleche\models\CurrentSeason;
use chocoleche\models\UserCombo;
use chocoleche\models\GameSeason;
use chocoleche\Http\Controllers\Controller;
use chocoleche\models\VwCurrentSeasonRanking;
use DB;
use Intervention\Image\ImageManager;
use chocoleche\models\GamePrize;
use chocoleche\User;

class SupportController extends Controller
{
    public static function getCurrentSeason(){
        $current_season = CurrentSeason::all();

        if( !empty( $current_season ) ){
            return $current_season->first();
        } else{
            return null;
        }
    }

    public static function getCurrentUserCombo( $user_id ){

        $data = (object)array(
            'user_id'        => $user_id,
            'current_season' => SupportController::getCurrentSeason()
        );

        if( empty($data->current_season) ){
            return null;
        }

        $user_combos = UserCombo::whereNested(function($query) use ($data) {

            $current_season = $data->current_season;


            $query->where('fk_user_id' , '=', intval($data->user_id));
            $query->where('created_at' , '>=', $current_season->start_date);
            $query->where('created_at' , '<=', $current_season->end_date);

        })->get();

        return count( $user_combos ) == 0 ? null : $user_combos;
    }

    public static function getUserPreviousSeasonCombo( $user_id ){

        // tries to get the previous season

        $last_season = SupportController::getPreviousSeason();

        if( empty( $last_season ) ){
            return null;
        }

        $data = (object)array(
            'user_id'     => $user_id,
            'last_season' => $last_season
        );

        // retrieves user combo for the last season
        $user_combos = UserCombo::whereNested(function($query) use ($data) {

            $last_season = $data->last_season;


            $query->where('fk_user_id' , '=', intval($data->user_id));
            $query->where('created_at' , '>=', $last_season->start_date);
            $query->where('created_at' , '<=', $last_season->end_date);

        })->get();

        return count( $user_combos ) == 0 ? null : $user_combos;

    }

    public static function getPreviousSeason(){

        $current_season = SupportController::getCurrentSeason();

        $last_season = GameSeason::where('end_date','<', $current_season->start_date )
        ->orderBy('end_date', 'desc')
        ->first();

        return $last_season;
    }

    public static function getUserRanking( $user_id ){

        $user_ranking = VwCurrentSeasonRanking::where('user_id', '=', $user_id)->first();
        $user = User::find( $user_id );

        // builds ranking data if user never played on the current season
        // if user never played in current season it won't appear in the ranking view in the database
        if( $user_ranking === NULL ){
            $user_ranking = (object) array(

                'user_id'   => $user->id,
                'score'     => 0,
                'rank'      => 101,
                'user_name' => $user->name,
                'last_name' => $user->lastname,
                'username'  => $user->username,
            );
        }

        return $user_ranking;

    }

    public static function createPrizesCompositeImages( $user_id ){

        $save_file_name = 'composite_' . $user_id . '.png';

        $user_prizes_combo = SupportController::getCurrentUserCombo( $user_id );
        $prizes_imgs = array();

        foreach( $user_prizes_combo as $c ){

            $prize = GamePrize::find( $c->fk_prize_id );

            $prizes_imgs[$prize->category] = (object)array(
                'mobile_image_composite'  =>  $prize->mobile_image_composite,
                'desktop_image_composite' =>  $prize->desktop_image_composite,
            );
        }

        // set image paths
        $img_1_path = dirname( __FILE__ ) . '/../../../public/assets/prizes/' . $prizes_imgs['tech']->mobile_image_composite;
        $img_2_path = dirname( __FILE__ ) . '/../../../public/assets/prizes/' . $prizes_imgs['adventure']->mobile_image_composite;
        $img_3_path = dirname( __FILE__ ) . '/../../../public/assets/prizes/' . $prizes_imgs['music']->mobile_image_composite;
        $save_path  = dirname( __FILE__ ) . '/../../../public/assets/composites/' . $save_file_name;

        // merge two images
        $manager = new ImageManager();
        $img = $manager->make($img_1_path);
        $img->insert($img_2_path);
        $img->save($save_path);

        // merge third image
        $manager = new ImageManager();
        $img = $manager->make($save_path);
        $img->insert($img_3_path);
        $img->save($save_path);

        return $save_file_name;
    }

    public static function responseWithNoCache( $viewName ){
        return response()->view( $viewName )
                             ->header('Cache-Control', 'private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0' )
                             ->header('Pragma','no-cache')
                             ->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');
    }

    public static function userHasPrize( $user_id ){
        return count( SupportController::getCurrentUserCombo( $user_id ) ) > 0;
    }

    // generates node room id based on current code id - for controling the game from cellphone controller
    public static function getRoomId( $code_id ){
        return intval( time() / $code_id );
    }

    // returns the node server domain
    public static function getNodeDomain(){

        if( $_SERVER['SERVER_NAME'] === 'localhost' ){
            return "http://localhost:3000/";
        }

        return "http://controlchocoleche.com/";
    }

}
