<?php

namespace chocoleche\Http\Controllers;

use Illuminate\Http\Request;

use chocoleche\Http\Requests;
use chocoleche\Http\Controllers\Controller;
use chocoleche\Http\Controllers\SupportController;
use chocoleche\models\VwCurrentSeasonRanking;
use Mail;
use Jenssegers\Agent\Agent;
use Auth;
use chocoleche\models\GamePrize;
use chocoleche\models\VwCurrentSeasonStatistics;


class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    private $Agent;

    function __construct(){
        $this->Agent = new Agent();
    }

    public function index(){

        // return view('site/home', array('title' => 'Inicio') );
        /*$response = "<a href='" . route('register') . "' >Register</a><br/>";
        $response .= "<a href='" . route('login') . "' >Login</a><br/>";
        $response .= "<a href='" . route('game-index') . "' >Game</a><br/>";
        $response .= "<a href='" . route('demo') . "' >Demo</a><br/>";
        $response .= "<a href='" . route('contact') . "' >contact</a><br/>";*/


        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.index');
        } else {

            if( !Auth::check() )
                return View('site.desktop.login');
            else
                return redirect( route('game-index') );
        }
    }

    public function noSeason(){

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.endtemp');
        } else {
            return View('site.desktop.fueratemporada');
        }
    }

    public function contact(){

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.contact');
        } else {
            return View('site.desktop.contacto');
        }
    }

    public function handleContact(Request $request){

        $mail_data = $_POST;

        // var_dump( $_POST );

        $r = Mail::send( 'emails.contact', ['data' => (object) $mail_data] , function($m) use ($mail_data){
            $m->to( 'ccbogota@alqueria.com.co' )
            ->from('noreply@alqueria.com', 'Alqueria Mezcla Perfecta')
            ->subject('Contacto recibido desde chocoleche mezcla perfecta');
        });

        return redirect( route('contact') )->with('show_modal',true)
        ->with('modal_txt', '¡Gracias por contactarnos! Pronto estaremos dándo respuesta a tu inquietud.');

    }

    public function ranking(){

        // publc ranking
        if( Auth::check() === false ){

            $data['season_ranking'] = VwCurrentSeasonRanking::take(5)
            ->orderBy('rank', 'asc')
            ->get();
        } else {

            $user_ranking = SupportController::getUserRanking( Auth::user()->id );
            $ranks_to_take = 5;

            if( $user_ranking->rank > 5 ){
                $data['user_ranking'] = $user_ranking;
                $ranks_to_take = 4;
            }

            $data['season_ranking'] = VwCurrentSeasonRanking::take( $ranks_to_take )
            ->orderBy('rank', 'asc')
            ->get();
        }

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.ranking', $data);
        } else {
            return View('site.desktop.ranking', $data);
        }

    }

    public function mechanics(){

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.mechanics');
        } else {
            return View('site.desktop.comojugar');
        }

    }

    public function experimentalIframe(){

        return View('test.experimental_iframe');
    }

    public function prizes(){

        $data['tech_prizes'] = GamePrize::whereNested(function($query){

            $query->where('active', '=', 'yes');
            $query->where('category', '=', 'tech');
        })->orderBy('order','asc')->get();

        $data['adventure_prizes'] = GamePrize::whereNested(function($query){

            $query->where('active', '=', 'yes');
            $query->where('category', '=', 'adventure');
        })->orderBy('order','asc')->get();

        $data['music_prizes'] = GamePrize::whereNested(function($query){

            $query->where('active', '=', 'yes');
            $query->where('category', '=', 'music');
        })->orderBy('order','asc')->get();

        if( Auth::check() ){
            $data['user_has_combo'] = SupportController::userHasPrize(Auth::user()->id);
        }

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){

            return View('site.mobile.allprize', $data);

        } else {
            return View('site.desktop.premios', $data);
        }

    }

    public function WeeklyWinners(){
        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.winners');
        } else {
            return View('site.desktop.ganadores');
        }
    }

    public function terms(){

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.terms');
        } else {
            return View('site.desktop.terminos');
        }
    }

    public function habeas(){

        if( $this->Agent->isMobile() && !$this->Agent->isTablet() ){
            return View('site.mobile.habeas');
        } else {
            return View('site.desktop.habeasdata');

        }
    }

    public function statistics(){

        $data = array(
            'statistics' => VwCurrentSeasonStatistics::all()
        );

        return View( 'site.desktop.statistics', $data );
    }

    public function passPhraseStatistics(){

        return View( 'site.desktop.pass-phrase-statistics' );
    }

    public function postStatistics(Request $request){

        session_start();

        if( $request->input('password') !== 'Wunderman2015' ){

            return redirect( route('pass-phrase-statistics') )->with('status','Wrong password');
        } else {

            $_SESSION['statistics_access'] = TRUE;
            return redirect( route('statistics') );
        }
    }

}
