<?php

namespace chocoleche\models;

use Illuminate\Database\Eloquent\Model;

class GameSeason extends Model
{
    //
    protected $table = 'tbl_game_season';
}
