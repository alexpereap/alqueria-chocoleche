<?php

namespace chocoleche\models;

use Illuminate\Database\Eloquent\Model;

class VwGameCode extends Model
{
    //
    protected $table = 'vw_game_code';
}
