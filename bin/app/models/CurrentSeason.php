<?php

namespace chocoleche\models;

use Illuminate\Database\Eloquent\Model;

class CurrentSeason extends Model
{
    //
    protected $table = 'vw_current_season';
}
