<?php

namespace chocoleche\models;

use Illuminate\Database\Eloquent\Model;

class VwCurrentSeasonStatistics extends Model
{
    protected $table = 'vw_current_season_statistics';
}
