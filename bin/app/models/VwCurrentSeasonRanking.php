<?php

namespace chocoleche\models;

use Illuminate\Database\Eloquent\Model;

class VwCurrentSeasonRanking extends Model
{
    //
    protected $table = 'vw_current_season_ranking';
}
