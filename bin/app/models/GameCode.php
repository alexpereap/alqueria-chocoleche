<?php

namespace chocoleche\models;

use Illuminate\Database\Eloquent\Model;

class GameCode extends Model
{
    protected $table = 'tbl_game_code';
}
