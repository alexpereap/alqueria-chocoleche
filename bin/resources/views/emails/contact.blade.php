<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contacto desde alqueria chocoleche mezcla perfecta</title>
</head>
<body>
    <h1>Datos: </h1>
    <p><b>Nombre completo: </b>{{ $data->name }}</p>
    <p><b>Correo electronico: </b>{{ $data->email }}</p>
    <p><b>Teléfono: </b>{{ $data->phone }}</p>
    <p><b>Mensaje: </b>{{ $data->message }}</p>
</body>
</html>