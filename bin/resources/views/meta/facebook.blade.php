<meta property="og:site_name" content="Chocoleche Mezcla Perfecta"/>
    <meta property="og:title" content="Chocoleche Mezcla Perfecta" />
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:description" content="¡Yo participé por mi mezcla perfecta en www.chocoleche.com.co! Pon a prueba tu puntería y gana los premios que más te gusten haciendo clic aquí." />
    <meta property="fb:app_id" content="1670818706484866" />
    <meta property="og:image" content="{{ asset('./assets/general/300x300.jpg') }}" />