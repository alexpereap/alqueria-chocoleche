<!DOCTYPE html>
<html manifest="offline.appcache">
<head>
<base href="{{ route('home') }}/thegame/">
    <meta charset="UTF-8" />

    <!-- This ensures the canvas works on IE9+.  Don't remove it! -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- Standardised web app manifest -->
    <link rel="manifest" href="appmanifest.json" />

    <!-- Allow fullscreen mode on iOS devices. (These are Apple specific meta tags.) -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('./webapp.png') }}">
    <meta name="HandheldFriendly" content="true" />

    <!-- Chrome for Android web app tags -->
    <meta name="mobile-web-app-capable" content="yes" />
    <link rel="icon" href="{{ asset('./favicon.ico?v0.2') }}">

    <title>Perfect Mix Shooter</title>
    <!-- Note: running this exported project from disk may not work exactly like preview, since browsers block some features on the file:// protocol.  Once you've uploaded it to a server, it should work OK. -->

    <style type="text/css">
        body
        {
            background-color: black;
            color: white;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        #side
        {
            height: 0px;
            position: absolute;
            left: 0px;
            right: 0px;
            bottom: 0px;
        }

        #main
        {
            position: absolute;
            left: 0px;
            top: 0px;
            bottom: 0px;
            right: 0px;
        }

        iframe
        {
            border: none;
        }
    </style>
</head>

<body>
    <script src="jquery-2.1.1.min.js"></script>
    <script>

        function launchIntoFullscreen(element) {
          if(element.requestFullscreen) {
            element.requestFullscreen();
          } else if(element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
          } else if(element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
          } else if(element.msRequestFullscreen) {
            element.msRequestFullscreen();
          }
        }


        $("#goFs").click(function(e){
            e.preventDefault();
            launchIntoFullscreen(document.documentElement); // the whole page
        });

    </script>

    <script type="text/javascript">

        var prevent_refresh = true;

        window.onbeforeunload = function(){

            if( prevent_refresh === true ){
                return "Estas seguro que quieres recargar la página. Perderás tu puntaje actual.";
            }
        };

        function endGameRedirect(){

            prevent_refresh = false;

            url = '{{ route("game-result") }}';
            window.location.href = url;
        }
    </script>

    <script>
    var side_size = 0;

    // Issue a warning if trying to preview an exported project on disk.
    (function(){
        // Check for running exported on file protocol
        if (window.location.protocol.substr(0, 4) === "file")
        {
            alert("Exported games won't work until you upload them.");
        }
    })();

    function size_iframe()
    {
        var w = jQuery(window).width();
        var h = jQuery(window).height() - side_size;
        jQuery("iframe").width(w).height(h);
    };

    jQuery(document).ready(size_iframe);
    jQuery(window).resize(size_iframe);

    if (navigator.serviceWorker && navigator.serviceWorker.register)
    {
        // Register an empty service worker to trigger web app install banners.
        navigator.serviceWorker.register("sw.js", { scope: "./" });
    }

    </script>

    <div id="side">

    </div>
    <div id="main">
        <iframe allowfullscreen="true" src="{{ route('game-core') }}" scrolling="no" noresize="noresize" />
    </div>

</body>
</html>