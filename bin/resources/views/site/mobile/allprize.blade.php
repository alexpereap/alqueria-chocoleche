@extends('layouts.mobile.default')


@section('header')

	@include('modules.mobile.header', array('title'=>'Premios'))

@stop

@section('app')

<div class="hero hero-prize">
	<p>Estos son los premios que puedes ganar.<br>!Haz tu mezcla perfecta y animate a participar!</p>
</div>

<div class="section full foot">
	<div class="panel panel-allprize">

		@foreach( $adventure_prizes as $p )
			<div class="panel-item">
				<div class="category">
					<img width="159" src="{{ asset('assets/mobile/img/text-aventure.png') }}" alt="">
				</div>
				<div class="image">
					<img src="{{ asset( 'assets/mobile/img/prize/' . $p->image_presentation_mobile ) }}" alt="">
				</div>
				<div class="title"><span>{{ $p->name }}</span></div>
			</div>
		@endforeach

		@foreach( $music_prizes as $p )
			<div class="panel-item">
				<div class="category">
					<img width="159" src="{{ asset('assets/mobile/img/text-music.png') }}" alt="">
				</div>
				<div class="image">
					<img src="{{ asset( 'assets/mobile/img/prize/' . $p->image_presentation_mobile ) }}" alt="">
				</div>
				<div class="title"><span>{{ $p->name }}</span></div>
			</div>
		@endforeach

		@foreach( $tech_prizes as $p )
			<div class="panel-item">
				<div class="category">
					<img width="159" src="{{ asset('assets/mobile/img/text-tech.png') }}" alt="">
				</div>
				<div class="image">
					<img src="{{ asset( 'assets/mobile/img/prize/' . $p->image_presentation_mobile ) }}" alt="">
				</div>
				<div class="title"><span>{{ $p->name }}</span></div>
			</div>
		@endforeach
		
	</div>
</div>


@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>

@stop


@section('overlay')

	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))

@stop