@extends('layouts.mobile.default', array('classWrapper' => 'device-768', 'classApp' => 'extended' ))


@section('header')

	@include('modules.mobile.header', array('title'=>'Premios por los que participas'))

@stop

@section('app')

<div class="section extended foot">
	<div class="page-title">Esta es tu mezcla perfecta, no pares de participar hasta conseguirlos</div>
	<div class="panel panel-prize">
		@foreach( $prizes as $p )
		<div class="panel-col">
			<div class="image">
				<img src="{{ asset('assets/prizes/' . $p->image_mobile ) }}" alt="">
			</div>
			<div class="text">
				{{ $p->name }}
			</div>
		</div>
		@endforeach
		<div class="panel-disclaimer">*Imágenes de referencia</div>
	</div>
	<div class="btn-group">
		<a class="btn btn-image btn-back" href="{{ URL::previous() }}" >
			<img width="265" src="{{ asset('assets/mobile/img/btn-back.png') }}" alt="">
		</a>
		<a href="{{ route('prizes') }}" class="btn btn-image btn-more">
			<img width="265" src="{{ asset('assets/mobile/img/btn-more.png') }}" alt="">
		</a>
	</div>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>

@stop

@section('overlay')

	@include('modules.mobile.overlay', array('classOverlay'=> 'block-portrait'))

@stop