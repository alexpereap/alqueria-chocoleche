@extends('layouts.mobile.default', array('classWrapper' => 'device-768', 'classApp' => 'extended' ))


@section('header')

	@include('modules.mobile.header', array('title'=>'Ingresa tus códigos'))

@stop

@section('app')

<div class="section extended foot overlayPage">
	<div class="page-title">
		Ingresa tus códigos para comenzar
		<div class="small"><span class="link overlayPageAction">¿Dónde encuentro el código?</span></div>
		<div>{{ session('status') }}</div>
	</div>
	<form id="submitCodeForm" action="{{ route('insert-code') }}" class="form form-code" method="POST" >
		<div class="group-form">
			<div class="form-select">
				<div class="form-text" id="dateText">Día</div>
				<div class="form-icon">
					<div class="align-middle"></div>
					<i class="fa fa-chevron-down"></i>
				</div>
				<div class="form-drop">
					<input required name="day" type="hidden" id="codeDay" >
					@for($i = 1; $i < 32 ; $i++)
					<div class="form-option" data-value="{{ $i }}">{{ $i }}</div>
					@endfor
				</div>
			</div>
			<div class="form-select">
				<div class="form-text">Mes</div>
				<div class="form-icon">
					<div class="align-middle"></div>
					<i class="fa fa-chevron-down"></i>
				</div>
				<div class="form-drop">
					<input required name="month" type="hidden" id="codeMonth" >
					@for($i = 1; $i < 13 ; $i++)
					<div class="form-option" data-value="{{ $i }}">{{ $i }}</div>
					@endfor
				</div>
			</div>
			<div class="form-select">
				<div class="form-text">Año</div>
				<div class="form-icon">
					<div class="align-middle"></div>
					<i class="fa fa-chevron-down"></i>
				</div>
				<div class="form-drop">
					<input required name="year" type="hidden" id="codeYear" >
					@for($i = 2015; $i < 2017; $i++)
					<div class="form-option" data-value="{{ $i }}">{{ $i }}</div>
					@endfor
				</div>
			</div>
		</div>
		<div class="group-form">

			<input required type="text" id="codeBatch" class="form-input" name="code_batch" placeholder="Lote">

			<input required type="text" id="codeHour" name="hour" class="validate-code-hour form-input" placeholder="Hora">

		</div>

		<button class="btn btn-image btn-confirm" type="submit">
			<img width="270" src="{{ asset('assets/mobile/img/btn-confirm.png') }}" alt="">
		</button>
		{!! Form::token() !!}
	</form>
</div>

<div class="section extended foot overlayPage hidden">
	<div class="panel panel-ready">
		<div class="image">
			<img width="460" src="{{ asset('assets/mobile/img/bg-guide.png') }}" alt="">
		</div>
		<div class="text">Debes ingresar los códigos que se encuentran en los empaques, aqui te mostramos cómo.</div>
	</div>
	<button class="btn btn-image btn-back overlayPageAction">
		<img width="330" src="{{ asset('assets/mobile/img/btn-back.png') }}" alt="">
	</button>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>

@stop

@section('overlay')

	@include('modules.mobile.overlay', array('classOverlay'=> 'block-portrait'))

@stop

@section('script')
	<script>
		$(document).ready(function() {
			$(document).click(function(event) {
				$('.form-drop').removeClass('opened');
			});
		});
	</script>
@append