@extends('layouts.mobile.default', array('classWrapper' => 'device-768', 'classApp' => 'extended' ))


@section('header')

	@include('modules.mobile.header', array('title'=>'Escoge tus premios'))

@stop

@section('app')

<div class="section extended foot">
	<div class="page-title">Desliza los premios hacia arriba o abajo hasta crear tu mezcla perfecta y selecciónalos</div>
	<div class="panel panel-selectprize">
		<div class="social-link hidden">
			<img src="{{ asset('assets/mobile/img/social-link.png') }}" alt="">
		</div>
		<div class="panel-col">
			<div class="panel-box panel-item-01">
				<ul class="bxslider">
				 @foreach ($tech_prizes as $p)
					<li class="tech-prizes-list" data-value="{{ $p->id }}" >
						<img width="145" src="{{ asset('assets/prizes/' . $p->image_mobile ) }}" alt="">
					</li>
				@endforeach
				</ul>
				<div class="panel-control">
					<div class="prev"></div>
					<div class="next"></div>
				</div>
			</div>
		</div>
		<div class="panel-col">
			<div class="panel-box panel-item-02">
				<ul class="bxslider">

				@foreach ($adventure_prizes as $p)
					<li class="adventure-prizes-list" data-value="{{ $p->id }}" >
						<img width="145" src="{{ asset('assets/prizes/' . $p->image_mobile ) }}" alt="">
					</li>
				@endforeach

				</ul>
				<div class="panel-control">
					<div class="prev"></div>
					<div class="next"></div>
				</div>
			</div>
		</div>
		<div class="panel-col">
			<div class="panel-box panel-item-03">
				<ul class="bxslider">
					@foreach ($music_prizes as $p)
						<li class="music-prizes-list" data-value="{{ $p->id }}" >
							<img width="145" src="{{ asset('assets/prizes/' . $p->image_mobile ) }}" alt="">
						</li>
					@endforeach
				</ul>
				<div class="panel-control">
					<div class="prev"></div>
					<div class="next"></div>
				</div>
			</div>
		</div>
	</div>

	<form id="selectComboForm" class="form" action="{{ route('save-game-combo') }}" method="POST" >
		<input type="hidden" id="techPrize" name="tech_prize" value="{{ $tech_prizes[0]->id }}" >
		<input type="hidden" id="adventurePrize" name="adventure_prize" value="{{ $adventure_prizes[0]->id }}" >
		<input type="hidden" id="musicPrize" name="music_prize" value="{{ $music_prizes[0]->id }}" >
		<button type="submit" class="btn btn-image btn-select">
			<img width="265" src="{{ asset('assets/mobile/img/btn-select.png') }}" alt="">
		</button>
		{!! Form::token() !!}
	</form>
</div>


@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>

@stop


@section('overlay')

	@include('modules.mobile.overlay', array('classOverlay'=> 'block-portrait'))

@stop

@section('script')
	<script src="{{ asset('assets/mobile/components/bxslider-4/dist/jquery.bxslider.min.js') }}"></script>
	<script>
		$(document).ready(function() {
			$('.panel-item-01 ul').show().bxSlider({
				"mode": "vertical",
				"preventDefaultSwipeY": true,
				"prevSelector": ".panel-item-01 .prev",
				"prevText": "<span class='icon'></span>",
				"nextSelector": ".panel-item-01 .next",
				"nextText": "<span class='icon'></span>",
				"hideControlOnEnd": true
			});
		});
		$(document).ready(function() {
			$('.panel-item-02 ul').show().bxSlider({
				"mode": "vertical",
				"preventDefaultSwipeY": true,
				"prevSelector": ".panel-item-02 .prev",
				"prevText": "<span class='icon'></span>",
				"nextSelector": ".panel-item-02 .next",
				"nextText": "<span class='icon'></span>",
				"hideControlOnEnd": true
			});
		});
		$(document).ready(function() {
			$('.panel-item-03 ul').show().bxSlider({
				"mode": "vertical",
				"preventDefaultSwipeY": true,
				"prevSelector": ".panel-item-03 .prev",
				"prevText": "<span class='icon'></span>",
				"nextSelector": ".panel-item-03 .next",
				"nextText": "<span class='icon'></span>",
				"hideControlOnEnd": true
			});
		});
	</script>
@append

