@extends('layouts.mobile.default')


@section('header')

	@include('modules.mobile.header', array('title'=>'Contáctenos'))

@stop

@section('app')

<div class="hero hero-contact">
	<p>Responderemos todas tus inquietudes en la mayor brevedad posible. Tus preguntes son muy importantes para nosotros.</p>
</div>

<div class="section foot">
	<form class="form form-contact" action="{{ route('handle-contact') }}" method="POST" >
		<div class="control">
			<input required class="validate-alpha" placeholder="Nombre completo" type="text" name="name" id="name">
			<div class="input-msg hidden"><span></span></div>
		</div>
		<div class="control">
			<input required placeholder="Correo Electrónico" type="text" name="email" id="email">
			<div class="input-msg hidden"><span></span></div>
		</div>
		<div class="control">
			<input required class="validate-phone" placeholder="Teléfono" type="text" name="phone" id="phone">
			<div class="input-msg hidden"><span></span></div>
		</div>
		<div class="control">
			<textarea required placeholder="Mensaje" name="message" id="msg" rows="4"></textarea>
		</div>
		<button class="btn btn-image" type="submit">
			<img width="390" src="{{ asset('assets/mobile/img/btn-send.png') }}" alt="">
		</button>

		{!! Form::token() !!}
	</form>
</div>

@stop

@section('wrapperPage')
<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>
@stop

@section('overlay')
	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))
@stop