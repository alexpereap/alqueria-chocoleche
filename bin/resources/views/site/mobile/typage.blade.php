@extends('layouts.mobile.default')


@section('header')

	@include('modules.mobile.header', array('title'=> 'Chocoleche' ))

@stop

@section('app')

<div class="section foot">
	<div class="page-logo">
		<img class="max" width="420" src="{{ asset('assets/mobile/img/bg-letters.png') }}" alt="">
		<img class="min" width="150" src="{{ asset('assets/mobile/img/check-ty.png') }}" alt="">
	</div>
	<div class="page-center">Tu registro ha sido exitoso, gracias por participar.</div>
	<div class="btn btn-image btn-continue">
		<a href="{{ route('mechanics') }}"><img src="{{ asset('assets/mobile/img/btn-continue.png') }}" width="260" alt=""></a>
	</div>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines bg-brown"></div>
</div>

@stop

@section('script')

<!-- Google Code for Convirtieron Choco Juego Ni&ntilde;os OCT 15 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 989393533;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "IcOMCMKijmAQ_eTj1wM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/989393533/?label=IcOMCMKijmAQ_eTj1wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

@append


@section('fb_conversion')
    @include('conversion.fb')
@stop