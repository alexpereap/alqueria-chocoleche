@extends('layouts.mobile.default')

@section('header')

	@include('modules.mobile.header', array('title'=>'Términos y condiciones'))

@stop

@section('app')

<div class="section fold">
	<div class="terms">
		<div class="terms-box">
			<div class="title">LA MECÁNICA DE LA PROMOCIÓN CONSISTE EN:</div>
			<p><strong>1.	Mecánica General:</strong> La mecánica general del juego es la siguiente.</p>
			<blockquote>
				a.	El usuario ingresa a <a href="http://chocoleche.com.co" target="blank">www.chocoleche.com.co</a>  y registra sus datos personales. <br>
				b.	El usuario selecciona el combo por el cual quiere participar. <br>
				c.	Para empezar a participar el usuario debe ingresar un código (el cual se encontrará en los empaques de Chocoleche  y corresponde a la fecha de vencimiento y lote) para obtener 80 oportunidades para disparar.  <br>
				d.	Mientras más códigos registre,  mayores oportunidades de disparo tiene el usuario y así podrá acumular más puntos. <br>
				e.	Cada semana se premiará a los 2 usuarios con el mayor puntaje obtenido durante esa semana. El puntaje será visualizado en un ranking. Cada semana se reinicia el ranking y los puntos obtenidos. Por lo que  al inicio de cada semana los usuarios arrancarán con cero puntos. <br>
				f.	Una vez un usuario es ganador y cumple con los requisitos de entrega, quedará inhabilitado para ganar nuevamente.
			</blockquote>
			<p><strong>2.	Registro:</strong> El usuario deberá llenar los siguientes campos para poder registrarse. Todos los datos son obligatorios</p>
			<blockquote>
				a.	Nombre <br>
				b.	Apellido <br>
				c.	Fecha de nacimiento (día/mes/año) <br>
				d.	Correo electrónico de los padres o acudiente <br>
				e.	Telefono y/o celular de los padres o acudiente <br>
				f.	Nombre de usuario <br>
				g.	Confirmar que los padres o acudiente saben que el usuario está participando en la actividad. <br>
				h.	Aceptar la política de términos y condiciones de la actividad.
			</blockquote>
			<p><strong>3.	Selección de “Combo”:</strong> El usuario encontrara en la plataforma 3 categorías las cuales son: </p>
			<blockquote>
				1. Tecnología en donde se encontraran los siguientes elementos: Iphone 5s, computador Acer mini e3-112, televisor Samsung un32j4300 ak (smart tv), Apple Ipad mini 16 gb wi-fi md 531, y juego consola wii u 32gb con control + juego. <br>
				2. Música en donde se encontraran los siguientes elementos: Bateria drum 22 jbp1103 tom grasso, guitarra electrica fst-122 tom grasso + estuche p/guit. elect. forma/gota adc nal + amplif guitar ph1012 10w qx12 kustom, ipod nano 16gb, audifono sony color negro manos libres peso: 125 g (sin cable) / capacidad: 1000 mw / respuesta en frecuencia (hz): 10–24.000 hz / longitud del cable: 1,2 m / enchufe: minienchufe estéreo en forma de l y bañado en oromini estéreo de cuatro conductores bañado en oro con forma de l / garantia 1 año directamente con centro de servicio ,y parlante x300 wireless mobile speaker (black housing/brown); marca: Logitech. <br>
				3. Aventura en donde se encontraran los siguientes elementos: pistola nerf rebelle  para niña y hasbro  nerf - soa - flashflood 2 para niño, carpa iglú dome 4 personas klimber + sleeping bag con cabezal 180 x 75 cm colores surtidos klimber + colchoneta enrollar impermeable, cancha de basketball elaborada en metal y plástico, bicicleta montaña juvenil-adulto rin 26 y patines en línea freestyle con chasis corto.
			</blockquote>
			<p>El usuario podrá únicamente elegir  un (1) elemento de las categorías señaladas  y así crear el “combo”  que desea. Para lograr ser un posible ganador el usuario debe registrar códigos de Chocoleche durante la vigencia de la promoción y acumular la mayor cantidad de puntos.</p>
			<blockquote>
				a.	Cambio de combo: El usuario solo podrá cambiar su combo en el  siguiente  evento: <br>
				<blockquote>
					i.	Se reinicia el juego: Cada semana que se reinicia el puntaje del juego, el usuario podrá decidir si quiere cambiar su combo o seguir participando por el mismo.
				</blockquote>
			</blockquote>
			<p><strong>4.	Ingreso de códigos:</strong> Los códigos son información que se encuentra en el empaque  del producto y corresponde a la siguiente información: día, mes, año, lote  (fecha de vencimiento y lote). Por cada código ingresado el usuario obtendrá 80 oportunidades para disparar. Al finalizar cada oportunidad de disparo el usuario podrá ingresar todos los códigos que desee con la finalidad de acumular el mayor puntaje y estar en el ranking de los 2 mejores de la semana. El usuario debe guardar los empaques de los códigos que ingresa para poder reclamar su combo en caso de ser un posible ganador.</p>
			<p><strong>5.	Modo de juego:</strong> El juego consiste en obtener la mayor cantidad de puntos disparando un chorro de Chocoleche a los blancos que aparecen en la pantalla. Los dos usuarios con la mayor cantidad de puntos en la semana serán los ganadores y podrán reclamar su combo siempre y cuando cumplan con las condiciones y restricciones de la promoción. A continuación hay un detalle de las variables que tiene el juego:</p>
			<blockquote>
				a.	Puntos: Los puntos son otorgados al pegarle a un blanco. Los blancos tienen un puntaje mínimo de 200  y un puntaje máximo de 700 (Golpe Perfecto) y el puntaje obtenido es definido según qué tan cerca el usuario le pega al centro del blanco. <br>
				b.	Puntos Extras: En cada sesión de juego aparecerán dos blancos con bonificaciones. Los tipos de bonificaciones son: 5 disparos extras o 600 puntos extras; los cuales se otorgan si le pega al centro del blanco.
			</blockquote>
			<p><strong>6.	Finalización Sesión Juegos:</strong> Cuando un usuario se queda sin disparos se acaba la sesión del código ingresado. Para poder seguir jugando, el usuario deberá ingresar otro código nuevo. Al ingresar otro código se iniciará otra partida y podrá seguir aumentando su puntaje. </p>
			<p><strong>7.	Ganadores:</strong> Los posibles ganadores son los dos usuarios que en la semana hayan logrado la mayor cantidad de puntos. Estos usuarios serán contactados a través del e-mail y/o teléfono suyos o de sus padres (en caso de ser menores de edad), para verificar que cumplan con las condiciones y restricciones de la promoción. Si los posibles ganadores cumplen  todos y cada uno de los requisitos, se programa la entrega de los premios, de lo contrario se acumula la cantidad de posibles ganadores para la semana siguiente. Los usuarios deberán presentar los empaques del producto para confirmar los códigos usados. Los usuarios que han ganado un combo queda inhabilitados para ganar pero pueden jugar si lo desean. </p>
			<p><strong>8.	Descalificación:</strong> Se descalificará a un usuario si:</p>
			<blockquote>
				a.	Utiliza un nickname no apropiado (incluyan groserías, connotaciones racistas, sexuales, discriminatorias y cualquier otra expresión que sea ofensiva o inadecuada) <br>
				b.	Utiliza códigos inválidos o no tiene el empaque al presentarlos <br>
				c.	Se comprueba un intento de fraude o hackeo del juego. <br>
				d.	Se sospecha el uso de algún método de participación en la actividad que se realice por cualquier proceso, técnica o mecánica de participación distinta a la detallada en los términos y condiciones. La utilización de técnicas de participación en la actividad de naturaleza robótica, repetitiva, automática, programada, mecanizada, mediante software, sistemas especiales, y otros dispositivos no autorizados, o cualquier técnica similar llevará a la anulación de la participación de que se trate y será causal de  descalificación total y permanente del participante
			</blockquote>
			<div class="title">CONDICIONES PARA PARTICIPAR:</div>
			<p>Para participar en la promoción “MEZCLA LO QUE MAS TE GUSTA Y CHOCOLECHE TE LO REGALA”  el participante debe:</p>
			<blockquote>
				•	Tener autorización previa de los padres  o tutores  si el usuario es menor de edad. <br>
				•	Leer los términos y condiciones de la actividad, en señal de aceptación de los términos.  <br>
				•	Deberá ingresar a la aplicación y seguir los pasos. <br>
				•	Guardar los empaques inscritos para reclamar el premio si llegase a ser ganador.
			</blockquote>
			<div class="title">TERMINOS Y CONDICIONES GENERALES:</div>
			<p>•	Vigencia de la promoción: Promoción Valida desde el 1 de Octubre del 2015 hasta el 30 de Noviembre del 2015, y/o hasta agotar existencias.</p>
			<p>•	Se realizaran premiaciones semanales premiando a los 2 primeros lugares asi:</p>
			<blockquote>
				o	Semana 1: 1 de Octubre del 2015 al 11 de Octubre del 2015. <br>
				o	Premiación Semana 1: 12 de Octubre del 2015  <br>
				o	Semana 2: 12 de Octubre al 18 de Octubre del 2015 <br>
				o	Premiación Semana 2: 19 de Octubre del 2015 <br>
				o	Semana 3: 19 de Octubre al 25 de Octubre del 2015 <br>
				o	Premiación Semana 3: 26 de Octubre del 2015 <br>
				o	Semana 4: 26 de Octubre al 1 de Noviembre del 2015 <br>
				o	Premiación Semana 4: 2 de Noviembre del 2015 <br>
				o	Semana 5: 2 de Noviembre al 8 de Noviembre del 2015 <br>
				o	Premiación Semana 5: 9 de Noviembre del 2015 <br>
				o	Semana 6: 9 de Noviembre al 15 de Noviembre del 2015 <br>
				o	Premiación Semana 6: 16 de Noviembre del 2015 <br>
				o	Semana 7: 16 de Noviembre al 22 de Noviembre del 2015 <br>
				o	Premiación Semana 7: 23 de Noviembre del 2015 <br>
				o	Semana 8: 23 de Noviembre al 29 de Noviembre del 2015 <br>
				o	Premiación Semana 8: 30 de Noviembre del 2015 <br>
			</blockquote>
			<p>•	Cobertura: Aplica a nivel nacional (República de Colombia) en todas las ciudades y municipios donde se comercialice Chocoleche Alquería. Todas las personas que tengan acceso a la página <a href="http://chocoleche.com.co" target="blank">www.chocoleche.com.co</a>  y que se encuentren en territorio colombiano podrán participar.</p>
			<p>•	El usuario no podrá ingresar el mismo código por más de una vez.</p>
			<p>•	El Término de la garantía de los premios que PRODUCTOS NATURALES DE LA SABANA S.A. entregue  será de un día a partir de la fecha en la que sean recibidos por el ganador.  </p>
			<p>•	Un usuario podrá ganar solo una vez, quedando inhabilitado para ganar otro premio.</p>
			<p>•	Los usuarios deberán guardar los empaques, para ser presentados en el momento en que se haga la entrega de los premios (en el caso de ser ganadores). La entrega de los premios se hará únicamente dentro del territorio colombiano, y se hará  a un mayor de edad que puede ser  cualquiera de los padres o tutor en caso de ser menores de edad.  Si no se presentan los empaques de Chocoleche, el participante NO  se hará acreedor del premio. Los premios no son canjeables por dinero ni acumulables con otras promociones.</p>
			<p>•	Esta promoción no es válida para colaboradores de PRODUCTOS NATURALES DE LA SABANA S.A., ni para sus familiares hasta el segundo grado de consanguinidad, primero de afinidad y/o primero civil; tampoco es válida para sus cónyuges y/o compañeros/as permanentes</p>
		</div>
	</div>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
	<div class="bg bg-chocolate bg-large"></div>
	<div class="bg bg-badboys"></div>
	<div class="bg-group">
		<div class="bg bg-target bg-target-01"></div>
		<div class="bg bg-target bg-target-02"></div>
	</div>
	<div class="bg bg-letters">
		<img src="{{ asset('assets/mobile/img/bg-letters.png') }}" alt="">
	</div>
</div>

@stop