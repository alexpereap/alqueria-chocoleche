<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        body{
            padding:0;
            margin:0;
            width: 100%;
            height: 100%;
        }
    </style>
    <script src="{{ asset('./assets/mobile/components/jquery/jquery.min.js') }}"></script>
</head>
<body>
    <iframe id="theGame" style="position: absolute; width:100%; height: 100%; border: none" src="{{ route('home') . '/thegame2?v=' . time() }}" frameborder="0"></iframe>
    <script>
        $(window).load(function(){

            var img_url = '{{ $prizes_combo }}';

            html = '<form>' +
        '<input type="hidden" id="image_path" name="image_path" value="' + img_url + '">' +
        '<input type="hidden" id="nick" name="nick" value="{{ $nickname }}">' +
        '<input type="hidden" id="rank" name="rank" value="{{ $rank }}">' +
    '</form>';

            document.getElementById('theGame').contentWindow.setInitialValues(html);
        });
    </script>

    <script>
        window.onbeforeunload = function(){
            return 'Estas seguro de abandonar el juego?\nSi sales perderás tú puntaje.';
      };
    </script>
</body>
</html>