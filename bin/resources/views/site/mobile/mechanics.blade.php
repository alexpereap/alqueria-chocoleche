@extends('layouts.mobile.default')


@section('header')

	@include('modules.mobile.header', array('title'=>'Mecánica'))

@stop

@section('app')




<div class="hero-block">
	<ul class="bxslider">
		<li>
			<div class="hero mechanics-01">
				<p><span>1.</span> Regístrate ingresando tus datos.</p>
			</div>
		</li>
		<li>
			<div class="hero mechanics-02">
				<p><span>2.</span> Escoge los premios que más te gustan y crea tu mezcla perfecta</p>
			</div>
		</li>
		<li>
			<div class="hero mechanics-03">
				<p><span>3.</span> Ingresa los códigos de los empaques y estos se convertirán en oportunidades de lanzamiento, demuestra tu puntería y juega por tu mezcla perfecta. Tu código equivale a 80 oportunidades.</p>
			</div>
		</li>
		<li>
			<div class="hero mechanics-04">
				<p><span>4.</span> Semanalmente hay 2 posibles ganadores y se reiniciará el puntaje para que tengas la oportunidad de volver a jugar y ganar.</p>
			</div>
		</li>
	</ul>
	<div class="hero-pagination"></div>
</div>

@if( !Auth::check() )
<div class="section fold">
	<div class="btn btn-image btn-skip">
		<a href="{{ route('handle_register') }}"><img width="190" src="{{ asset('assets/mobile/img/btn-skip.png') }}" alt=""></a>
	</div>
</div>
@else
<div class="section fold">
	<div class="btn btn-image btn-skip">
		<a href="{{ route('game-index') }}"><img width="190" src="{{ asset('assets/mobile/img/btn-play.png') }}" alt=""></a>
	</div>
</div>
@endif

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
	<div class="bg bg-chocolate bg-large"></div>
	<div class="bg bg-badboys"></div>
	<div class="bg-group">
		<div class="bg bg-target bg-target-01"></div>
		<div class="bg bg-target bg-target-02"></div>
	</div>
	<div class="bg bg-letters">
		<img src="{{ asset('assets/mobile/img/bg-letters.png') }}" alt="">
	</div>
</div>

@stop

@section('overlay')
	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))
@stop

@section('script')
	<script src="{{ asset('assets/mobile/components/bxslider-4/dist/jquery.bxslider.min.js') }}"></script>
	<script>
		$(document).ready(function() {
			$('.bxslider').show().bxSlider({
				"mode": "fade",
				"pagerSelector": ".hero-pagination"
			});
		});
	</script>
@append