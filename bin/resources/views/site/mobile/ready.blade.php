@extends('layouts.mobile.default', array('classWrapper' => 'device-768' ))


@section('header')

	@include('modules.mobile.header', array('title'=>'¡Es hora de jugar!'))

@stop

@section('app')

<div class="section foot">
	<div class="page-title">Ahora cuentas con:</div>
	<div class="panel panel-ready">
		<div class="image">
			<img width="460" src="{{ asset('assets/mobile/img/bg-play.png') }}" alt="">
		</div>
		<div class="text"><strong>80 oportunidades</strong></div>
	</div>

	<form action="{{ route('game-validation-bridge') }}" method="POST" class="form" >
		<input type="hidden" name="code_id" value="{{ Crypt::encrypt( session('code_id') ) }}" >
		<button class="btn btn-image btn-play">
			<img width="330" src="{{ asset('assets/mobile/img/btn-play.png') }}" alt="">
		</button>
		{!! Form::token() !!}
	</form>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>

@stop