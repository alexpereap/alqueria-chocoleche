@extends('layouts.mobile.default')


@section('header')

	@include('modules.mobile.header', array('title'=>'Registro'))

@stop

@section('app')

<div class="section fold">
	<div class="page-title">Comienza completando los siguientes datos:</div>
	<form id="registerForm" class="form form-register" action="{{ route('handle_register') }}" method="POST">
		<div class="control">
			<input class="validate-alpha" required placeholder="Escribe tu nombre" type="text" name="name" id="name">
		</div>
		<div class="control">
			<input class="validate-alpha" required placeholder="Escribe tu apellido" type="text" name="lastname" id="surname">
		</div>
		<div class="control">
			<input class="make-datepicker" required readonly placeholder="Fecha de nacimiento" type="text" name="birthdate" id="pass">
			<div class="hidden label-place">DD / MM / AA</div>
			<div class="input-msg">
				<span>Está será tu clave de acceso.</span>
			</div>
		</div>
		<div class="control">
			<input class="validate-email" required placeholder="Correo electrónico de uno de los padres" type="text" name="tutor_email" id="email">
			<div class="input-msg">
				<span id="emailError" class="error hidden">Este email ya se encuentra registrado.</span>
			</div>
		</div>
		<div class="control">
			<input class="validate-phone" required placeholder="Teléfono de uno de los padres" type="text" name="tutor_phone" id="phone">
		</div>
		<div class="control">
			<input class="validate-username" required placeholder="Nombre de usuario" type="text" name="username" id="username">
			<div class="input-msg">
				<span id="userNameError" class="error hidden">Este usuario ya existe, escribe uno diferente.</span>
			</div>
		</div>
		<div class="control">
			<label>
				<div class="box-check">
					<!-- no change position of input -->
					<input class="hidden" type="checkbox" name="" id="tutorApproval">
					<div class="box-check-ok">
						<span></span>
					</div>
					<div class="box-text table">
						<div class="table-cell">
							<p>Soy mayor de edad o mis papás saben que estoy participando en este concurso</p>
						</div>
					</div>
				</div>
			</label>
		</div>
		<div class="control">
			<label>
				<div class="box-check">
					<!-- no change position of input -->
					<input class="hidden" type="checkbox" name="" id="terms1">
					<div class="box-check-ok">
						<span></span>
					</div>
					<div class="box-text table">
						<div class="table-cell">
							<p>He leído y acepto los términos y condiciones de la actividad</p>
						</div>
					</div>
				</div>
			</label>
		</div>
		<!--
		<div class="control">
			<label>
				<div class="box-check">
					<input class="hidden" type="checkbox" name="" id="terms2">
					<div class="box-check-ok">
						<span></span>
					</div>
					<div class="box-text table">
						<div class="table-cell">
							<p>
								He leído y acepto la política de uso dedatos y las condiciones de la actividad. Autorizo a Productos Naturales de la Sabana S.A. y a sus filiales a enviarme información vía correo electrónico.
							</p>
						</div>
					</div>
				</div>
			</label>
		</div>
		-->
		<div class="page-text">
			Recuerda escribir los datos correctos. En caso de que resultes ganador nos pondremos en contacto al correo eléctronico de tus papás. *Todos los datos son obligatorios
		</div>
		<button type="submit" class="btn btn-image" >
			<img width="390" src="{{ asset('assets/mobile/img/btn-register.png') }}" alt="">
		</button>
		{!! Form::token() !!}
	</form>
</div>

@stop

@section('wrapperPage')

	<div class="backgrounds">
		<div class="bg bg-lines"></div>
		<div class="bg bg-chocolate bg-large"></div>
		<div class="bg bg-badboys"></div>
		<div class="bg-group">
			<div class="bg bg-target bg-target-01"></div>
			<div class="bg bg-target bg-target-02"></div>
		</div>
		<div class="bg bg-letters">
			<img src="{{ asset('assets/mobile/img/bg-letters.png') }}" alt="">
		</div>
	</div>

@stop

@section('overlay')
	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))
@stop