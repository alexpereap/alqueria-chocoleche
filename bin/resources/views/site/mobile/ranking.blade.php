@extends('layouts.mobile.default', array('classWrapper' => 'device-768', 'classApp' => 'extended' ))


@section('header')

	@include('modules.mobile.header', array('title'=>'Ranking'))

@stop

@section('app')

<div class="section ">
	<div class="page-title">¡Muy bíen! Sigue participando hasta conseguir tu mezcla perfecta</div>
</div>
<div class="table-ranking">
	<div class="table-header">
		<table class="table-wrap">
			<tr>
				<td>Posición</td>
				<td> <i class="fa fa-user"></i> Jugador</td>
				<td> <i class="fa fa-star"></i> Punto</td>
			</tr>
		</table>
	</div>
	<div class="table-body">
	@foreach( $season_ranking as $r )
		<div class="table-item <?php if( Auth::check() === true ){ if( $r->user_id == Auth::user()->id ) echo 'active'; }?> ">
			<table class="table-wrap">
				<tr>
					<td>{{ $r->rank }}</td>
					<td>{{ $r->username }}</td>
					<td>{{ number_format( $r->score ) }}</td>
				</tr>
			</table>
		</div>
	@endforeach

	@if( isset( $user_ranking ) )
		<div class="table-item active">
			<table class="table-wrap">
				<tr>
					<td>{{ $user_ranking->rank > 100 ? '100+' : $user_ranking->rank }}</td>
					<td>{{ $user_ranking->username }}</td>
					<td>{{ number_format( $user_ranking->score ) }}</td>
				</tr>
			</table>
		</div>
	@endif
	</div>
</div>
<div class="section foot">
	<div class="btn btn-image btn-back">
		<a href="{{ URL::previous() }}"><img width="270" src="{{ asset('assets/mobile/img/btn-back.png') }}" alt=""></a>
	</div>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>

@stop

@section('overlay')

	@include('modules.mobile.overlay', array('classOverlay'=> 'block-portrait'))

@stop