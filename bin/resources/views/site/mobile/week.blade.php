@extends('layouts.mobile.default')


@section('header')

	@include('modules.mobile.header', array('title'=> $current_season->name ))

@stop

@section('app')

<div class="hero hero-week">
	<p>¡Hola! Estás en la {{  $current_season->name }} que va del {{ date('Y-m-d', strtotime($current_season->start_date) ) }} al {{ date('Y-m-d', strtotime($current_season->end_date) ) }} y estaremos entregando {{ $combo }}  <br> ¿Deseas cambiar tu combo? </p>
	<div class="btn-group-week">
		<form style="display:inline" class="form" action="{{ route('set-previous-combo') }}" method="POST" >
			<button type="submit" name="set_prev_combo" value="yes" class="btn btn-image">
				<img src="{{ asset('assets/mobile/img/btn-ok.png') }}" alt="">
			</button>
			{!! Form::token() !!}
		</form>
		<form style="display:inline" class="form" action="{{ route('set-previous-combo') }}" method="POST" >
			<button type="submit" name="set_prev_combo" value="no" class="btn btn-image">
				<img src="{{ asset('assets/mobile/img/btn-not.png') }}" alt="">
			</button>
			{!! Form::token() !!}
		</form>
	</div>
</div>

<div class="section foot"></div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>

@stop

@section('overlay')

	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))

@stop