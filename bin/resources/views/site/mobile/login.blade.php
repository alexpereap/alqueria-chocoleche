@extends('layouts.mobile.default', array('classApp' => 'min-device' ))

@section('header')

	@include('modules.mobile.header', array('title'=>'Inicia Sesión'))

@stop

@section('app')



<div class="section foot">
	<div class="page-title">Para comenzar a divertirte ingresa <br> tus datos o regístrate</div>
	<form action="{{ route('handle_login') }}" method="POST" id="loginForm" >
		<div class="form form-login">
			<div class="control">
				<input placeholder="Escribe tu usuario" type="text" name="username" id="name">
			</div>
			<div class="control">
				<input readonly class="make-datepicker" placeholder="Fecha de nacimiento" type="text" name="birthdate" id="pass">
				<div class="label-place hidden">YY-MM-DD</div>
				<div class="input-msg">@if( session('status') !== NULL ) <span class="error" >{{ session('status') }}</span> @endif</div>
			</div>
			<div class="control">
				<div class="btn btn-image sbmt-form" form-target="loginForm" >
					<img src="{{ asset('assets/mobile/img/btn-login.png') }}" width="390" alt="">
				</div>
			</div>
		</div>
		{!! Form::token() !!}

		<div class="page-links row">
			<div class="col-xs-6">
				<a href="{{ route('register') }}"><span>Quiero Regístrarme</span></a>
			</div>
			<div class="col-xs-6">
				<a href="#" id="iForgot" ><span>¿Olvidaste tu usuario?</span></a>
			</div>
		</div>
	</form>

	<form class="hidden form" action="{{ route('pass-recover') }}" method="POST" id="recoverPassForm" >
		<div class="form form-login">
			<div class="control">
				<input required placeholder="Escribe tu email de registro" type="text" name="email" >
			</div>
			<div class="control">
				<button type="submit" class="btn btn-image">
					<img src="{{ asset('assets/mobile/img/btn-send.png') }}" width="390" alt="">
				</button>
			</div>
		</div>
		{!! Form::token() !!}

		<div class="page-links row">
			<div class="col-xs-6">
				<a href="{{ route('register') }}"><span>Quiero Regístrarme</span></a>
			</div>
			<div class="col-xs-6">
				<a href="#" id="iForgot" ><span>Inicia Sesión</span></a>
			</div>
		</div>
	</form>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
	<div class="bg bg-chocolate bg-large"></div>
	<div class="bg bg-badboys"></div>
	<div class="bg-group">
		<div class="bg bg-target bg-target-01"></div>
		<div class="bg bg-target bg-target-02"></div>
	</div>
	<div class="bg bg-letters">
		<img src="{{ asset('assets/mobile/img/bg-letters.png') }}" alt="">
	</div>
</div>

@stop

@section('overlay')
	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))
@stop