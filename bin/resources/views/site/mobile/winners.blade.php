@extends('layouts.mobile.default', array('classWrapper' => 'device-768', 'classApp' => 'extended' ))

@section('style')
	
	<style>
		body .table-winners{
			margin-bottom: 150px;
		}
	</style>

@stop


@section('header')

	@include('modules.mobile.header', array('title'=>'Ranking'))

@stop

@section('app')

<div class="hero hero-winners">
	<p>¡Felicitaciones a nuestros ganadores de semanas anteriores! ¡Qué esperas para ser uno de ellos? </p>
</div>

<div class="table-winners">
	<div class="table-header">

		<table class="table-wrap">
			<tr>
				<td> <i class="fa fa-user"></i> Jugador</td>
				<td> <i class="fa fa-star"></i> Combo</td>
			</tr>
		</table>

	</div>
	<div class="table-body">

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>AMOR8</td>
					<td>Bici, iPhone, Parlante</td>
				</tr>
			</table>
		</div>

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>maléfico</td>
					<td>Bici, Guitarra, Tv</td>
				</tr>
			</table>
		</div>

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>fvidal</td>
					<td>Bateria, Bici, iPad</td>
				</tr>
			</table>
		</div>

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>Cristianv</td>
					<td>Audifonos, Bici, iPhone</td>
				</tr>
			</table>
		</div>

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>ALLO</td>
					<td>Bici, Guitarra, Tv</td>
				</tr>
			</table>
		</div>

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>JAKO</td>
					<td>Bici, Guitarra, Wii U</td>
				</tr>
			</table>
		</div>

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>BOOM</td>
					<td>Bici, Guitarra, iPhone</td>
				</tr>
			</table>
		</div>

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>paternoste</td>
					<td>Bici, Guitarra, iPhone</td>
				</tr>
			</table>
		</div>

		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>chopa95</td>
					<td>Guitarra, iPhone, Patines</td>
				</tr>
			</table>
		</div>
		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>Prodigy</td>
					<td>Bateria, Bici, iPhone</td>
				</tr>
			</table>
		</div>
		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>yeyajaimez</td>
					<td>Bateria, Bici ,iPhone</td>
				</tr>
			</table>
		</div>
		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>MAO7</td>
					<td>Audifonos, Bici, iPhone</td>
				</tr>
			</table>
		</div>
		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>lcortes</td>
					<td>Bateria, Bici, Tv</td>
				</tr>
			</table>
		</div>
		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>DENYFOUR</td>
					<td>Bici, iPhone, iPod</td>
				</tr>
			</table>
		</div>
		<div class="table-item ">
			<table class="table-wrap">
				<tr>
					<td>248565</td>
					<td>Bici, Guitarra, Tv</td>
				</tr>
			</table>
		</div>

	</div>
</div>
<div class="section foot"></div>

@stop


@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>

@stop

@section('overlay')

	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))

@stop