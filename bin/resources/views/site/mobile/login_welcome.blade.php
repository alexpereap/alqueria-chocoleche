@extends('layouts.mobile.default')


@section('header')

    @include('modules.mobile.header', array('title'=> $current_season->name ))

@stop

@section('app')

<div class="hero hero-week">
    <p>{{ $message }} Recuerda que cada semana tu puntaje se reinicia y tienes oportunidades de ganar.</p>
    <div class="btn-group-week">
            <a href="{{ route('game-index') }}" class="btn btn-image">
                <img src="{{ asset('assets/mobile/img/btn-continue.png') }}" alt="">
            </a>
    </div>
</div>

<div class="section foot"></div>

@stop

@section('wrapperPage')

<div class="backgrounds">
    <div class="bg bg-lines"></div>
</div>

@stop

@section('overlay')

    @include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))

@stop