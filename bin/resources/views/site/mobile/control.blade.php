@extends('layouts.mobile.default', array('classScroll' => 'without-head' ))

@section('app')

<div class="section border-top">
	<canvas id="mycanvas" width="350" height="500"></canvas>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
	<div class="bg bg-gun"></div>
	<div class="bg bg-letters">
		<img src="{{ asset('assets/mobile/img/bg-letters.png') }}" alt="">
	</div>
</div>

@stop

@section('overlay')
	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))
@stop

@section('script')
	<script>
		$(document).ready(function() {
			
			var canvas = document.getElementById('mycanvas');
			var ctx = canvas.getContext("2d");
			ctx.fillStyle = "rgba(0,0,0,0)";
			ctx.fillRect(0,0,350,500);
		});
	</script>
@stop