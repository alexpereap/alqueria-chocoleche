@extends('layouts.mobile.default', array('classScroll' => 'without-head', 'classApp' => 'min-device' ))

@section('style')
	<style>
		body #app.min-device{
			min-height: 610px;
		}
		.btn-winners{
		    display: block;
		    width: 292px;
		    height: 47px;
		    margin-left: auto;
		    margin-right: auto;

		    background-repeat: no-repeat;
		    background-position: center center;
		    background-image: url('./assets/mobile/img/btn-winners.png');
		}
	</style>
@endsection

@section('app')

<div class="section">
	<div class="page-hero">
		<img width="420" src="{{ asset('assets/mobile/img/bg-letters.png') }}" alt="">
		<br>
		<img width="420" src="{{ asset('assets/desktop/img/end-title.png') }}" alt="">
	</div>
	<div class="page-center">¡Gracias a todos por participar!</div>
	<a href="{{ route('weekly-winners') }}" class="btn-winners"></a>
</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines bg-brown"></div>
	<div class="bg bg-chocolate bg-large"></div>
	<div class="bg bg-badboys"></div>
	<div class="bg-group">
		<div class="bg bg-target bg-target-01"></div>
		<div class="bg bg-target bg-target-02"></div>
	</div>
</div>

@stop


@section('overlay')
	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))
@stop