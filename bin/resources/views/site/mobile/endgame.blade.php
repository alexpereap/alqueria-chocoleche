@extends('layouts.mobile.default', array('classWrapper' => 'device-768', 'classApp' => 'extended', 'classScroll' => 'without-head' ))


@section('app')

<div class="statistics">
	<div class="social-link">
		<a href=""  ><img id="theShareButton" src="{{ asset('assets/mobile/img/social-link.png') }}" alt=""></a>
	</div>
	<div class="title">Partida finalizada</div>
	<p>No olvides que entre más productos compres, más oportunidades tienes de ganar.</p>
	<div class="statistics-box">
		<span class="hidden statistics-item">
			<i class="icon icon-bullet"></i>
			<strong>X0</strong>
			<span>Oportunidades</span>
		</span>
		<span class="statistics-item">
			<i class="icon icon-user"></i>
			<strong>Posición:</strong>
			<span>{{ $user_rank->rank }}</span>
		</span>
		<span class="statistics-item">
			<i class="icon icon-star"></i>
			<strong>{{ $user_rank->score }}</strong>
			<span>puntos</span>
		</span>
	</div>
	<div class="group-btn-statistics">
		<div class="btn btn-image">
			<a href="{{ route('home') }}"><img src="{{ asset('assets/mobile/img/btn-end.png') }}" alt=""></a>
		</div>
		<div class="btn btn-image">
			<a href="{{ route('game-index') }}"><img src="{{ asset('assets/mobile/img/btn-again.png') }}" alt=""></a>
		</div>
		<div class="btn btn-image">
			<a href="{{ route('ranking') }}"><img src="{{ asset('assets/mobile/img/btn-rank.png') }}" alt=""></a>
		</div>
	</div>
</div>

<div class="section foot"></div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
</div>


@stop

@setion('script')
	<script>
		$(document).ready(function() {
			snapper.disable();
		});
	</script>
@append