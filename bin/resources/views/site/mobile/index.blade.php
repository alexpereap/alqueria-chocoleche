@extends('layouts.mobile.default')

@section('header')

	@include('modules.mobile.header', array('title'=>'Chocoleche'))

@stop

@section('app')

<div class="section foot">
	<div class="page-logo">
		<img width="305" src="{{ asset('assets/mobile/img/logoresp.png') }}" alt="">

	</div>


	<div class="btn-group-home">
		<div class="btn btn-image">
			<p>¡Prueba el juego ya!</p>
			<a href="{{ route('demo') }}"><img src="{{ asset('assets/mobile/img/btn-demo.png') }}" width="390" alt=""></a>
		</div>

		<div class="btn btn-image">
			<p>¿No te has registrado? Haz clic en el botón</p>
			<a href="{{ route('register') }}"><img  src="{{ asset('assets/mobile/img/btn_reg.png') }}" width="390" alt=""></a>
		</div>

		<div class="btn btn-image">
			<p>Si ya te registraste, continúa aquí</p>
			<a href="{{ route('login') }}"><img  src="{{ asset('assets/mobile/img/btn-signin.png') }}" width="390" alt=""></a>
		</div>

		<div class="bg bg-badboys bg-off"></div>
	</div>


</div>

@stop

@section('wrapperPage')

<div class="backgrounds">
	<div class="bg bg-lines"></div>
	<div class="bg bg-chocolate bg-small"></div>
	<div class="bg-group">
		<div class="bg bg-target bg-target-01 bg-off"></div>
		<div class="bg bg-target bg-target-02 bg-off"></div>
	</div>
</div>

@stop

@section('overlay')
	@include('modules.mobile.overlay', array('classOverlay'=> 'block-landscape'))
@stop