 @extends('layouts.desktop.default')


@section('header')

    @include('modules.desktop.header', array('title'=>'codigos'))

@stop

@section('desk_cont_real')

 <div class="al_dis_contenido al_codigos">
                <img src="{{ asset('./assets/desktop/img/al_codes_ti.png') }}" alt="contacto">
                <p>Ingresa tus códigos para comenzar</p>
                <p>{{ session('status') }}</p>
                <form id="submitCodeForm" action="{{ route('insert-code') }}" method="POST" >
                    <div id="al_form_choco">

                        <div class="al_campo_cod">
                            <div class="nselec">
                                <select required name="day">
                                    <option >Día</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>


                            </div>
                            <div class="nselec">
                                <select required name="month">
                                    <option >Mes</option>
                                    <option value="1">Enero</option>
                                    <option value="2">Febrero</option>
                                    <option value="3">Marzo</option>
                                    <option value="4">Abril</option>
                                    <option value="5">Mayo</option>
                                    <option value="6">Junio</option>
                                    <option value="7">Julio</option>
                                    <option value="8">Agosto</option>
                                    <option value="9">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>

                                </select>
                            </div>
                        </div>


                        <div class="al_campo_cod">
                            <input required type="text" name="code_batch" placeholder="Lote">
                            <input required type="text" name="hour" placeholder="Hora" class="validate-code-hour" >
                        </div>

                        <div class="al_campo_cod">
                            <div class="nselec">
                                <select required name="year">
                                    <option>Año</option>                                    
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                </select>
                            </div>
                        </div>

                    </div>



                    <div class="der_img">
                        <img src="{{ asset('./assets/desktop/img/al_codes.png') }}">

                    </div>
                    <p class="p_f_r">Los códigos los encuentras detrás de la bolsa de 200g o delante en el tetra de 225g</p>

                    <div class="centrado">
                        <input type="submit" id="al_eviar_reg">
                    </div>
                    {!! Form::token() !!}
                </form>
            </div>
            @stop
@section('contenido_alerta')
<div class="func_text">
    <h2 style="visibility:hidden;" >partida finalizada</h2>
    <p id="modalTxt" ><!-- js filled content--></p>
    <ul class="savora">
        <li><a href="" id="sendSubmitCodeForm" class="cerrar_alerta" >Continuar</a></li>
    </ul>
</div>
@stop