@extends('layouts.desktop.default')

@section('pantalla_completa',"pantalla_completa")
@section('delete', "delete")

@section('contenido_alerta')


            <div class="imagen"><img src="{{ asset('./assets/desktop/img/admiracion.png') }}"></div>

            <div class="func_text">
                <h2>{{  $current_season->name }}</h2>
                <p>¡Hola! Estás en la {{  $current_season->name }} que va del {{ date('Y-m-d', strtotime($current_season->start_date) ) }} al {{ date('Y-m-d', strtotime($current_season->end_date) ) }} y estaremos entregando {{ $combo }}  <br> ¿Deseas cambiar tu combo? </p>


                <ul class="savora">
                    <li>
                        <form style="display:inline" class="form" action="{{ route('set-previous-combo') }}" method="POST" >
                            <button name="set_prev_combo" value="yes" type="submit" >Si</button>
                            {!! Form::token() !!}
                        </form>
                    </li>
                    <li>
                        <form style="display:inline" class="form" action="{{ route('set-previous-combo') }}" method="POST" >
                            <button name="set_prev_combo" value="no" type="submit" >No</button>
                            {!! Form::token() !!}
                        </form>
                    </li>

                </ul>

            </div>
@stop