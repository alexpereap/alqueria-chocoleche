@extends('layouts.desktop.default')

@section('header')
@endsection

@section('style')
    <style>
        body #al_contenido{
            z-index: 500;
        }
        body #al_contenido > div#logo{
            margin-top: 0;
            padding-top: 35px;
        }
        body section#al_chocoleche{
            min-height: 0;
        }
        body .al_texto{
            z-index: 500;
        }
        .end-title{
            text-align: center;
            margin: 40px auto 25px auto;
        }
        .end-title img{
            position: relative;
            z-index: 500;
            display: block;
            margin: 0 auto;
            height: auto;
            width: 100%;
            max-width: 524px;
        }
        .end-subtitle{
            position: relative;
            z-index: 500;
            font-family: "Gotham-Bold";
            font-size: 20px;
            line-height: 1.2;
            text-align: center;
            display: block;
            color: #301506;
            margin-bottom: 25px;
        }
        .end-fix{
            position: fixed;
            bottom: 85px;
            left: 0;
            right: 0;
            width: 100%;
            height: 352px;
            z-index: 40;
        }
        .end-boys{
            text-align: center;
            margin: 20px auto 15px auto;
        }
        .end-boys img{
            display: block;
            margin: 0 auto;
            height: auto;
            width: 100%;
            max-width: 508px;
        }
        body footer{
            z-index: -1 ;
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            height: auto;
        }
        body .back_leche{
            height: 378px;
        }
        @media only screen and (max-height: 730px) and (min-height: 600px){
            .end-boys{
                opacity: .4;
            }
        }
        @media only screen and (max-height: 600px){
            .end-fix,
            .end-boys{
                display: none;
            }
        }
        .btn-ganadores{
            display: block;
            width: 292px;
            height: 47px;
            margin-bottom: 420px;
            margin-left: auto;
            margin-right: auto;

            background-repeat: no-repeat;
            background-position: center center;
            background-image: url('./assets/desktop/img/btn-ganadores.png');
        }
    </style>
@endsection

@section('auxiliar')

    <div class="end-title">
        <img src="{{ asset('assets/desktop/img/end-title.png') }}" alt="">
    </div>

    <div class="end-subtitle">
        ¡Gracias a todos por participar!
    </div>

    <a href="{{ route('weekly-winners') }}" class="btn-ganadores"></a>

@endsection


@section('footer')

    @include('modules.desktop.footer', array( 'cierre' => true ))

@endsection