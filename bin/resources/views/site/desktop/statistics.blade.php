<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Statistics</title>

    <link rel="stylesheet" href="{{ asset('bootstrap-3.3.5-dist/css/bootstrap.min.css') }}">
    <style>
    body{
        margin-top:50px;
    }
    </style>
</head>
<body>


    <div class="container">
        <blockquote>
            Statistics
        </blockquote>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Rank</th>
                    <th>Username</th>
                    <th>Score</th>
                    <th>Average score</th>
                    <th>Used Codes</th>
                </tr>
            </thead>

            <tbody>
                @foreach( $statistics as $s )
                    <tr>
                        <td>{{ $s->rank }}</td>
                        <td>{{ $s->username }}</td>
                        <td>{{ number_format($s->score) }}</td>
                        <td>{{ number_format(intval($s->average_score)) }}</td>
                        <td>{{ $s->used_codes }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <script src="{{ asset('js/jquery-1.11.3.min.js') }}" ></script>
    <script src="{{ asset('bootstrap-3.3.5-dist/js/bootstrap.min.js') }}" ></script>

</body>
</html>