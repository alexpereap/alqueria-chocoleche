@extends('layouts.desktop.default')


@section('header')

    @include('modules.desktop.header', array('title'=>'Ganadores'))

@stop

@section('desk_cont_real')



            <div class="al_dis_contenido">
                <img src="{{ asset('./assets/desktop/img/al_ganador.png') }}" alt="ganadores">
                <p>¡Felicitaciones a nuestros ganadores! ¿Qué esperas para ser uno de ellos?</p>
            </div>

@stop

@section('auxiliar')

<div id="tabs" >
    <ul class="tabs_semana">
        <li class="sem"><a href="#tabs-1">1 Oct - 11 Oct</a></li>
        <li class="sem"><a href="#tabs-2">12 Oct - 18 Oct</a></li>
        <li class="sem"><a href="#tabs-3">19 Oct - 25 Oct</a></li>
        <li class="sem"><a href="#tabs-4">26 Oct - 1 Nov</a></li>
        <li class="sem"><a href="#tabs-5">2 Nov - 8 Nov</a></li>
        <li class="sem"><a href="#tabs-6">9 Nov - 15 Nov</a></li>
        <li class="sem"><a href="#tabs-7">16 Nov - 22 Nov</a></li>
        <li class="sem"><a href="#tabs-8">23 Nov - 29 Nov</a></li>
    </ul>


    <div id="tabs-1">
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>

                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">AMOR8</td>
                    <td class="tab_rig">{{ number_format( 5798885 ) }}</td>
                    <td></td>
                </tr>


                <tr>
                    <td></td>

                    <td class="tab_cen">maléfico</td>
                    <td class="tab_rig">{{ number_format( 5120291 ) }}</td>
                    <td></td>
                </tr>

                <!--tab-->

            </table>


        </div>
    </div>

    <div id="tabs-2" >
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>

                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">fvidal</td>
                    <td class="tab_rig">{{ number_format( 4799886 ) }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">Cristianv</td>
                    <td class="tab_rig">{{ number_format( 4448562 ) }}</td>
                    <td></td>
                </tr>

                <!--tab-->

            </table>


        </div>
    </div>


    <div id="tabs-3" >
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>

                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">ALLO</td>
                    <td class="tab_rig">{{ number_format( 22104449 ) }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">Jako</td>
                    <td class="tab_rig">{{ number_format( 21189063 ) }}</td>
                    <td></td>
                </tr>

                <!--tab-->

            </table>


        </div>
    </div>

    <div id="tabs-4" >
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>

                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">BOOM</td>
                    <td class="tab_rig">{{ number_format( 13085969 ) }}</td>
                    <td></td>
                </tr>


                <!--tab-->

            </table>


        </div>
    </div>

    <div id="tabs-5" >
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>

                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">paternoste</td>
                    <td class="tab_rig">{{ number_format( 18422437 ) }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">chopa95</td>
                    <td class="tab_rig">{{ number_format( 17342722 ) }}</td>
                    <td></td>
                </tr>


                <!--tab-->

            </table>


        </div>
    </div>

    <div id="tabs-6" >
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>

                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">Prodigy</td>
                    <td class="tab_rig">{{ number_format( 10497594 ) }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">yeyajaimez</td>
                    <td class="tab_rig">{{ number_format( 6377455 ) }}</td>
                    <td></td>
                </tr>


                <!--tab-->

            </table>


        </div>
    </div>

    <div id="tabs-7" >
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>

                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">MAO7</td>
                    <td class="tab_rig">{{ number_format( 20502383 ) }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">lcortes</td>
                    <td class="tab_rig">{{ number_format( 20437082 ) }}</td>
                    <td></td>
                </tr>


                <!--tab-->

            </table>


        </div>
    </div>

    <div id="tabs-8" >
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>

                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">DENYFOUR</td>
                    <td class="tab_rig">{{ number_format( 35315180 ) }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>

                    <td class="tab_cen">248565</td>
                    <td class="tab_rig">{{ number_format( 35298433 ) }}</td>
                    <td></td>
                </tr>


                <!--tab-->

            </table>


        </div>
    </div>



</div>
@stop

@section('script')
<script>

    $(window).load(function(){
        $("a[href='#tabs-8']").click();
    });
</script>
@stop