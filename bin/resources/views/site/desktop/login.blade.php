@extends('layouts.desktop.default')


@section('header')
    @include('modules.desktop.header', array('title'=>'Registro'))
@stop

@section('style_datapiker')
<link rel="stylesheet" href="{{ asset('./assets/mobile/components/jquery-ui/themes/ui-lightness/jquery-ui.min.css') }}">
@stop

@section('mod_footer', "mod_footer")

@section('solo_login')

<div class="desk_login ">
    <form action="{{ route('handle_login') }}" method="POST" >
        <div id="al_form_choco" class="form-login">

             <img src="{{ asset('./assets/desktop/img/al_user.png') }}" alt="registro">


<div class="nolog">
            <div class="campo_reg">
                <input required class="validate-username" type="text" id="username" name="username" placeholder="Escribe tu usuario">

            </div>
            <div class="campo_reg">
                    <input required type="text" id="birthdate" name="birthdate" placeholder="DD/MM/AA" readonly class="make-datepicker">
                <!--div class="input-msg">
                    <span>Está será tu clave de acceso.</span>
                </div-->
            </div>
            <div id="ingreso">
                <input type="submit" id="ing_login" value="ingresar">
                <p>Si ya te registraste, continua aquí</p>
            </div>
            <div id="reg_login">
                <a  href="{{ route('register') }}">Regístrate</a>
                <p>¿No te haz registrado? Haz click en el botón</p>
            </div>


            </div>

<div class="silog">
    <div class="info">
                    <p id="user">Julián Calvachi</p>
                </div>
                <div class="info">
                    <p>Posición: <span>1</span></p>
                </div>

                <div class="cont_login_desk">
                    <a href="">Jugar</a>
                </div>
                <div class="cont_login_desk">
                    <a href="">Salir</a>
                </div>



</div>

        </div>
       

        <p>
            @if( session('status') !== NULL )
            
                <span id="userNameError" class="error ">{{ session('status') }}</span><br>
            
        @endif
             
            <a href="javascript:;" id="olvido_pass">¿Olvidaste tu usuario o contraseña?</a></p>
        {!! Form::token() !!}
    </form>

</div>



@stop



@section('auxiliar')
<div class="log_img">
    <div class="cont_login_desk">
        <a href="{{ route('demo') }}">¡Prueba Gratis!</a>
        <p>¡Prueba el juego ya!</p>

    </div>
    <img src="{{ asset('./assets/desktop/img/gran_back_login.png') }}" alt="registro"></div>
@stop


@section('contenido_alerta')
<div class="escribe_correo">
    <div class="vista1" id="vista1">
            <div class="imagen"><img src="{{ asset('./assets/desktop/img/admiracion.png') }}"></div>

            <div class="func_text">

                <p>¿Olvidaste tu contraseña?<br>Escribe tu correo electrónico y te la enviaremos.</p>
                <p id="emailNotFound" >{{-- JS FILLED CONTENT --}}</p>
                <form action="{{ route('pass-recover') }}" method="POST" id="recoverPassForm" >
                    <input type="text" placeholder="ejemplo@ejemplo.com" name="email" >
                    <input type="submit" value="Enviar">
                </form>
            </div>
     </div>

     <div class="vista2" id="vista2">
            <div class="imagen"><img src="{{ asset('./assets/desktop/img/candado.png') }}"></div>

            <div class="func_text">

                <p id="recoverPassFeddback" >{{-- JS FILLED CONTENT --}}</p>

                    <input id="btn_cerrar" type="submit" value="Cerrar">

                            </div>
     </div>
</div>
@stop