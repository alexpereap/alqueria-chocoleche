@extends('layouts.desktop.default')

@section('pantalla_completa',"pantalla_completa")
@section('delete', "delete")

@section('footer_2', "footer_2")


@section('contenido_alerta')
            <div class="btnface"><a href=""><img id="theShareButton" src="{{ asset('./assets/desktop/img/al_facebook.png') }}"></a></div>


            <div class="imagen"><img src="{{ asset('./assets/desktop/img/al_flag.png') }}"></div>

            <div class="func_text">
                <h2>partida finalizada</h2>
                <p>Recuerda que entre más códigos ingreses, más oportunidad tienes de ganar. Y no olvides que acumulas tus puntos para las siguientes partidas</p>

                <ul class="oppopu">
                    <li><img src="{{ asset('./assets/desktop/img/al_human.png') }}">
                        <p>Posición:<span>{{ $user_rank->rank }}</span></p>
                    </li>
                    <li><img src="{{ asset('./assets/desktop/img/al_point.png') }}">
                        <p>{{ $user_rank->score }}<span>puntos</span></p>
                    </li>
                </ul>

                <ul class="savora">
                    <li><a href="{{ route('home') }}">Salir del juego</a></li>
                    <li><a href="{{ route('game-index') }}">Volver a Jugar</a></li>
                    <li><a href="{{ route('ranking') }}">Ranking</a></li>
                </ul>

            </div>
@stop