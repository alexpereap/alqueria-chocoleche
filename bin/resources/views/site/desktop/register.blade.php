@extends('layouts.desktop.default')


@section('header')
    @include('modules.desktop.header', array('title'=>'Registro'))
@stop

@section('style_datapiker')
<link rel="stylesheet" href="{{ asset('./assets/mobile/components/jquery-ui/themes/ui-lightness/jquery-ui.min.css') }}">
@stop

@section('desk_cont_real')
<div id="desk_cont_real" class="al_dis_contenido">

    <img src="{{ asset('./assets/desktop/img/img_regis.png') }}" alt="registro">
    <p>Comienza completando los siguientes datos:</p>

    <form action="{{ route('handle_register') }}" id="registerForm">

        <div id="al_form_choco" class="form-register">

            <div class="campo_reg">
                <input required class="validate-alpha" type="text" name="name" placeholder="Escribe tu nombre">
            </div>
            <div class="campo_reg">
                <input required class="validate-alpha" type="text" name="lastname" placeholder="Escribe tu apellido">
            </div>
            <div class="campo_reg">
                <input required class="validate-email" type="email" id="email" name="tutor_email" placeholder="Correo electrónico de uno de tus padres">
                <div class="input-msg">
                    <span id="emailError" class="error hidden">Este email ya se encuentra registrado.</span>
                </div>

            </div>

            <div class="campo_reg">
                <div class="campo_dividido nor" id="birthdateWrapper" >
                    <input required type="text" id="birthdate" name="birthdate" placeholder="Fecha de nacimiento" readonly class="make-datepicker">
                    <label>DD/MM/AA</label>

                </div>
                <div class="input-msg">
                    <span>Está será tu clave de acceso.</span>
                </div>

            </div>

            <div class="campo_reg">
                <input required class="validate-phone" type="text" name="tutor_phone" placeholder="Telefono de uno de tus padres">
            </div>

            <div class="campo_reg">
                <input required class="validate-username" type="text" id="username" name="username" placeholder="Crea un usuario para identificarte">
                <div class="input-msg">
                    <span id="userNameError" class="error hidden">Este usuario ya existe, escribe uno diferente.</span>
                </div>
            </div>



            <div class="campo_dividido che">
                <label>

                    <div class="box-check">
                        <!-- no change position of input -->
                        <input type="checkbox" name="terminos" id="terms1">

                        <div class="box-check-ok">
                            <span></span>
                        </div>
                        <p>He leído y acepto los términos y condiciones de la actividad</p>
                    </div>
                </label>
            </div>

            <div class="campo_dividido che">
                <label>
                    <div class="box-check">
                        <!-- no change position of input -->
                        <input type="checkbox" name="mayor" id="terms2">

                        <div class="box-check-ok">
                            <span></span>
                        </div>
                        <p>Soy mayor de edad o mis papás saben que estoy participando en este concurso</p>
                    </div>
                </label>
            </div>

            <p class="al_recuerde">Recuerda escribir los datos correctos. En caso de resultar ganador nos pondremos en contacto al correo electrónico de tus papás. *Todos los datos son obligatorios</p>

            <div class="centrado">
                <input type="submit" id="al_eviar_reg">
            </div>
        </div>
    </form>

    <section id="al_alertas"></section>

</div>
@stop


@section('contenido_alerta')

            <!-- <div class="btnface"><a href=""><img src="{{ asset('./assets/desktop/img/al_facebook.png') }}"></a></div>


            <div class="imagen"><img src="{{ asset('./assets/desktop/img/al_flag.png') }}"></div>

            <div class="func_text">
                <h2>partida finalizada</h2>
                <p>Recuerda que entre más códigos ingreses, más oportunidad tienes de ganar. Y no olvides que acumulas tus puntos para las siguientes partidas</p>
                <ul class="oppopu">
                    <li><img src="{{ asset('./assets/desktop/img/al_boton.png') }}">
                        <p>X0 <span>Oportunidades</span></p>
                    </li>
                    <li><img src="{{ asset('./assets/desktop/img/al_human.png') }}">
                        <p>Posición:<span> 128</span></p>
                    </li>
                    <li><img src="{{ asset('./assets/desktop/img/al_point.png') }}">
                        <p>1245 <span>puntos</span></p>
                    </li>
                </ul>
                <ul class="savora">
                    <li><a href="">Salir del juego</a></li>
                    <li><a href="">Volver a Jugar</a></li>
                    <li><a href="">Ranking</a></li>
                </ul>
            </div>-->

            <div class="func_text">
                <h2 style="visibility:hidden;" >partida finalizada</h2>
                <p id="modalTxt" ><!-- js filled content--></p>
                <ul class="savora">
                    <li><a href="" class="cerrar_alerta" >Cerrar</a></li>
                </ul>
            </div>
@stop