@extends('layouts.desktop.default')


@section('desk_cont_real')

            <div class="al_dis_contenido al_contactenos al_codigos">
                <img src="{{ asset('./assets/desktop/img/al_como.png') }}" alt="contacto">
                <div class="galeria">
                    <ul class="bxslider">
                        <li><div class="cont_gal"><img src="{{ asset('./assets/desktop/img/alcomo4.png') }}"><p><span>1.</span> Regístrate ingresando tus datos.</p></div></li>
                        <li><div class="cont_gal"><img src="{{ asset('./assets/desktop/img/alcomo3.png') }}"><p><span>2.</span> Escoge los premios que más te gustan
y crea tu mezcla perfecta</p></div></li>
                        <li><div class="cont_gal"><img src="{{ asset('./assets/desktop/img/alcomo2.png') }}"><p><span>3.</span> Ingresa los códigos de los empaques y estos se convertirán en 80 oportunidades de lanzamiento, demuestra tu puntería y juega por tu mezcla perfecta. Tu código equivale a 80 oportunidades.</p></div></li>
                        <li><div class="cont_gal"><img src="{{ asset('./assets/desktop/img/alcomo1.png') }}"><p><span>4.</span>  Semanalmente hay 2 posibles ganadores y se reiniciará el puntaje para que tengas la oportunidad de volver a jugar y ganar.</p></div></li>
                    </ul>
                </div>


            </div>

            @if( !Auth::check() )
                <div class="section fold desktop">
                    <div class="btn btn-image btn-skip">
                        <a href="{{ route('handle_register') }}"><img width="190" src="{{ asset('assets/mobile/img/btn-skip.png') }}" alt=""></a>
                    </div>
                </div>
            @else
                <div class="section fold desktop">
                    <div class="btn btn-image btn-skip">
                        <a href="{{ route('game-index') }}"><img width="336" src="{{ asset('assets/mobile/img/btn-play.png') }}" alt=""></a>
                    </div>
                </div>
            @endif
@stop