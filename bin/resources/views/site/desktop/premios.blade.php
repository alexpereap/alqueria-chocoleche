@extends('layouts.desktop.default')

@section('clasmil', "classmil")

@section('header')

    @include('modules.desktop.header', array('title'=>'Contactenos'))

@stop

@section('desk_cont_real')

            <div class="al_dis_contenido al_contactenos al_premios <?php if( !Auth::check() ) echo 'hide-radios' ?>">
                <img src="{{ asset('./assets/desktop/img/al_premios.png') }}" alt="premios">
                <p>Estos son los premios que puedes ganar. ¡Haz tu mezcla perfecta y anímate a participar!</p>
                <p class="imgref">Imágenes de referencia</p>
                <div class="todos_premios">
                    <form id="selectPrizeForm" action="{{ route('save-game-combo') }}" method="POST" >
                    <div class="cont_premios">
                        <div class="cat_pre">
                            <img src="{{ asset('./assets/desktop/img/pre_tecnologia.png') }}">
                        </div>
                        <ul class="premios">
                            @foreach ($tech_prizes as $p)
                            <li>
                                <div class="img_pre">
                                    <img src="{{ asset('assets/desktop/img/' . $p->image_desktop ) }}">
                                    @if( Auth::check() && $user_has_combo === FALSE )
                                    <input type="radio" value="{{ $p->id }}" required name="tech_prize">
                                    @endif
                                 </div>
                                <p class="text_prem">{{ $p->name }}</p>
                            </li>
                            @endforeach
                        </ul>
                    </div>



                <div class="cont_premios">
                        <div class="cat_pre">
                            <img src="{{ asset('./assets/desktop/img/pre_musica.png') }}">
                        </div>
                        <ul class="premios">
                            @foreach ($music_prizes as $p)
                            <li>
                                <div class="img_pre">
                                     <img src="{{ asset('assets/desktop/img/' . $p->image_desktop ) }}">
                                     @if( Auth::check() && $user_has_combo === FALSE )
                                    <input type="radio" value="{{ $p->id }}" required name="music_prize">
                                    @endif
                                 </div>
                                <p class="text_prem">{{ $p->name }}</p>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="cont_premios">
                        <div class="cat_pre">
                            <img src="{{ asset('./assets/desktop/img/pre_aventura.png') }}">
                        </div>
                        <ul class="premios">
                            @foreach ($adventure_prizes as $p)
                            <li>
                                <div class="img_pre">
                                     <img src="{{ asset('assets/desktop/img/' . $p->image_desktop ) }}">
                                     @if( Auth::check() && $user_has_combo === FALSE )
                                    <input type="radio" value="{{ $p->id }}" required name="adventure_prize">
                                    @endif
                                 </div>
                                <p class="text_prem">{{ $p->name }}</p>
                            </li>
                            @endforeach
                        </ul>
                    </div>



                    @if( Auth::check() && $user_has_combo === FALSE )
                    <div class="derecha">

                            <input type="submit" id="al_eviar_reg">
                        </div>
                        {!! Form::token() !!}
                    @endif
                </form>
                @if( Auth::check() && $user_has_combo === FALSE )
                    <input type="hidden" id="selectPrizesAvailable" value="1" >
                @endif
                </div>



            </div>
@stop


