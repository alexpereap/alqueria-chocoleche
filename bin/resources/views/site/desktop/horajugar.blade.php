@extends('layouts.desktop.default')


@section('header')

    @include('modules.desktop.header', array('title'=>'Contactenos'))

@stop

@section('desk_cont_real')
        <form action="{{ route('game-validation-bridge') }}" method="POST" class="form" >
        <input type="hidden" name="code_id" value="{{ Crypt::encrypt( session('code_id') ) }}" >
        <input type="hidden" name="type_of_gameplay" value="mouse-control" >

            <div class="al_dis_contenido al_horajugar">
                <img src="{{ asset('./assets/desktop/img/al_hora_game.png') }}" alt="contacto">
                <div class="eshora">
                     <p>Ahora cuentas con:</p>
                    <img src="{{ asset('./assets/desktop/img/al_horajugar.png') }}" alt="contacto">
                    <p><span>Tienes 80 oportunidades</span></p>

<div class="controls">
    <p>¿Qué quieres usar como control, el mouse o tu celular*?</p>
                    <div class="lamitad der">
<div class="cont_img">
    <button type="submit"></button>
                        <!--img src="{{ asset('./assets/desktop/img/chocomouse.png') }}" alt="contacto"--><p>Haz Click para jugar</p>
</div>
                    </div>
                    <div class="lamitad izq">
<div class="cont_img">
                        {{-- <img src="{{ asset('./assets/desktop/img/qrcode.png') }}" alt="contacto"> --}}
                        {!! $qrcode !!}
                        <p>Escanea el codigo con tu celular</p>
</div>
<div class="tec"><p>*Debes descargar una aplicación de lector de códigos QR</p></div>
                    </div>
                    
</div>






                </div>
            </div>
            {!! Form::token() !!}
        </form>

        <form id="mobileTypeOfGame"  action="{{ route('game-validation-bridge') }}" method="POST" class="form" >
            <input type="hidden" name="code_id" value="{{ Crypt::encrypt( session('code_id') ) }}" >
            <input type="hidden" name="type_of_gameplay" value="cellphone-control" >
            <input type="hidden" name="room_id" value="{{ $room_id }}" >
            {!! Form::token() !!}
        </form>
@stop


@section('script')
<iframe frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" tabindex="-1" src="{{ $node_comunicator_url }}" frameborder="0"></iframe>
<script src="{{ asset('js/node-comunicator.js?v=' . time() ) }}" ></script>

@stop