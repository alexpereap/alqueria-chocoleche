@extends('layouts.desktop.default')


@section('header')

    @include('modules.desktop.header', array('title'=>'Registro'))

@stop

@section('desk_cont_real')



            <div class="al_dis_contenido">
                <img src="{{ asset('./assets/desktop/img/al_ranking.png') }}" alt="contacto">
            </div>

@stop

@section('auxiliar')

<div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_medalla.png') }}">Posición</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_user.png') }}">Jugador</strong></td>
                    <td><strong><img src="{{ asset('./assets/desktop/img/al_star.png') }}">Puntos</strong></td>
                    <td></td>
                </tr>
                @foreach( $season_ranking as $r )
                <tr <?php if( Auth::check() === true ){ if( $r->user_id == Auth::user()->id ): ?> id="tab_tu" <?php endif; } ?> >
                    <td></td>
                        <td>{{ $r->rank }}</td>
                        <td>{{ $r->username }}</td>
                        <td>{{ number_format( $r->score ) }}</td>
                    <td></td>
                </tr>
                @endforeach


                @if( isset( $user_ranking ) )
                <tr id="tab_tu" >
                    <td></td>
                        <td>{{ $user_ranking->rank > 100 ? '100+' : $user_ranking->rank }}</td>
                        <td>{{ $user_ranking->username }}</td>
                        <td>{{ number_format( $user_ranking->score ) }}</td>
                    <td></td>
                </tr>
                @endif

            </table>

            <div class="cont_login_desk">
                <a href="{{ route('home') }}">Volver a jugar</a>
            </div>


        </div>

        @stop