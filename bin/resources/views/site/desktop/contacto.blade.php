@extends('layouts.desktop.default')


@section('header')

    @include('modules.desktop.header', array('title'=>'Contactenos'))

@stop

@section('desk_cont_real')

            <div class="al_dis_contenido al_contactenos">
                <img src="{{ asset('./assets/desktop/img/al_conta.png') }}" alt="contacto">
                <p>Responderemos todas tus inquietudes en la mayor brevedad posible. Tus preguntes son muy importantes para nosotros.</p>
                <form action="{{ route('handle-contact') }}" method="POST"  >
                    <div id="al_form_choco" class="form-register">
                        <div class="campo_reg">
                        <input required class="tercio validate-alpha" type="text" name="name" placeholder="Nombre Completo">
                    </div>
                    <div class="campo_reg">
                        <input required class="tercio validate-alpha" type="text" name="email" placeholder="Correo Electrónico">
                        </div>

                        <div class="campo_reg">
                        <input required class="tercio validate-phone" type="text" name="phone" placeholder="Télefono">
                        </div>
                        <textarea required name="message" placeholder="Mensaje"></textarea>
                        <div class="centrado">
                            <input type="submit" id="al_eviar_reg">
                        </div>
                    </div>
                    {!! Form::token() !!}
                </form>
            </div>
@stop

@section('contenido_alerta')
<div class="func_text">
    <h2 style="visibility:hidden;" >partida finalizada</h2>
    <p id="modalTxt" >{{ session('modal_txt') }}</p>
    <ul class="savora">
        <li><a href="" id="sendSubmitCodeForm" class="cerrar_alerta" >Cerrar</a></li>
    </ul>
</div>
@stop

@if( session('show_modal') === true )
<input type="hidden" id="showContactFeedback" value="1" >
@endif