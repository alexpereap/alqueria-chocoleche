@extends('layouts.desktop.default')




@section('desk_cont_real')

            <div class="al_dis_contenido al_contactenos">
                <img src="{{ asset('./assets/desktop/img/Tus_premios.png') }}" alt="tuspremios">
                <p>Esta es tu mezcla perfecta, no pares de participar hasta conseguirlos.</p>

                <div class="todos_premios">
                    <div id="img_mi_premio">
                    @foreach( $prizes as $p )
                    <div class="mi_premio" >
                        <div id="img_premio">
                            <img src="{{ asset('assets/desktop/img/' . $p->image_desktop ) }}" alt="">
                        </div>
                        <p>{{ $p->name }}</p>
                    </div>
                    @endforeach

                    </div>
                    <div id="btnpremios">
                    <div class="cont_login_desk">
                        <a href="{{ route('home') }}">Volver</a>
                    </div>

                    <div class="cont_login_desk">
                        <a href="{{ route('prizes') }}">Ver todos los premios</a>
                    </div>
                    </div>




                </div>



            </div>
@stop
