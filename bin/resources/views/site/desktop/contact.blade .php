@extends('layouts.desktop.default')


@section('header')

    @include('modules.desktop.header', array('title'=>'Contactenos'))

@stop

@section('desk_cont_real')
            <div class="al_dis_contenido al_contactenos">
                <img src="img/al_conta.png" alt="contacto">
                <p>Responderemos todas tus inquietudes en la mayor brevedad posible. Tus preguntes son muy importantes para nosotros.</p>
                <form>
                    <div id="al_form_choco" class="form-register">
                        <input class="tercio validate-alpha" type="text" name="Nombre" placeholder="Nombre Completo">
                        <input class="tercio validate-alpha" type="text" name="Apellido" placeholder="Correo Electrónico">
                        <input class="tercio validate-phone" type="text" name="Telefono" placeholder="Télefono">
                        <textarea placeholder="Mensaje" value="mensaje"></textarea>
                        <div class="centrado">
                            <input type="submit" id="al_eviar_reg">
                        </div>
                    </div>
                </form>
            </div>      
@stop
      

