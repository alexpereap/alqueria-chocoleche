<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Statistics</title>

    <link rel="stylesheet" href="{{ asset('bootstrap-3.3.5-dist/css/bootstrap.min.css') }}">
    <style>
    body {
          padding-top: 40px;
          padding-bottom: 40px;
          background-color: #eee;
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          height: auto;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
          padding: 10px;
          font-size: 16px;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="email"] {
          margin-bottom: -1px;
          border-bottom-right-radius: 0;
          border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
    </style>
</head>
<body>


    <div class="container">

      <form class="form-signin" action="{{ route('post-statistics') }}" method="POST" >
        <h2 class="form-signin-heading">Please sign in</h2>
        {{ session('status') }}
        <label for="inputPassword" class="sr-only">Password</label>
        <input autocomplete="off" type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        {!! Form::token() !!}
      </form>

    </div> <!-- /container -->

    <script src="{{ asset('js/jquery-1.11.3.min.js') }}" ></script>
    <script src="{{ asset('bootstrap-3.3.5-dist/js/bootstrap.min.js') }}" ></script>

</body>
</html>