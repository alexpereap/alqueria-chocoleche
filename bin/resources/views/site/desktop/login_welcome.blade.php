@extends('layouts.desktop.default')

@section('pantalla_completa',"pantalla_completa")
@section('delete', "delete")

@section('contenido_alerta')


            <div class="imagen"><img src="{{ asset('./assets/desktop/img/admiracion.png') }}"></div>

            <div class="func_text">
                <h2>{{  $current_season->name }}</h2>
                <p>{{ $message }} Recuerda que cada semana tu puntaje se reinicia y tienes oportunidades de ganar.</p>


                <ul class="savora no-padding-left">
                    <li>
                        <a href="{{ route('game-index') }}"  >Continuar</a>
                    </li>

                </ul>

            </div>
@stop