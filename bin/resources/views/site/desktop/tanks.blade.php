@extends('layouts.desktop.default')

@section('landing_alerta',"landing_alerta")


@section('header')

    @include('modules.desktop.header', array('title'=>'Contactenos'))

@stop





@section('fb_conversion')
    @include('conversion.fb')
@stop


@section('google_conversion')
    @include('conversion.google')
@stop



@section('desk_cont_real')

           <div id="gracias_regis">
            <div class="imagen"><img src="{{ asset('./assets/desktop/img/chulos.png') }}"></div>
            <p>Tu registro ha sido exitoso, gracias por participar.</p>
            <div class="cont_login_desk">
                <a id="continuar_registro" href="{{ route('mechanics') }}">Continuar</a>
            </div>
            </div>

@stop

@section('contenido_alerta')
<div class="func_text">
    <h2 style="visibility:hidden;" >partida finalizada</h2>
    <p id="modalTxt" >{{ session('modal_txt') }}</p>
    <ul class="savora">
        <li><a href="" id="sendSubmitCodeForm" class="cerrar_alerta" >Cerrar</a></li>
    </ul>
</div>
@stop


