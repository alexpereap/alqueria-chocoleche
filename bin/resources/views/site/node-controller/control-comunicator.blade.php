<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Document</title>
</head>
<body>
    <script src="{{ asset('js/jquery-1.11.3.min.js') }}" ></script>
    <p> {{ session('current_game_id') }} </p>

    <input type="hidden" id="saveUrl" value="{{ route('game-save') }}"  >
    <input type="hidden" id="redirectUrl" value="{{ route('game-result') }}" >
    <script src="{{ asset('js/node-comunicator.js?v=' . time() ) }}" ></script>

</body>
</html>