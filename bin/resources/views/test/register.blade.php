@extends('layouts.test')
@section('page-title', "test" )


@section('content')
<h3>Test register template</h3>
    <form id="registerForm" action="{{ route('handle_register') }}" method="POST" >
        <input type="text" name="name" placeholder="name" id="">
        <input type="text" name="lastname" placeholder="lastname" id="">
        <input type="text" name="username" placeholder="username" id="">
        <input type="text" name="birthdate" placeholder="birthdate" id="">
        <input type="text" name="tutor_email" placeholder="tutor_email" id="">
        <input type="text" name="tutor_name" placeholder="tutor_name" id="">
        <input type="checkbox" value="yes" required name="terms" placeholder="terms" id="">
        <input type="checkbox" value="yes" required name="tutor_approval" placeholder="tutor_approval" id="">
        {!! Form::token() !!}
        <input type="submit" value="Envíar">
    </form>
@endsection