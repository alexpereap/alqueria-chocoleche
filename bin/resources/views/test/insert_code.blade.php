@extends('layouts.test')
@section('page-title', "insert code" )


@section('content')
<h3>Test insert code template</h3>
    <form action="{{ route('insert-code') }}" method="POST" >

        <p>{{ session('status') }}</p>

        <input required type="date" name="code_date" id="">
        <input required type="text" name="code_batch" id="">
        <input type="submit" value="Enviar">
        {!! Form::token() !!}

    </form>

<p>test ajax save</p>
    <form action="{{ route('game-save') }}" id="ajaxTestSaveGame" >
        <input type="hidden" name="field1" value="fieldOne" >
        <input type="hidden" name="field2" value="fieldTwo" >

        <input type="submit" value="Envíar">
    </form>

@endsection