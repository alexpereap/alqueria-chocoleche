@extends('layouts.test')
@section('page-title', "Login" )


@section('content')
<h3>Test Login template</h3>
<p>{{ session('status') }}</p>
    <form id="loginForm" action="{{ route('handle_login') }}" method="POST" >

        <input type="text" name="username" placeholder="username" id="">
        <input type="date" name="birthdate" placeholder="birthdate" id="">

        {!! Form::token() !!}
        <input type="submit" value="Entrar">
    </form>
@endsection