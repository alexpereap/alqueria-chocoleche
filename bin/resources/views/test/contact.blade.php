@extends('layouts.test')
@section('page-title', "test" )


@section('content')
<h3>Test Contact template</h3>
    <form  action="{{ route('handle-contact') }}" method="POST" >
        <input type="text" name="name" placeholder="Nombre completo" >
        <input type="text" name="email" placeholder="Email" >
        <input type="text" name="phone" placeholder="Telefono" >
        <textarea name="message" placeholder="Mensaje" cols="30" rows="10"></textarea>
        {!! Form::token() !!}
        <input type="submit" value="Envíar">
    </form>
@endsection