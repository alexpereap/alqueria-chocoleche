@extends('layouts.test')
@section('page-title', "Game start up" )


@section('content')
<h3>Test game startup template</h3>
    <form action="{{ route('game-validation-bridge') }}" method="POST" >
        <input type="hidden" name="code_id" value="{{ Crypt::encrypt( session('code_id') ) }}" >

        <input type="submit" value="Enviar">
        {!! Form::token() !!}

    </form>
@endsection