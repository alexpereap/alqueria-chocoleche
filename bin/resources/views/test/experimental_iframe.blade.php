<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Document</title>
    <style>
        body{
            padding:0;
            margin:0;
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>

<iframe id="theGame" style="position: absolute; width:100%; height: 100%; border: none" src="{{ route('home') }}" frameborder="0"></iframe>

<script src="{{ asset('./assets/mobile/components/jquery/jquery.min.js') }}"></script>
<script>
    function launchIntoFullscreen(element) {
      if(element.requestFullscreen) {
        element.requestFullscreen();
    } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}

$("#goFs").click(function(e){
    e.preventDefault();
    launchIntoFullscreen(document.documentElement); // the whole page
});
</script>

</body>
</html>