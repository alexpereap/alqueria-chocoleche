<p>Tech</p>

<form action="{{ route('save-game-combo') }}" method="POST" >
    @foreach ($tech_prizes as $p)
        {{ $p->name }}<input type="radio" required name="tech_prize" value="{{ $p->id }}" >
    @endforeach

    <p>Adventure</p>

    @foreach ($adventure_prizes as $p)
        {{ $p->name }}<input type="radio" required name="adventure_prize" value="{{ $p->id }}" >
    @endforeach

    <p>Music</p>

    @foreach ($music_prizes as $p)
        {{ $p->name }}<input type="radio" required name="music_prize" value="{{ $p->id }}" >
    @endforeach
    {!! Form::token() !!}
    <input type="submit" value="Envíar">
</form>


@if( isset( $allow_set_previous_combo ) )

    <a href="{{ route('set-previous-combo') }}">Elegir combo de la semana pasada</a>

@endif