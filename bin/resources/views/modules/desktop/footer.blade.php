<footer class="@yield('mod_footer')">
     <div class="al_texto">
         <p>© 2015 todos los derechos reservados Productos Naturales de la Sabana S.A. <a href="{{ route('terms') }}">Términos y Condiciones |</a><a href="{{ route('habeas') }}"> Habeas Data |</a> </p>
     </div>
     <div class="back_leche">
         <div class="al_logo_al"><img src="{{ asset('assets/desktop/img/logo_alqueria_choco.png') }}"></div>
         <div class="al_logo_choco"><img src="{{ asset('assets/desktop/img/choco_leche_choco.png') }}"></div>
     </div>
     @if( isset($cierre) )
     	<div class="end-fix">
     		<div class="end-boys">	
     			<img src="{{  asset('assets/desktop/img/end-boys.png')  }}" alt="">	
     		</div>	
     	</div>
     @endif
</footer>