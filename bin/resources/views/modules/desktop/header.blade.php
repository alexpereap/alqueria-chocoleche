<header>
    <ul class="menu_choc">
        <!-- <li><a href="{{ route('home') }}">inicio</a></li> -->
        <!-- <li><a href="{{ route('mechanics') }}">¿cómo jugar?</a></li> -->
        <!-- <li><a href="<?php echo Auth::check() ? route('my-prizes') : route('prizes')  ?>">premios</a></li> -->
        <!-- <li><a href="{{ route('ranking') }}">ranking</a></li> -->
        <li><a href="{{ route('weekly-winners') }}">ganadores semanales</a></li>
        @if( !Auth::check() )
            <!-- <li><a href="{{ route('register') }}">registro</a></li> -->
        @else
            <!-- <li><a href="{{ route('logout') }}">salir</a></li> -->
        @endif

        <!-- <li><a href="{{ route('contact') }}">contáctenos</a></li> -->
    </ul>
</header>