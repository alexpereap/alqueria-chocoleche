<div class="header">
	<div id="snapDragger" class="toggle-menu">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div class="title">
		{{ $title }}
	</div>
</div>