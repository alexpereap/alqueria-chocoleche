<div class="navbar">
	<div class="brand">
		<img width="340" src="{{ asset('assets/mobile/img/bg-letters.png') }}" alt="">
	</div>
	<div class="navbar-scroll">
		<ul>
			<!-- <li>
				<a href="{{ route('home') }}"><span><img width="320" src="{{ asset('assets/mobile/img/navbar-init.png') }}" alt=""></span></a>
			</li>
			<li>
				<a href="{{ route('mechanics') }}"><span><img width="320" src="{{ asset('assets/mobile/img/navbar-how.png') }}" alt=""></span></a>
			</li>
			<li>
				<a href="<?php echo Auth::check() ? route('my-prizes') : route('prizes')  ?>"><span><img width="320" src="{{ asset('assets/mobile/img/navbar-prize.png') }}" alt=""></span></a>
			</li>
			<li>
				<a href="{{ route('ranking') }}"><span><img width="320" src="{{ asset('assets/mobile/img/navbar-rank.png') }}" alt=""></span></a>
			</li> -->
			<li>
				<a href="{{ route('weekly-winners') }}"><span><img width="320" src="{{ asset('assets/mobile/img/navbar-winners.png') }}" alt=""></span></a>
			</li>
			<!-- <li>
				<a href="{{ route('register') }}"><span><img width="320" src="{{ asset('assets/mobile/img/navbar-register.png') }}" alt=""></span></a>
			</li>
			<li>
				<a href="{{ route('contact') }}"><span><img width="320" src="{{ asset('assets/mobile/img/navbar-contact.png') }}" alt=""></span></a>
			</li> -->
			@if( Auth::check() )
			<!-- <li>
				<a href="{{ route('logout') }}"><span><img width="320" src="{{ asset('assets/mobile/img/navbar-close.png') }}" alt=""></span></a>
			</li> -->
			@endif
		</ul>
	</div>
	<div class="rib-block"></div>
</div>