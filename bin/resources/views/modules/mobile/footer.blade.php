<div class="footer-logo">
	<img width="190" src="{{ asset('assets/mobile/img/footer-logo.png') }}" alt="">
</div>
<div class="footer-terms hide">
	<div class="footer-term">
		<a href="{{ route('terms') }}" target="blank"><span>Términos y condiciones</span></a>
	</div>
	<div class="footer-term">
		<a href="{{ route('habeas') }}" target="blank"><span>Políticas habeas data</span></a>
	</div>
</div>