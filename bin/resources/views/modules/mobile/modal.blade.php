<div id="mainModal" class="<?php if( session('show_modal') === true ) echo 'active' ?> modal modal-alert table">
	<div class="table-cell">
		<div class="modal-lines"></div>
		<div class="modal-box">
			<div class="modal-logo">
				<span class="align-middle"></span>
				<img src="{{ asset('assets/mobile/img/check-ok.png') }}" alt="">
			</div>
			<div class="modal-text">
				<!-- Insert text here -->
				{{ session('modal_txt') }}
			</div>
			<div class="btn btn-image btn-close modal-close">
				<img width="105" src="{{ asset('assets/mobile/img/btn-close.png') }}" alt="">
			</div>
		</div>
	</div>
</div>


<div class="modal modal-code table">
	<div class="table-cell">
		<div class="modal-lines"></div>
		<div class="modal-box">
			<div class="modal-logo">
				<span class="align-middle"></span>
				<img src="{{ asset('assets/mobile/img/check-ok.png') }}" alt="">
			</div>
			<div class="modal-text">
				Para reclamar tus premios debes presentar los empaques con los códigos utilizados durante el juego
			</div>
			<div id="sendSubmitCodeForm" class="btn btn-image btn-close modal-close">
				<img width="105" src="{{ asset('assets/mobile/img/btn-continue.png') }}" alt="">
			</div>
		</div>
	</div>
</div>