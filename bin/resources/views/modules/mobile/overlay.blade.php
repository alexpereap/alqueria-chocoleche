<div class="overlay-device table {{ $classOverlay }}">
	<div class="table-cell">
		<div class="overlay-lines"></div>
		<div class="overlay-content">
			
			<div class="overlay-msg">Rota el dispositivo para poder jugar</div>
		</div>
	</div>
</div>