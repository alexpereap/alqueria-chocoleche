<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{{ url('/') }}">
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('page-title')</title>
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
    

    <script src="{{ asset('js/jquery-1.11.3.min.js') }}" ></script>
    <script src="{{ asset('js/backend.js?v=' . time() ) }}" ></script>

</body>
</html>