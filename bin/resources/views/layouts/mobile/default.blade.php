<!DOCTYPE html>
<html lang="en">
<head>
@include('meta.facebook')
	<!-- Info -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}" />


	@section('title')
		<title>Chocoleche Mezcla Perfecta</title>
	@show


	<!-- Organic -->
	<meta name="description" content="¡Yo participé por mi mezcla perfecta en www.chocoleche.com.co! Pon a prueba tu puntería y gana los premios que más te gusten haciendo clic aquí.">

	<!-- Viewport responsive -->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<!-- Webapp -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('./webapp.png') }}">
	<link rel="manifest" href="{{ asset('./manifest.json') }}">

	<!-- Opengraph ( Facebook and google+ ) -->
	<meta property="og:title" content="">
	<meta property="og:type" content="">
	<meta property="og:image" content="">
	<meta property="og:url" content="">
	<meta property="og:description" content="">

	<!-- Twitter -->
	<meta name="twitter:card" content="">
	<meta name="twitter:url" content="">
	<meta name="twitter:title" content="">
	<meta name="twitter:description" content="">
	<meta name="twitter:image" content="">

	<!-- Favicon X-icon  -->
	<link rel="icon" href="{{ asset('./favicon.ico?v0.2') }}">

	<!-- Plugin Style -->
	<link rel="stylesheet" href="{{ asset('./assets/mobile/components/jquery-ui/themes/ui-lightness/jquery-ui.min.css') }}">

	<!-- Styles -->
	<link rel="stylesheet" href="{{ asset('./assets/mobile/css/application.min.css') }}">

	@yield('style')

	<!-- Icon Font -->
	<link rel="stylesheet" href="{{ asset('./assets/mobile/css/font-awesome.min.css') }}">

	<!-- Script Important -->
	<script src="{{ asset('./assets/mobile/components/pace/pace.min.js') }}"></script>
	<script src="{{ asset('./assets/mobile/components/modernizr/modernizr.js') }}"></script>

	<!-- Jquery UI override -->
	<link rel="stylesheet" href="{{ asset('./assets/general/jquery-ui-override.css') }}">

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '926731727352142');
	fbq('track', "PageView");
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=926731727352142&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	@yield('fb_conversion')
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '1468262936828634');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1468262936828634&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>
<body>
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WXQ48F"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WXQ48F');</script>
	<!-- End Google Tag Manager -->
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '1670818706484866',
	      xfbml      : true,
	      version    : 'v2.4'
	    });
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	<!-- Wrapper -->
	<div class="wrapper @if( isset($classWrapper) ){{ $classWrapper }}@endif">

		@yield('wrapper')

		<!-- Wrapper-viewport -->
		<div class="wrapper-viewport">

			<div class="wrapper-navbar">

				<div class="snap-drawers">
					@include('modules.mobile.navbar')
				</div>

			</div>

			<div class="wrapper-app" id="wrapperApp">

				@yield('header')

				<div class="wrapper-scroll @if( isset($classScroll) ){{ $classScroll }}@endif">

					<div class="wrapper-page">

						<div id="app" class="@if( isset($classApp) ){{ $classApp }}@endif">
							@yield('app')
						</div>
						
						@yield('wrapperPage')

					</div>
				</div>

			</div>

			@section('footer')
				@include('modules.mobile.footer')
			@show

			@include('modules.mobile.modal')

			@yield('overlay')

		</div>
	</div>

	<script src="{{ asset('./assets/mobile/components/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('./assets/mobile/components/Snap.js/snap.min.js') }}"></script>
	<script src="{{ asset('./assets/mobile/components/jquery-ui/jquery-ui.min.js') }}" ></script>
	<script src="{{ asset('js/backend.js?v=' . time() ) }}" ></script>
	<script src="{{ asset('./assets/mobile/js/app.js?v=' . time() ) }}"></script>
	<script src="{{ asset('./assets/mobile/js/validate.js?v=' . time() ) }}"></script>
	<script src="{{ asset('./assets/mobile/js/service.js') }}"></script>

	@yield('script')

	<!-- IE fallback -->
	<!--[if (gte IE 6)&(lte IE 8)]>
	<script src="{{ asset('./assets/mobile/components/respond/dest/respond.min.js') }}"></script>
	<script src="{{ asset('./assets/mobile/components/selectivizr/selectivizr.js') }}"></script>
	<![endif]-->

	<!-- Google Code for Visitaron Chocoleche Juego Ni&ntilde;os OCT 15 -->
	<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 989393533;
	var google_conversion_label = "709-CK_NjWAQ_eTj1wM";
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/989393533/?value=1.00&amp;currency_code=COP&amp;label=709-CK_NjWAQ_eTj1wM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

	@if( Auth::check() )
	@if( Auth::user()->id == 280 )
	<script>
	  alert("¡Hola! Eres uno de nuestros posibles ganadores y hemos tratado de ponernos en contacto contigo. Por favor revisa tu bandeja de entrada y de SPAM del email que registraste y responde nuestro email.");
	</script>
	@endif
	@endif
</body>
</html>