<!DOCTYPE html>
<html lang="en">

<head>
@include('meta.facebook')
	<!-- Info -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}" />

  <meta name="description" content="¡Yo participé por mi mezcla perfecta en www.chocoleche.com.co! Pon a prueba tu puntería y gana los premios que más te gusten haciendo clic aquí.">
	<!-- Favicon X-icon  -->
	<link rel="icon" href="{{ asset('./favicon.ico?v0.1') }}">
	<!-- Plugin Style -->
	@yield('style')
    <link rel="stylesheet" href="{{ asset('./assets/desktop/css/desktop_css_game_choco.css?v=' . time() ) }}">
    <link rel="stylesheet" href="{{ asset('./assets/desktop/css/jquery.bxslider.css') }}">

    <!-- Plugin Style -->
    @yield('style_datapiker')   

    <!-- Jquery UI override -->
    <link rel="stylesheet" href="{{ asset('./assets/general/jquery-ui-override.css?v=' . time() ) }}"> 


    @section('title')
    <title>Chocoleche Mezcla Perfecta</title>
    @show








@yield('fb_conversion')

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1468262936828634');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1468262936828634&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>




<body class="@yield('delete') @yield('landing_alerta') @yield('fuera_de_temporada')">

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WXQ48F"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WXQ48F');</script>
<!-- End Google Tag Manager -->

@yield('google_conversion')


<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1670818706484866',
      xfbml      : true,
      version    : 'v2.4'
    });
  };



  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '926731727352142');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=926731727352142&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<section id="al_alertas2" class="@yield('pantalla_completa')">    
      <div class="toda_alerta">    
        <div class="contenedro_alerta">
            <div id="cerrar"><a id="cerrar_alerta" href="javascript:;">cerrar <span>X</span></a></div>
           @yield('contenido_alerta')
           
        </div>
        </div>
<div id="footer_alert" style="display:none">
        @include('modules.desktop.footer')
</div>

    </section>
	<section id="al_chocoleche" class="al_chocoleche">




        @section('header')
				@include('modules.desktop.header')
	    @show

	    @yield('solo_login')

        <div id="al_contenido" class="@yield('clasmil')">
            <div id="logo"><img src="{{ asset('./assets/desktop/img/logo_choco.png') }}"></div>

                  @yield('desk_cont_real')

        </div>
        @yield('auxiliar')


        @section('footer')
				@include('modules.desktop.footer')
	    @show

    </section>





	<script src="{{ asset('./assets/mobile/components/jquery/jquery.min.js') }}"></script>
    <!--script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script-->
    <script src="{{ asset('./assets/desktop/js/jquery.bxslider.js') }}"></script>

	<script src="{{ asset('./assets/desktop/js/desktop_jsbasico.js?v=' .time() ) }}"></script>
	<script src="{{ asset('./assets/desktop/js/validate.js?v='. time() ) }}"></script>
    <script src="{{ asset('./assets/mobile/components/jquery-ui/jquery-ui.min.js') }}" ></script>
	<script src="{{ asset('./js/backend.js') }}"></script>
	<script src="{{ asset('./assets/mobile/js/service.js') }}"></script>

	@yield('script')


<!-- Google Code for Visitaron Chocoleche Juego Ni&ntilde;os OCT 15 -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 989393533;
var google_conversion_label = "709-CK_NjWAQ_eTj1wM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/989393533/?value=1.00&amp;currency_code=COP&amp;label=709-CK_NjWAQ_eTj1wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

@if( Auth::check() )
@if( Auth::user()->id == 280 )
<script>
  alert("¡Hola! Eres uno de nuestros posibles ganadores y hemos tratado de ponernos en contacto contigo. Por favor revisa tu bandeja de entrada y de SPAM del email que registraste y responde nuestro email.");
</script>
@endif
@endif

</body>






</html>