// Ajax csrf token secure token
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

window.addEventListener('message',function(event) {
    // if(event.origin !== 'http://scriptandstyle.com') return;
    console.log( 'Origin: ' + event.origin );
    console.log('received response:  ',event.data);


    var pass_data = JSON.parse(event.data);
    switch( pass_data.action ){

        case 'initMobileTypeOfGame':
            initMobileTypeOfGame();
            break;

        case 'gameSave':
            gameSave( pass_data.score, pass_data.perfect_hits );
            break;
    }

},false);

function initMobileTypeOfGame(){

    $("#mobileTypeOfGame").submit();
    $("form").attr('disabled','disabled');
}

function gameSave(score, perfect_hits){

    $.ajax({
        url  : $("#saveUrl").val(),
        type : 'post',
        data : {
            score        : score,
            perfect_hits : perfect_hits
        },
        success : function(result){
            console.log(result);
            parent.window.location.replace( $("#redirectUrl").val() );
        }
    })
}