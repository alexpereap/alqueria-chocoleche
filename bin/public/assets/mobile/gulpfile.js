var gulp = require('gulp');
var livereload = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');
var stylus = require('gulp-stylus');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');
var mainBowerFiles = require('gulp-main-bower-files');
var del  = require('del');


var PATH = {
	APP: './stylus/application.styl',
	STYL: './stylus/**/*.styl',
	PHP: '../../../resources/views/**/*.php',
	CSS: './css',
	JS: './js/**/*.js'
}

gulp.task('clean:lib',function(){
	return del([
		'./components/**/*'
	]);
});

gulp.task('mainBowerFiles',['clean:lib'],function(){
	gulp.src('./bower.json')
		.pipe(mainBowerFiles({base: './bower_components',includeDev: true}))
		.pipe(gulp.dest("./components"));
});

gulp.task('stylus', function() {
	gulp.src([PATH.APP])
		.pipe(stylus())
		.pipe(autoprefixer({
			browsers: ['last 4 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(PATH.CSS))
		.pipe(minifyCSS({
			keepBreaks: true,
			compatibility: 'ie7,-units.ch,-units.in,-units.pc,-units.pt,-units.rem,-units.vh,-units.vm,-units.vmax,-units.vmin'
		}))
		.pipe(rename('application.min.css'))
		.pipe(gulp.dest(PATH.CSS))
		.pipe(livereload());
});

gulp.task('default', ['stylus','mainBowerFiles'], function() {
	livereload.listen();
	gulp.watch(PATH.STYL, ['stylus']);
	gulp.watch(PATH.PHP, livereload.changed);
	gulp.watch(PATH.JS, livereload.changed);
});
