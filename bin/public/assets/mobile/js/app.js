$(document).ready(function() {

	if( $('#snapDragger').length > 0 ){
		snapper = new Snap({
			element: document.getElementById('wrapperApp'),
			dragger: document.getElementById('snapDragger'),
			disable: 'right',
			maxPosition: 420
		});

		snappTrack();
	}

	$('#snapDragger').click(function(event) {
		if ( snapper.state().state == "left" ) {
			snapper.close('left');
		}else{
			snapper.open('left');
		}
	});

	$('.modal-close').click(function(event) {
		$('.modal').removeClass('active');
	});

	$('.form-select').click(function(event) {
		event.stopPropagation();
		$(this).find('.form-drop').toggleClass('opened');
	});

	$('.form-option').click(function(event) {
		var parent = $(this).closest('.form-select');
		var input = parent.find('input:hidden');
		var value = $(this).data('value');

		input.val( value );

		parent.find('.form-text').html( value );
	});

	$('.overlayPageAction').click(function(event) {
		$('.overlayPage').toggleClass('hidden');
		scrollTrack();
	});

	// sizeScreen();

	overlayTrack();
});

$(window).load(function() {
	scrollTrack();
});

// Edit device track
var sizeScreen = function(){
	var ws = $('.wrapper').width();
	var wh = $('.wrapper').height();

	var newDiv = $('<div><div>').css({
		position: 'fixed',
		bottom: '10px',
		right: '5px',
		color: '#f00',
		font: '12px'
	});

	newDiv.html('W' + ws + ' x H' + wh )

	$('body').append(newDiv);
}

// Fix snap open
var snappTrack = function(){
	if( $('#snapDragger').length == 0 ){
		return ;
	}

	var ww = $(this).innerWidth();
	if( ww > 470 ){
		snapper.settings({maxPosition: 420})
	}else{
		var limit = ww - 50;
		snapper.settings({maxPosition: limit})
	}
}

// Overlay block device traking
var overlayTrack = function(){
	if( $('input:focus').length > 0 ){ return ;}

	if( $('.block-portrait').length > 0 ){
		if ( $(this).innerHeight() > $(this).innerWidth()){
			$('.block-portrait').addClass('active');
		}else{
			$('.block-portrait').removeClass('active');
		}
	}

	if( $('.block-landscape').length > 0 ){
		if ( $(this).innerHeight() < $(this).innerWidth()){
			$('.block-landscape').addClass('active');
		}else{
			$('.block-landscape').removeClass('active');
		}
	}
}

// Scroll track over footer
var scrollTrack = function(){
	var wh = $('.wrapper-scroll').height();
	var st = $('.wrapper-scroll').get(0).scrollTop;
	var sh = $('.wrapper-scroll').get(0).scrollHeight;

	if( (wh + st + 30 ) >= sh  ){
		$('.footer-terms').removeClass('hide');
	}else{
		$('.footer-terms').addClass('hide');
	}
}

$('.wrapper-scroll').scroll(function(event) {
	scrollTrack();
});

// Open Modal Alert
$(document).ready(function() {

	$.fn.openModalAlert = function(text){
		$('.modal-alert').addClass('active');
		$('.modal-alert .modal-text').html(text);
	}

});

// Smart Resize
(function($,sr){

	var debounce = function (func, threshold, execAsap) {

	    var timeout;

	    return function debounced () {
	        var obj = this, args = arguments;
	        function delayed () {
	            if (!execAsap)
	                func.apply(obj, args);
	            timeout = null;
	        };

	        if (timeout)
	            clearTimeout(timeout);
	        else if (execAsap)
	            func.apply(obj, args);

	        timeout = setTimeout(delayed, threshold || 300);
	    };
	}

	$.fn[sr] = function(fn){
		return fn ? this.bind( 'resize', debounce(fn) ) : this.trigger(sr);
	}

})(jQuery, 'smartresize');

$(window).smartresize(function(){
	overlayTrack();
	snappTrack();
	scrollTrack();
	if( $('#snapDragger').length > 0 ){
		snapper.close();
	}
},500);

// forgot password form handling
$("#iForgot").click(function(e){

	e.preventDefault();
	$("#loginForm").addClass('hidden');
	$("#recoverPassForm").removeClass('hidden');

});

$("#iSession").click(function(e){

	e.preventDefault();
	$("#recoverPassForm").addClass('hidden');
	$("#loginForm").removeClass('hidden');

});