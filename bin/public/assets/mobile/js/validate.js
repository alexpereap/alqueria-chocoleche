// Ajax csrf token secure token
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).ready(function(){

    // Register Form Submit pre validate
    $("#registerForm").submit(function(e){
        e.preventDefault();
        var form = $(this);
        var form_error = false;

        if( !$("#tutorApproval").is(':checked') ){

            showMobileModal('Debes marcar que eres mayor de edad o que tienes la autorización de tus padres para participar.');
            return false;
        }

        if( !$("#terms1").is(':checked') ){
            showMobileModal('Debes aceptar términos y condiciones.');
            return false;
        }

        /*if( !$("#terms2").is(':checked') ){
            showMobileModal('Debes aceptar la politica de uso de datos y las condiciones de la actividad.');
            return false;
        }*/

        if( validEmail( $("#email").val() ) === false ){
            showMobileModal('Escribe una dirección de email valida');
             $("#email").focus();
             return false;
        }

        $("input[required]").each(function(){

            if( $(this).val() == '' ){
                showMobileModal('Todos los campos son obligatorios');
                return false;
            }
        });

        if( form_error === true ){
            return false;
        }

        $("#registerForm button[type=submit]").attr('disabled','disabled');

        $("#userNameError, #emailError").addClass('hidden');

        $.ajax({
            url  : form.attr('action'),
            data : form.serialize(),
            type : 'post',
            dataType : 'json',
            success : function(result){

                $("#registerForm button[type=submit]").removeAttr('disabled');

                if( result.success === false ){

                    switch( result.error ){
                        case 'unavailable-username':

                            $("#userNameError").removeClass('hidden');
                            $("#username").focus();

                            break;

                        case 'unavailable-email':

                            $("#emailError").removeClass('hidden');
                            $("#email").focus();

                            break;

                        case 'unknown':
                            alert(result.msg);
                            break;

                        case 'invalid-birthdate':{
                            showMobileModal( result.msg );
                        }
                    }

                    return false;
                }

                window.location.href = result.game_uri;
            }
        });
    }); // eof

    // submit code form pre submit validation
    $("#submitCodeForm").submit(function(e){

        if( $(this).hasClass('ready-to-send') ){
            return true;
        }

        var form_error = false;

        $("input[required]").each(function(){

            if( $(this).val() == '' ){
                showMobileModal('Por favor completa todos los campos');
                form_error = true;
                return false;
            }
        });

        if( form_error === true ){

            e.preventDefault();
            return false;
        }

        $(".modal-code").addClass('active');
        e.preventDefault();
    });

    $("#sendSubmitCodeForm").click(function(){
        $("#submitCodeForm").addClass('ready-to-send').submit();
    });

    $("#recoverPassForm").submit(function(e){
        e.preventDefault();

        $("#recoverPassForm button[type=submit]").attr('disabled','disabled');

        var form = $(this);

        $.ajax({

            url : $(this).attr('action'),
            data: form.serialize(),
            type : 'post',
            dataType : 'json',
            success:function(result){

                console.log(result);
                showMobileModal( result.feedback );
                $("#recoverPassForm button[type=submit]").removeAttr('disabled');
            }
        });
    })

}); // dready end

/*
 * Triggers modal box on mobile view
 * @param text: String, Text Showed by the modal
 * @param logo: Boolean, Shows or hides the modal logo
 */
function showMobileModal( text, logo ){

    $("#mainModal .modal-text").text(text);

    if( logo === false ){

        $("#mainModal .modal-logo").hide();
    }

    $("#mainModal").addClass('active');
}