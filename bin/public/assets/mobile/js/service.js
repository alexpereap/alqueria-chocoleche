$(document).ready(function(){

    // set the selected values on the form and then send the form
    $("#selectComboForm").submit(function(e){

        $("#selectComboForm button[type='submit']").attr('disabled','disabled');

        if( $(this).hasClass('ready-to-send') ){
            return true;
        }

        e.preventDefault();

        tech_prize_id = $(".tech-prizes-list[aria-hidden='false']").attr('data-value');
        adventure_prize_id = $(".adventure-prizes-list[aria-hidden='false']").attr('data-value');
        music_prize_id = $(".music-prizes-list[aria-hidden='false']").attr('data-value');

        $("#techPrize").val( tech_prize_id );
        $("#adventurePrize").val( adventure_prize_id );
        $("#musicPrize").val( music_prize_id );

        $(this).addClass('ready-to-send');
        $(this).submit();

    });
});