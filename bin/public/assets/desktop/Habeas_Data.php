<html>

<head>
<meta charset="utf-8">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536870145 1073786111 1 0 415 0;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-1593833729 1073750107 16 0 415 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
h2
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Título 2 Car";
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:2;
	font-size:18.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
a:link, span.MsoHyperlink
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:#0563C1;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:#954F72;
	text-decoration:underline;
	text-underline:single;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Título 2";
	font-family:"Times New Roman","serif";
	mso-ascii-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	font-weight:bold;}
p.msochpdefault, li.msochpdefault, div.msochpdefault
	{mso-style-name:msochpdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
p.msopapdefault, li.msopapdefault, div.msopapdefault
	{mso-style-name:msopapdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:106%;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
span.apple-converted-space
	{mso-style-name:apple-converted-space;
	mso-style-unhide:no;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-hansi-font-family:Calibri;}
.MsoPapDefault
	{mso-style-type:export-only;
	margin-bottom:8.0pt;
	line-height:106%;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:70.85pt 3.0cm 70.85pt 3.0cm;
	mso-header-margin:35.4pt;
	mso-footer-margin:35.4pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Tabla normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin-top:0cm;
	mso-para-margin-right:0cm;
	mso-para-margin-bottom:8.0pt;
	mso-para-margin-left:0cm;
	line-height:106%;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=ES-CO link="#0563C1" vlink="#954F72" style='tab-interval:35.4pt'>

<div class=WordSection1>

<h2 align=center style='margin:0cm;margin-bottom:.0001pt;text-align:center'><i><span
style='font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
color:#96928F'>Política uso de base de datos</span></i><span style='mso-fareast-font-family:
"Times New Roman";color:#96928F'><o:p></o:p></span></h2>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'>&nbsp;Al ingresar a este
sitio USTED ASUME LA OBLIGACIÓN DE RESPETAR LAS CONDICIONES DE USO (explicadas
a continuación), NOTIFICACIONES LEGALES, POLÍTICA DE PRIVACIDAD y todas las cláusulas
de exención de responsabilidad y términos y condiciones que figuran en el sitio
web de Alquería. Por favor lea cuidadosamente el contenido de los siguientes
textos.</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'>&nbsp;</span><span
style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal><span style='color:#96928F'>ADVERTENCIA: Si eres menor de
edad &nbsp;tu padre/madre, representante legal o acudiente debidamente
facultado, deberá leer obligatoriamente los términos y condiciones presentes en
el sitio web y autorizar &nbsp;el uso y tratamiento de tus datos personales
para poder participar y ganar. Recuerda que Productos Naturales de la Sabana
S.A solo hará entrega de premios a mayores de edad. </span><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'><br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-bottom:12.0pt;text-indent:-18.0pt;
line-height:14.4pt'><b><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>1.</span></b><b><span style='font-size:7.0pt;font-family:"Times New Roman","serif";
color:#96928F'>&nbsp;&nbsp;&nbsp; </span></b><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>Exención de
responsabilidad y limitación de responsabilidad.</span></strong><span
style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'>Al utilizar el sitio web de
Alquería, usted acepta en forma expresa que el uso del sitio web de Alquería se
realiza bajo su exclusiva responsabilidad y riesgo. El sitio web de Alquería se
proporciona &quot;tal cual está&quot; y &quot;como está disponible&quot;. Ni
Alquería ni su empresa matriz, filiales, subsidiarias o designadas, así como
tampoco sus respectivos funcionarios, directores, empleados, agentes, proveedores
de contenidos de terceros, diseñadores, contratistas, distribuidores,
vendedores, patrocinadores, concedentes y demás (conjuntamente,
&quot;Relacionados&quot;) garantizan que el uso del sitio web de Alquería no
sufrirá interrupciones ni contendrá errores. Ni Alquería ni sus relacionados
garantizan la precisión ni la integridad del contenido ofrecido en el sitio web
de Alquería. Además, Alquería no declara que el contenido provisto en el sitio
web de Alquería sea aplicable a, ni de uso apropiado en, lugares fuera de
Colombia.&nbsp;<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'>Bajo ninguna circunstancia
Alquería ni sus relacionados serán responsables de ningún daño directo,
indirecto, imprevisto, especial ni emergente que se genere, ya sea por el uso o
falta de capacidad para usar el sitio web, incluidos de forma no exclusiva, los
daños que surjan a raíz de su confianza en la información obtenida en el sitio
web de Alquería que ocasione errores, omisiones, interrupciones, eliminación o
corrupción de archivos, virus, demoras en la operación o transmisión, o cualquier
otro tipo de error en el funcionamiento. La limitación de responsabilidad
precedente se aplicará en toda acción legal, aun cuando un representante
autorizado de Alquería haya sido informado o debiera tener conocimiento de la
posibilidad de dichos daños.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:14.4pt'><strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>2.
Aceptación de Términos Al ingresar &nbsp;y/o usar este sitio.</span></strong><span
style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>a</span></strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>. El
usuario admite haber leído y entendido estos términos de uso y está de acuerdo
con acogerse a los mismos y cumplir con todas las leyes y los reglamentos
aplicables que hagan parte de la legislación colombiana. Además, cuando el
usuario utilice cualquier servicio suministrado en este sitio, por ejemplo,
buzones de sugerencias, actividades promocionales, estará sujeto a las reglas,
guías, políticas, términos y condiciones aplicables a dicha actividad. En caso
de que el usuario no esté de acuerdo con estos términos, debe abstenerse de
usar este sitio. Este sitio es controlado y operado por Productos Naturales de
la Sabana S.A. (propietario de la marca Alquería) desde sus oficinas ubicadas
en Cajicá. Aquellos que decidan ingresar a este sitio desde otros países lo
harán bajo su propia iniciativa y es su responsabilidad el sujetarse a las
leyes locales que sean aplicables. Cualquier reclamo en relación con el uso de
este sitio y el material en él contenido está regulado por las leyes de
Colombia. Estos términos y condiciones están sujetos a cambios sin previo aviso
en cualquier momento, bajo la sola voluntad de Alquería, y a partir de la fecha
de modificación de estos términos y condiciones, todas las operaciones que se
celebren entre Alquería y el usuario se regirán por el documento modificado.
Alquería se reserva el derecho de realizar cambios en el sitio web, en las
condiciones de uso y en las notificaciones legales en cualquier momento. Cada vez
que utiliza el sitio web de Alquería, debe consultar las condiciones de uso,
notificaciones legales y política de privacidad vigentes en ese momento, que se
aplican a las transacciones y al uso del sitio. Si no está satisfecho con el
sitio web de Alquería, su contenido, las condiciones de uso o las
notificaciones legales, usted acepta que la única solución disponible es dejar
de utilizar el sitio web de Alquería.</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'><br>
<strong><span style='font-family:"Verdana","sans-serif"'>b</span></strong>. Se
prohíbe usar el sitio indebidamente, falsear la identidad de un usuario,
utilizar agentes de compra y llevar a cabo actividades fraudulentas en el
sitio. </span><span style='color:#96928F'><a href="http://www.alqueria.com.co"><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>www.alqueria.com.co</span></a></span><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>
&nbsp;es un sitio web dedicado a &nbsp;informar al público las novedades,
respecto a los productos vinculados a la marca Alquería. Alquería es de
propiedad de Productos Naturales de la Sabana S.A., sociedad comercial
constituida bajo las leyes colombianas, cuya actividad principal es la
producción y comercialización de productos aptos para el consumo humano.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:14.4pt'><strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>3.
Derechos de Propiedad Intelectual.</span></strong><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>a</span></strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>. Todo
el material informático, gráfico, publicitario, fotográfico, de multimedia,
audiovisual y/o de diseño, así como todos los contenidos, textos y bases de
datos puestos a su disposición en este sitio son de propiedad exclusiva de
Alquería, o en algunos casos, de terceros que han autorizado a Alquería su uso
y/o explotación (en adelante &quot;los contenidos&quot;). Igualmente, el uso en
Alquería de algunos contenidos de propiedad de terceros se encuentra
expresamente autorizado por la ley. Todos los contenidos en Alquería están
protegidos por las normas sobre derechos de autor, marcas y por todas las
normas nacionales e internacionales que le sean aplicables. Exceptuando lo
expresamente estipulado en estos términos y condiciones, queda prohibido todo
acto de copia, reproducción, modificación, creación de trabajos derivados,
venta o distribución, exhibición de los contenidos, de ninguna manera o por
ningún medio, incluyendo, mas no limitado a, medios electrónicos, mecánicos, de
fotocopiado, de grabación o de cualquier otra índole, sin el permiso previo por
escrito de Alquería o del titular de los derechos de autor. En ningún caso
estos términos y condiciones confieren derechos, licencias y/o autorizaciones
para realizar los actos anteriormente descritos. Cualquier uso no autorizado de
los contenidos constituirá una violación a los presentes términos y condiciones
y a las normas vigentes sobre marcas, derechos de autor y/u otras normas de
propiedad intelectual tanto nacionales e internacionales aplicables. Alquería
otorga al usuario una licencia y derecho personal, intransferible y no
exclusivo para desplegar Alquería en la pantalla de un computador ordenador o
dispositivo PDA bajo su control.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:14.4pt'><strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>4.
Propiedad Industrial.</span></strong><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>a.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Todas las marcas, enseñas, logos, nombres
y cualesquiera otros signos distintivos, así como los modelos de utilidad y/o
diseños industriales y demás elementos de propiedad industrial o intelectual
insertados, usados y/o desplegados en este sitio son Propiedad exclusiva de
Alquería y en algunos casos son de propiedad de terceros que han autorizado
expresamente a Alquería para su uso y/o explotación. Nada en Alquería podrá ser
interpretado como concesión u otorgamiento a cualquier título de
autorizaciones, licencias o cualquier otro derecho para usar o disponer de
cualquier forma de la Propiedad Industrial, sin el permiso por escrito de
Alquería o del titular de los derechos de la misma. Cualquier uso no autorizado
constituirá una violación a los presentes términos y condiciones y a las normas
vigentes nacionales e internacionales sobre Propiedad Industrial y dará lugar a
las acciones civiles y penales correspondientes.</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span lang=EN-US
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F;
mso-ansi-language:EN-US'><br>
<span class=GramE><strong><span style='font-family:"Verdana","sans-serif"'>b.</span></strong>Linking,
Deeplinking y Framing.</span> </span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Alquería prohíbe el uso de los nombres
y/o logotipos, marcas y cualesquiera otros signos distintivos de su propiedad
como enlaces hipertextuales o de cualquier otra forma (&quot;links&quot;)
dirigidos a sitios en Internet cuyo URL sea diferente de </span><span
style='color:#96928F'><a href="http://www.alqueria.com.co"><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>http://www.alqueria.com.co</span></a></span><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>, a
menos que el establecimiento de un enlace de tal naturaleza sea aprobado por
Alquería por escrito. Alquería se reserva el derecho de solicitar el retiro de
enlaces que hayan sido establecidos en sitios web sin su expresa y previa
autorización. El establecimiento en páginas no controladas por Alquería de
enlaces a subdirectorios dentro de el URL </span><span style='color:#96928F'><a
href="http://www.alqueria.com.co"><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>http://www.alqueria.com.co</span></a></span><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>,
(&quot;Deeplinking&quot;) queda prohibido. El despliegue, uso, exhibición,
copia o cualquier otra forma de reproducción de los sitios </span><span
style='color:#96928F'><a href="http://www.alqueria.com.co"><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>http://www.alqueria.com.co</span></a></span><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>, o de
cualquiera de sus subdirectorios y/o páginas, en sitios no controlados por <span
class=GramE>Alquería(</span>&quot;Framing&quot;) queda expresamente prohibido.
La inobservancia de estas prohibiciones será una violación a los derechos de
propiedad intelectual sobre los contenidos y a los derechos sobre la Propiedad
Industrial.</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'>&nbsp;</span><span
style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:14.4pt'><strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>5.
Consultas.</span></strong><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>a.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Cualquier pregunta relacionada con el uso
de los contenidos o con el establecimiento de links hacia los URL </span><span
style='color:#96928F'><a href="http://www.alqueria.com.co"><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>http://www.alqueria.com.co</span></a></span><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>,
podrá dirigirse a Alquería en Bogotá: 4119200 o a través del email&nbsp;</span><span
style='color:#96928F'><a href="mailto:redessociales@alqueriacontigo.com"><span
style='font-size:11.5pt;color:#96928F'>redessociales@alqueriacontigo.com</span></a></span><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>. Su
uso, en contravención de lo aquí dispuesto, dará lugar a las acciones civiles y
penales correspondientes.</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'>&nbsp;</span><span
style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:14.4pt'><strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>6.
Exoneración y Garantías.</span></strong><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>a.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>De conformidad con la Ley de la República
de Colombia, el material contenido en este sitio, incluyendo sin limitación,
textos, gráficos y vínculos (links), son suministrados sobre la base de
&quot;tal como es&quot;, sin haber de por medio garantías de ningún tipo, ya
sea expresas o implícitas, incluyendo, mas no limitado a ellas, garantías de
comerciabilidad, adecuación para un propósito particular, no contravención u
otra violación de derechos. Alquería no garantiza el uso, validez, precisión o
confiabilidad del material en este sitio, o los resultados de su uso o
cualquier otra situación con respecto a dicho material. Alquería se reserva el
derecho de corregir cualquier error, omisión o inexactitud, cambiar o
actualizar la misma en cualquier momento y sin previo aviso.</span><span
style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'><br>
<strong><span style='font-family:"Verdana","sans-serif"'>b.</span></strong><span
class=apple-converted-space>&nbsp;</span>Responsabilidad limitada. Sin
perjuicio de lo determinado en las normas imperativas de la legislación
colombiana aplicable, Alquería no asume responsabilidad alguna, incluyendo
cualquier responsabilidad por cualquier daño o perjuicio, incluyendo, mas no
limitado a, pérdida de información o utilidades, existencia de virus,
resultados del uso o la incapacidad para usar el material en este sitio,
oportunidades de negocios perdidas, o cualquier otro daño. Bajo los términos
aquí señalados, Alquería no asume ninguna responsabilidad por la información
que se suministra en la página, incluyendo, pero sin limitarse a, la referente
a productos, notas de interés, opiniones, consejos prácticos y solución de
inquietudes. Alquería no asume responsabilidad alguna por problemas o por la
imposibilidad de utilización del sitio o de alguna de las páginas que lo
conforman, incluyendo pero sin limitarse a eventos tales como problemas en el
servidor o en la conexión, interrupciones en su comunicación, problemas
técnicos. En caso de que el uso del material de este sitio arroje como
resultado la necesidad de dar servicio, reparar o corregir equipo o información,
el usuario asume cualquier costo derivado de ello.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:14.4pt'><strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>7.
Revisión de los términos.</span></strong><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>a.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Alquería puede, en cualquier momento,
revisar los términos de uso aquí contenidos, por medio de la actualización de este
anuncio. Al usar la página web de Alquería, el usuario conviene en darse por
obligado por cualquiera de tales revisiones, las cuales estarán vigentes a
partir del momento en que las mismas sean accesibles a los usuarios, debiendo
entonces visitar periódicamente esta página para determinar los términos
vigentes en el momento, a los cuales el usuario estará obligado en el evento
que decida utilizar este sitio.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>b.</span></strong><span
class=apple-converted-space><b><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></b></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Legislación aplicable y jurisdicción
Estos términos y condiciones de venta se regirán e interpretarán de acuerdo con
las leyes de la república de Colombia. Cualquier controversia que derive de
este documento se someterá a los jueces competentes de acuerdo con la
legislación colombiana, y tanto Alquería como el cliente renuncian expresamente
a cualquier otro fuero que pudiera corresponderles en razón de su domicilio
presente o futuro.</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'><br>
<strong><span style='font-family:"Verdana","sans-serif"'>c.</span></strong><span
class=apple-converted-space>&nbsp;</span>Independencia de disposiciones. En
caso de que una o más de las disposiciones contenidas en este documento sean
consideradas nulas, ilegales o ineficaces en cualquier aspecto, la validez,
legalidad y exigibilidad o eficacia del resto de las disposiciones del presente
documento no se verán afectadas o anuladas por dicha circunstancia.</span><span
style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><span style='font-size:9.0pt;
font-family:"Verdana","sans-serif";color:#96928F'>&nbsp;</span><span
style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>d.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Políticas de uso de base de datos. Este sitio
se preocupa por la protección de los datos de carácter personal. Esa es la
razón por la que, cuando visita </span><span style='color:#96928F'><a
href="http://www.alqueria.com.co"><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>www.alqueria.com.co</span></a></span><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>,
nosotros le ayudamos a mantener el control sobre sus datos personales en Internet.
A continuación describimos los lineamientos que utilizamos para proteger la
información que el usuario nos proporciona durante su visita a nuestro sitio
web.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>(i)</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>&nbsp;Información personal: En algunas
partes de este sitio le pedimos que nos proporcione información que nos permita
mejorar la visita al sitio, y nos permita realizar actividades promocionales, o
dar seguimiento a las inquietudes tras la visita.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>(ii)</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>LA PARTICIPACIÓN ES TOTALMENTE OPCIONAL.
Es posible que le pidamos su nombre, dirección de correo electrónico, número de
teléfono, domicilio, tipo de empresa, información de su preferencia, tarjeta de
crédito, cuenta bancaria, número de identificación y número de servicio, así
como también otra información personal similar que se necesita para registrarlo
o suscribirlo a actividades promocionales. Si alguna vez llegamos a requerir
información considerablemente diferente, se lo informaremos.&nbsp;<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>(iii)</span></strong><span
class=apple-converted-space><b><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></b></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Cuando el usuario proporciona información
personal a Alquería, no la daremos o venderemos a ninguna compañía externa para
su uso con fines de mercadotecnia u ofertas de servicios. Mantendremos la
confidencialidad de la información personal y sólo se utilizará por parte de
Alquería para fomentar la relación con nosotros.&nbsp;<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>(iv)</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>En el caso de boletines o listas de
correspondencia, o cualquier actividad publicitaria o promocional, nuestra
intención es enviar mensajes de correo electrónico sólo a clientes que creamos
han elegido recibir estos mensajes. En cualquier momento, el usuario tiene el
derecho de optar por no recibir comunicación de Alquería en el futuro,
procediendo a la cancelación de su suscripción a estos servicios, a través de
nuestro sitio, o del sitio web que se informe.&nbsp;<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>(v)</span></strong><span
class=apple-converted-space><b><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></b></span><span class=GramE><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>Cookies
.</span></span><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'> Las cookies son pequeños archivos de datos que los sitios web
como Alquería o los correos electrónicos guardan en su buscador y opcionalmente
en el disco duro. Las cookies nos permiten &quot;recordar&quot; la información
sobre sus preferencias y sesión, y a usted le permite desplazarse dentro de las
áreas de nuestros sitios web sin volver a introducir sus datos. Esto hace
posible crear una experiencia de conocimiento del sitio web más personalizada y
cómoda. Alquería usa un tercero para colocar cookies en su computador, para
recopilar información que no sea personal e identificable. A pesar de que las
cookies contienen un número único de usuario, no recopilan ni guardan ninguna
información personal identificable.&nbsp;<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>e.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Impedimento de uso. Alquería puede, a su
exclusivo criterio impedir que utilice el sitio web de Alquería en cualquier
momento. Usted es personalmente responsable de todo pedido que realice o de los
cargos en los que incurra antes de que se le cancele el uso del sitio. Alquería
se reserva el derecho de cambiar, suspender o descontinuar todo y cualquier
elemento del sitio web de Alquería en cualquier momento y sin aviso previo.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>f.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Responsabilidad del usuario. Al ingresar
a este sitio, el usuario adquiere el compromiso de suministrar información
personal correcta y verdadera, así como proceder a la actualización de sus
datos cada vez que se requiera<span class=GramE>..</span> Se le prohíbe al
usuario poner en, o transmitir a, o desde este sitio cualquier material ilegal,
amenazador, calumniador, difamatorio, obsceno, escandaloso, pornográfico o
profano, o cualquier otro material que pudiera dar lugar a cualquier
responsabilidad civil o penal en los términos de la ley. Usted acepta no
utilizar ningún dispositivo, software, rutina ni datos para obstruir o intentar
obstruir el funcionamiento correcto del sitio web de Alquería o toda actividad
realizada en este sitio. Además, acepta no utilizar ni intentar utilizar ningún
motor, software, herramienta, agente, datos u otro dispositivo o mecanismo
(incluidos, entre otros, exploradores, arañas, robots, personajes digitales o
agentes inteligentes) para navegar o buscar en el sitio web de Alquería que no
sea el motor de búsqueda o los agentes de búsqueda brindados por Alquería o los
exploradores en general disponibles para el público. Información recopilada por
Alquería, puede incluir información personal, como: 1. información de contacto,
incluyendo nombre, dirección postal, número de teléfono y dirección de e-mail.
También podremos recopilar información demográfica, incluyendo edad, intereses
personales y preferencias de productos. Por ejemplo cuando, participe en un
sorteo, promoción o encuesta o se comunique con nosotros por preguntas o
inquietudes.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>g.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Usos de su información personal. Usamos
la información personal que usted nos suministra para propósitos que incluyen,
pero no se limitan a: 1. Brindar servicio al cliente. 2. Administrar sorteos,
promociones o encuestas. 3. Ofrecer nuevos productos. 4. Mejorar la efectividad
de nuestros sitios web, de nuestros esfuerzos de comercialización y nuestros
servicios y ofertas. 5. Llevar a cabo investigaciones y análisis. 6. Enviar
comunicaciones de comercialización. 7. Realizar otras actividades comerciales
según sea necesario o según se describa en otras partes dentro de esta
política. 8. Información personal &nbsp;pública proporcionada a Alquería por
terceros 9. Información de o sobre amigos y familia proporcionada por los
usuarios 10. Si usted nos proporciona información de otras personas, o si otras
personas nos dan información sobre usted, solamente utilizaremos dicha
información para la razón específica para la que se suministró. 11. Compartir
información personal. Alquería no vende ni alquila su información personal a
terceros. Podemos tener la necesidad de compartir su información personal con
terceros tales como nuestros agentes, proveedores de servicios y otros
representantes que actúan en nuestro nombre, con fines limitados. Por ejemplo,
podemos compartir información personal con terceros que realizan servicios en
nombre nuestro.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>h.</span></strong><span
class=apple-converted-space><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>De vez en cuando es posible que se nos
requiera compartir información personal como respuesta a una orden judicial
válida, a una investigación gubernamental o de otra manera requerida por la
ley. También nos reservamos el derecho de informar a las agencias policíacas
las actividades que de buena fe creamos ser ilegales. Compartiremos cierta
información personal cuando consideremos que tal divulgación sea razonablemente
necesaria para proteger los derechos, la propiedad y la seguridad de otros y de
nosotros mismos. Es posible que también transfiramos información personal en
caso de una venta corporativa, fusión, adquisición, disolución o evento
similar. Toda información que le identifique personalmente y que presente en un
tablero de anuncios, blog, chat, comentario sobre un producto, u otro foro
público en este sitio web, puede ser leída, recopilada o utilizada por otros
usuarios de estos foros y podría ser usada para enviarle mensajes no
solicitados. Le recomendamos que no dé su nombre, correo electrónico u otra
información personal. Observe que si elige hacerlo, es bajo su propio riesgo y
acepta la total responsabilidad de dichos comentarios y las consecuencias que
puedan surgir de publicar dicha información. En ocasiones, podremos asociarnos
con un tercero para crear un sitio que puede parecer como si estuviera en una
página de Alquería Estos sitios pueden compartir marca o bien, dar la
apariencia y la sensación de estar en nuestro sitio. Sin embargo, la
información recopilada en estas páginas puede no estar cubierta por la política
de privacidad de Alquería. Informaremos de estos sitios y publicaremos la
política de privacidad que sea aplicable a la información recopilada en estas
páginas.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>i.&nbsp;</span></strong><span
style='font-size:9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>Exoneración
de Responsabilidad Siempre que no se haya notificado oportunamente por parte
del usuario la existencia de la violación de su información personal, o cuando
el usuario no haya procedido a notificar a las correspondientes entidades
financieras o cooperativas de la pérdida, uso indebido, sustracción o hurto de
los instrumentos conferidos por éstas para realizar transacciones, o cuando se
realice un uso indebido de sus datos de registro, Alquería no asume ninguna
responsabilidad por tales acciones. Elecciones respecto a su información
personal Alquería se comunica con sus clientes a través de múltiples canales y
medios para mantenerlos actualizado en temas como promociones, ofertas,
contenido de la experiencia del cliente y productos nuevos. Una vez que usted
proporciona su información personal, podemos ponernos en contacto con usted por
correo directo, correo electrónico o teléfono. Podremos seguir comunicándonos
por medio de estos canales a no ser que usted solicite dejar de recibir estas
comunicaciones optando por ser excluido. Antes de enviarle publicidad móvil
(por ejemplo mensajes de texto), le pediremos su consentimiento expreso
solicitándole su participación.<br>
&nbsp;</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal style='line-height:14.4pt'><strong><span style='font-size:
9.0pt;font-family:"Verdana","sans-serif";color:#96928F'>j.</span></strong><span
class=apple-converted-space><b><span style='font-size:9.0pt;font-family:"Verdana","sans-serif";
color:#96928F'>&nbsp;</span></b></span><span style='font-size:9.0pt;font-family:
"Verdana","sans-serif";color:#96928F'>Revisión de los términos. Alquería puede,
en cualquier momento, revisar los términos de uso aquí contenidos, por medio de
la actualización de este anuncio. Al usar el sitio web de Alquería, el usuario
conviene en darse por obligado por cualquiera de tales revisiones, las cuales
estarán vigentes a partir del momento en que las mismas sean accesibles a los
usuarios, debiendo entonces visitar periódicamente esta página para determinar
los términos vigentes en el momento, a los cuales el usuario estará obligado en
el evento que decida utilizar este portal.</span><span style='color:#96928F'><o:p></o:p></span></p>

<p class=MsoNormal><span style='color:#96928F'>&nbsp;<o:p></o:p></span></p>

</div>

</body>

</html>
