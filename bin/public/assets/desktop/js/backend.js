// Ajax csrf token secure token
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function(){

    $('.validate-alpha').keypress(function (e) {
        var regex = new RegExp("^[A-za-zÁÉÍÓÚáéíóúñÑäëïöüÄËÏÖÜ\s\b_ ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });


    $('.validate-phone').keypress(function (e) {

        if( $(this).val().length === 10 ){
            e.preventDefault();
            return false;
        }

        var regex = new RegExp("^[0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });

    $('.validate-username').keypress(function (e) {

        if( $(this).val().length === 10 ){
            e.preventDefault();
            return false;
        }

        var regex = new RegExp("^[A-za-z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });

    $('.validate-code-hour').keypress(function (e) {

        console.log( $(this).val().length );

        if( $(this).val().length === 8 ){
            e.preventDefault();
            return false;
        }

        if( $(this).val().length == 2  || $(this).val().length == 5 ){
            $(this).val($(this).val() + ':' );
        }

        var regex = new RegExp("^[0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });

    $("#ajaxTestSaveGame").submit(function(e){

        e.preventDefault();
        var form = $(this);

        $.ajax({

            url  : $(this).attr('action'),
            data : form.serialize(),
            type : 'post',
            success:function(result){
                console.log(result);
            }
        });
    });

    $(".sbmt-form").click(function(e){

        console.log("trigger form submit");

        e.preventDefault();

        $("#" + $(this).attr('form-target') ).submit();
    });
});


$.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

function validEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

$(function(){
    $(".make-datepicker").datepicker({
        format: "yyyy-mm-dd",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
});