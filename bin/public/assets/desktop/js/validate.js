// Ajax csrf token secure token
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).ready(function(){

    // Register Form Submit pre validate
    $("#registerForm").submit(function(e){
        e.preventDefault();
        var form = $(this);
        var form_error = false;


        if( !$("#terms1").is(':checked') ){
            showMobileModal('Debes aceptar términos y condiciones.');
            return false;
        }

        if( !$("#terms2").is(':checked') ){
            showMobileModal('Debes marcar que eres mayor de edad o que tienes la autorización de tus padres para participar.');
            return false;
        }

        if( validEmail( $("#email").val() ) === false ){
            showMobileModal('Escribe una dirección de email valida');
             $("#email").focus();
             return false;
        }

        $("input[required]").each(function(){

            if( $(this).val() == '' ){
                showMobileModal('Todos los campos son obligatorios');
                return false;
            }
        });

        if( form_error === true ){
            return false;
        }

        $("#registerForm button[type=submit]").attr('disabled','disabled');

        $("#userNameError, #emailError").addClass('hidden');

        $.ajax({
            url  : form.attr('action'),
            data : form.serialize(),
            type : 'post',
            dataType : 'json',
            success : function(result){

                $("#registerForm button[type=submit]").removeAttr('disabled');

                if( result.success === false ){

                    switch( result.error ){
                        case 'unavailable-username':

                            $("#userNameError").removeClass('hidden');
                            $("#username").focus();

                            break;

                        case 'unavailable-email':

                            $("#emailError").removeClass('hidden');
                            $("#email").focus();

                            break;

                        case 'unknown':
                            alert(result.msg);
                            break;

                        case 'invalid-birthdate':{
                            showMobileModal( result.msg );
                        }
                    }

                    return false;
                }

                window.location.href = result.game_uri;
            }
        });
    }); // eof

    // birthdate secure trigger
    $("#birthdateWrapper").click(function(e){
        e.preventDefault();
        console.log("jojo");
        $("#birthdate").focus();
    })

    // submit code form pre submit validation
    $("#submitCodeForm").submit(function(e){

        if( $(this).hasClass('ready-to-send') ){
            return true;
        }

        var form_error = false;

        $("input[required]").each(function(){

            if( $(this).val() == '' ){
                showMobileModal('Por favor completa todos los campos');
                form_error = true;
                return false;
            }
        });

        if( form_error === true ){

            e.preventDefault();
            return false;
        }

        showMobileModal( 'Para reclamar tus premios debes presentar los empaques con los códigos utilizados durante el juego' );
        e.preventDefault();
    });

    $("#sendSubmitCodeForm").click(function(){
        $("#submitCodeForm").addClass('ready-to-send').submit();
    });

    $("#recoverPassForm").submit(function(e){
        e.preventDefault();

        $("#recoverPassForm input[type=submit]").attr('disabled','disabled');
        $("#recoverPassForm input[type=text]").attr('readonly','readonly');
        $("#emailNotFound").text('');

        var form = $(this);

        $.ajax({

            url : $(this).attr('action'),
            data: form.serialize(),
            type : 'post',
            dataType : 'json',
            success:function(result){

                console.log(result);
                if( result.success === false ){
                    $("#emailNotFound").text(result.feedback);
                } else if( result.success === true ){

                    $("#vista1").hide();
                    $("#vista2").show();
                    $("#recoverPassFeddback").text( result.feedback );
                }

                $("#recoverPassForm input[type=submit]").removeAttr('disabled');
                $("#recoverPassForm input[type=text]").removeAttr('readonly');
            }
        });
    }); // EOF

    $("#selectPrizeForm").submit(function(e){

        var form_error = false;

        $("#selectPrizeForm input[required]").each(function(){

            if( $(this).val() == '' ){
                showMobileModal('Por favor completa todos los campos');
                form_error = true;
                return false;
            }
        });

        if( form_error === true ){
            e.preventDefault();
        }
    });

    // shows modal on contact form
    if( $("#showContactFeedback").val() == 1 ){
        console.log("show contact modal");
        showMobileModal();
    }

}); // dready end

/*
 * Triggers modal box on mobile view
 * @param text: String, Text Showed by the modal
 * @param logo: Boolean, Shows or hides the modal logo
 */
function showMobileModal( text, logo ){


    if( typeof text != 'undefined' ){

        $("#modalTxt").text( text );
    }
    $("#al_alertas2").css("display","block");
    $("body").css("overflow-y","hidden");
}