<html>

<head>

<meta charset="utf-8">

<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536870145 1073786111 1 0 415 0;}
@font-face
	{font-family:"Segoe UI";
	panose-1:2 11 5 2 4 2 4 2 2 3;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520084737 -1073683329 41 0 479 0;}
@font-face
	{font-family:"Century Gothic";
	panose-1:2 11 5 2 2 2 2 2 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:647 0 0 0 159 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Texto comentario Car";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
a:link, span.MsoHyperlink
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:#0563C1;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:#954F72;
	text-decoration:underline;
	text-underline:single;}
p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Asunto del comentario Car";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"Segoe UI","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
	{mso-style-priority:1;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
span.TextocomentarioCar
	{mso-style-name:"Texto comentario Car";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Texto comentario";
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-hansi-font-family:Calibri;}
span.AsuntodelcomentarioCar
	{mso-style-name:"Asunto del comentario Car";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Asunto del comentario";
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-hansi-font-family:Calibri;
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Texto de globo";
	font-family:"Segoe UI","sans-serif";
	mso-ascii-font-family:"Segoe UI";
	mso-hansi-font-family:"Segoe UI";
	mso-bidi-font-family:"Segoe UI";}
p.msolistparagraphcxspfirst, li.msolistparagraphcxspfirst, div.msolistparagraphcxspfirst
	{mso-style-name:msolistparagraphcxspfirst;
	mso-style-unhide:no;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
p.msolistparagraphcxspmiddle, li.msolistparagraphcxspmiddle, div.msolistparagraphcxspmiddle
	{mso-style-name:msolistparagraphcxspmiddle;
	mso-style-unhide:no;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
p.msolistparagraphcxsplast, li.msolistparagraphcxsplast, div.msolistparagraphcxsplast
	{mso-style-name:msolistparagraphcxsplast;
	mso-style-unhide:no;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
p.msochpdefault, li.msochpdefault, div.msochpdefault
	{mso-style-name:msochpdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Calibri","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";}
p.msopapdefault, li.msopapdefault, div.msopapdefault
	{mso-style-name:msopapdefault;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:105%;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-hansi-font-family:Calibri;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:70.85pt 3.0cm 70.85pt 3.0cm;
	mso-header-margin:35.4pt;
	mso-footer-margin:35.4pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Tabla normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=ES-CO link="#0563C1" vlink="#954F72" style='tab-interval:35.4pt'>

<div class=WordSection1>

<p class=MsoNoSpacing style='text-align:justify;line-height:115%'><b><span
style='color:#B6B2B1'>LA MECÁNICA DE LA PROMOCIÓN CONSISTE EN:</span></b><span
style='color:#B6B2B1'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify'><span style='color:#B6B2B1'>&nbsp;<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>1.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='color:#B6B2B1'>Mecánica General:</span></b><span
style='color:#B6B2B1'> La mecánica general del juego es la siguiente.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>a.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>El usuario ingresa a 
<a target="_blank" href="http://www.chocoleche.com.co"><span style='color:#B6B2B1;text-decoration:
underline;text-underline:none'>www.chocoleche.com.co</span></a> &nbsp;y registra sus
datos personales.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>b.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>El usuario selecciona el combo por el cual
quiere participar.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>c.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Para empezar a participar el usuario debe
ingresar un código (el cual se encontrará en los empaques de <span
class=SpellE>Chocoleche</span> &nbsp;y corresponde a la fecha de vencimiento y
lote) para obtener 80 oportunidades para disparar. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>d.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Mientras más códigos registre, &nbsp;mayores
oportunidades de disparo tiene el usuario y así podrá acumular más puntos.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>e.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Cada semana se premiará a los 2 usuarios con
el mayor puntaje obtenido durante esa semana. El puntaje será visualizado en un
ranking. Cada semana se reinicia el ranking y los puntos obtenidos. Por lo
que&nbsp; al inicio de cada semana los usuarios arrancarán con cero puntos.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>f.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Una vez un usuario es ganador y cumple con
los requisitos de entrega, quedará inhabilitado para ganar nuevamente.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>2.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='color:#B6B2B1'>Registro:</span></b><span
style='color:#B6B2B1'> El usuario deberá llenar los siguientes campos para
poder registrarse. Todos los datos son obligatorios<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>a.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Nombre<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>b.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Apellido<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>c.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Fecha de nacimiento (día/mes/año)<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>d.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Correo electrónico de los padres o acudiente<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>e.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span class=SpellE><span style='color:#B6B2B1'>Telefono</span></span><span
style='color:#B6B2B1'> y/o celular de los padres o acudiente<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>f.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Nombre de usuario<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>g.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Confirmar que los padres o acudiente saben
que el usuario está participando en la actividad.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>h.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Aceptar la política de términos y
condiciones de la actividad. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal'><span
style='color:#B6B2B1'>&nbsp;<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
normal'><span style='color:#B6B2B1'>3.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='color:#B6B2B1'>Selección de “Combo”: </span></b><span
style='color:#B6B2B1'>El usuario encontrara en la plataforma 3 categorías las
cuales son: <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify'><b><span
style='color:#B6B2B1'>1.</span></b><span style='color:#B6B2B1'> Tecnología en
donde se encontraran los siguientes elementos: <span class=SpellE>Iphone</span>
5s, computador Acer mini e3-112, televisor Samsung un32j4300 <span
class=SpellE>ak</span> (<span class=SpellE>smart</span> tv), Apple <span
class=SpellE>Ipad</span> mini 16 <span class=SpellE>gb</span> <span
class=SpellE>wi</span>-fi md 531, y juego consola <span class=SpellE>wii</span>
u 32gb con control + juego</span><span style='font-family:"Century Gothic","sans-serif";
color:#B6B2B1'>.</span><span style='color:#B6B2B1'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify'><b><span
style='color:#B6B2B1'>2.</span></b><span style='color:#B6B2B1'> Música en donde
se encontraran los siguientes elementos: <span class=SpellE>Bateria</span> <span
class=SpellE>drum</span> 22 jbp1103 <span class=SpellE>tom</span> <span
class=SpellE>grasso</span>, guitarra <span class=SpellE>electrica</span> fst-122
<span class=SpellE>tom</span> <span class=SpellE>grasso</span> + estuche p/<span
class=SpellE>guit</span>. <span class=SpellE><span class=GramE>elect</span></span>.
forma/gota <span class=SpellE>adc</span> <span class=SpellE>nal</span> + <span
class=SpellE>amplif</span> guitar ph1012 10w qx12 <span class=SpellE>kustom</span>,
<span class=SpellE>ipod</span> nano 16gb, <span class=SpellE>audifono</span> <span
class=SpellE>sony</span> color negro manos libres peso: 125 g (sin cable) /
capacidad: 1000 <span class=SpellE>mw</span> / respuesta en frecuencia (<span
class=SpellE>hz</span>): 10–24.000 <span class=SpellE>hz</span> / longitud del
cable: 1,2 m / enchufe: <span class=SpellE>minienchufe</span> estéreo en forma
de l y bañado en <span class=SpellE>oromini</span> estéreo de cuatro
conductores bañado en oro con forma de l / <span class=SpellE>garantia</span> 1
año directamente con centro de servicio ,y parlante x300 <span class=SpellE>wireless</span>
<span class=SpellE>mobile</span> speaker (<span class=SpellE>black</span> <span
class=SpellE>housing</span>/<span class=SpellE>brown</span>); marca: Logitech.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify'><b><span
style='color:#B6B2B1'>3.</span></b><span style='color:#B6B2B1'> Aventura en
donde se encontraran los siguientes elementos: pistola <span class=SpellE>nerf</span>
<span class=SpellE>rebelle</span> &nbsp;para niña y <span class=SpellE>hasbro</span>
&nbsp;<span class=SpellE>nerf</span> - <span class=SpellE>soa</span> - <span
class=SpellE>flashflood</span> 2 para niño, carpa iglú dome 4 personas <span
class=SpellE>klimber</span> + sleeping bag con cabezal 180 x 75 cm colores
surtidos <span class=SpellE>klimber</span> + colchoneta enrollar impermeable,
cancha de <span class=SpellE>basketball</span> elaborada en metal y plástico,
bicicleta montaña juvenil-adulto rin 26 y patines en línea <span class=SpellE>freestyle</span>
con chasis corto.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span
style='color:#B6B2B1'>&nbsp;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span style='color:#B6B2B1'>El usuario podrá
únicamente elegir &nbsp;un (1) elemento de las categorías señaladas &nbsp;y así
crear el “combo”&nbsp; que desea. Para lograr ser un posible ganador el usuario
debe registrar códigos de <span class=SpellE>Chocoleche</span> durante la
vigencia de la promoción y acumular la mayor cantidad de puntos. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span style='color:#B6B2B1'>&nbsp;<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>a.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Cambio de combo: El usuario solo podrá
cambiar su combo en el&nbsp; siguiente&nbsp; evento:<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;line-height:
normal'><span style='color:#B6B2B1'>&nbsp;<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:90.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-90.0pt;line-height:normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>i.</span><span style='font-size:7.0pt;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Se reinicia el juego: Cada semana que se
reinicia el puntaje del juego, el usuario podrá decidir si quiere cambiar su
combo o seguir participando por el mismo.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:81.0pt;margin-bottom:.0001pt;line-height:normal'><span
style='color:#B6B2B1'>&nbsp;<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>4.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='color:#B6B2B1'>Ingreso de códigos:</span></b><span
style='color:#B6B2B1'> Los códigos son información que se encuentra en el
empaque &nbsp;del producto y corresponde a la siguiente información: día, mes,
año, lote &nbsp;(fecha de vencimiento y lote). Por cada código ingresado el
usuario obtendrá 80 oportunidades para disparar. Al finalizar cada oportunidad
de disparo el usuario podrá ingresar todos los códigos que desee con la
finalidad de acumular el mayor puntaje y estar en el ranking de los 2 mejores
de la semana. El usuario debe guardar los empaques de los códigos que ingresa
para poder reclamar su combo en caso de ser un posible ganador.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>5.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='color:#B6B2B1'>Modo de juego:</span></b><span
style='color:#B6B2B1'> El juego consiste en obtener la mayor cantidad de puntos
disparando un chorro de <span class=SpellE>Chocoleche</span> a los blancos que
aparecen en la pantalla. Los dos usuarios con la mayor cantidad de puntos en la
semana serán los ganadores y podrán reclamar su combo siempre y cuando cumplan
con las condiciones y restricciones de la promoción. A continuación hay un
detalle de las variables que tiene el juego:<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:72.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>a.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Puntos: Los puntos son otorgados al pegarle
a un blanco. Los blancos tienen un puntaje mínimo de 200&nbsp; y un puntaje
máximo de 700 (Golpe Perfecto) y el puntaje obtenido es definido según qué tan
cerca el usuario le pega al centro del blanco. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:72.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>b.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Puntos Extras: En cada sesión de juego
aparecerán dos blancos con bonificaciones. Los tipos de bonificaciones son: 5
disparos extras o 600 puntos extras; los cuales se otorgan si le pega al centro
del blanco.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>6.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='color:#B6B2B1'>Finalización Sesión Juegos:</span></b><span
style='color:#B6B2B1'> Cuando un usuario se queda sin disparos se acaba la
sesión del código ingresado. Para poder seguir jugando, el usuario deberá
ingresar otro código nuevo. Al ingresar otro código se iniciará otra partida y
podrá seguir aumentando su puntaje.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>7.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='color:#B6B2B1'>Ganadores:</span></b><span
style='color:#B6B2B1'> Los posibles ganadores son los dos usuarios que en la
semana hayan logrado la mayor cantidad de puntos. Estos usuarios serán
contactados a través del e-mail y/o teléfono suyos o de sus padres (en caso de
ser menores de edad), para verificar que cumplan con las condiciones y
restricciones de la promoción. Si los posibles ganadores cumplen &nbsp;todos y
cada uno de los requisitos, se programa la entrega de los premios, de lo
contrario se acumula la cantidad de posibles ganadores para la semana
siguiente. Los usuarios deberán presentar los empaques del producto para
confirmar los códigos usados. Los usuarios que han ganado un combo <span
class=GramE>queda</span> inhabilitados para ganar pero pueden jugar si lo
desean. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>8.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='color:#B6B2B1'>Descalificación:</span></b><span
style='color:#B6B2B1'> Se descalificará a un usuario si:<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>a.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Utiliza un <span class=SpellE>nickname</span>
no apropiado (incluyan groserías, connotaciones racistas, sexuales,
discriminatorias y cualquier otra expresión que sea ofensiva o inadecuada)<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>b.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>&nbsp;Utiliza códigos inválidos o no tiene
el empaque al presentarlos<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>c.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Se comprueba un intento de fraude o <span
class=SpellE>hackeo</span> del juego.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-18.0pt;line-height:normal'><span style='color:#B6B2B1'>d.</span><span
style='font-size:7.0pt;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Se sospecha el uso de algún método de
participación en la actividad que se realice por cualquier proceso, técnica o
mecánica de participación distinta a la detallada en los términos y
condiciones. La utilización de técnicas de participación en la actividad de
naturaleza robótica, repetitiva, automática, programada, mecanizada, mediante
software, sistemas especiales, y otros dispositivos no autorizados, o cualquier
técnica similar llevará a la anulación de la participación de que se trate y
será causal de&nbsp; descalificación total y permanente del participante.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
style='color:#B6B2B1'><br>
<b>CONDICIONES PARA PARTICIPAR:</b><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.75pt;vertical-align:baseline'><span
style='color:#B6B2B1'>Para participar en la promoción “MEZCLA LO QUE MAS TE
GUSTA Y CHOCOLECHE TE LO REGALA” &nbsp;el participante debe:<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-bottom:3.75pt;text-indent:-18.0pt;
vertical-align:baseline'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Tener autorización previa de los padres&nbsp; o tutores
&nbsp;si el usuario es menor de edad.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-bottom:3.75pt;text-indent:-18.0pt;
vertical-align:baseline'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Leer los términos y condiciones de la actividad, en señal
de aceptación de los términos.&nbsp;<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-bottom:3.75pt;text-indent:-18.0pt;
vertical-align:baseline'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Deberá ingresar a la aplicación y seguir los pasos.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-bottom:3.75pt;text-indent:-18.0pt;
vertical-align:baseline'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Guardar los empaques inscritos para reclamar el premio si
llegase a ser ganador.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.75pt;vertical-align:baseline'><span
style='color:#B6B2B1'>&nbsp;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b><span
style='color:#B6B2B1'>TERMINOS Y CONDICIONES GENERALES:</span></b><span
style='color:#B6B2B1'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b><span
style='color:#B6B2B1'>&nbsp;</span></b><span style='color:#B6B2B1'><o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Vigencia de la promoción: Promoción Valida desde el 1 de
Octubre del 2015 hasta el 30 de Noviembre del 2015, y/o hasta agotar existencias.<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Se realizaran premiaciones semanales premiando a los 2
primeros lugares <span class=SpellE>asi</span>:<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Semana 1: 1 de Octubre del 2015 al 11 de Octubre del
2015.<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Premiación Semana 1: 12 de Octubre del 2015 <o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Semana 2: 12 de Octubre al 18 de Octubre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Premiación Semana 2: 19 de Octubre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Semana 3: 19 de Octubre al 25 de Octubre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Premiación Semana 3: 26 de Octubre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Semana 4: 26 de Octubre al 1 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Premiación Semana 4: 2 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Semana 5: 2 de Noviembre al 8 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Premiación Semana 5: 9 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Semana 6: 9 de Noviembre al 15 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Premiación Semana 6: 16 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Semana 7: 16 de Noviembre al 22 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Premiación Semana 7: 23 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Semana 8: 23 de Noviembre al 29 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:72.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span class=GramE><span style='font-family:"Courier New";
color:#B6B2B1'>o</span></span><span style='font-size:7.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Premiación Semana 8: 30 de Noviembre del 2015<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Cobertura: Aplica a nivel nacional (República de
Colombia) en todas las ciudades y municipios donde se comercialice <span
class=SpellE>Chocoleche</span> Alquería. Todas las personas que tengan acceso a
la página www.chocoleche.com.co &nbsp;y que se encuentren en territorio
colombiano podrán participar.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:18.0pt;margin-bottom:.0001pt;text-indent:-18.0pt'><span
style='font-family:Symbol;color:#B6B2B1'>·</span><span style='font-size:7.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>El usuario no podrá ingresar el mismo código
por más de una vez.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt'><span
style='font-family:Symbol;color:#B6B2B1'>·</span><span style='font-size:7.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>&nbsp;El Término de la garantía de los
premios que PRODUCTOS NATURALES DE LA SABANA S.A. entregue&nbsp; será de un día
a partir de la fecha en la que sean recibidos por el ganador.&nbsp; <o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt'><span
style='font-family:Symbol;color:#B6B2B1'>·</span><span style='font-size:7.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='color:#B6B2B1'>Un usuario podrá ganar solo una vez,
quedando inhabilitado para ganar otro premio.<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Los usuarios deberán guardar los empaques, para ser
presentados en el momento en que se haga la entrega de los premios (en el caso
de ser ganadores). La entrega de los premios se hará únicamente dentro del
territorio colombiano, y se hará &nbsp;a un mayor de edad que puede ser
&nbsp;cualquiera de los padres o tutor en caso de ser menores de edad. &nbsp;Si
no se presentan los empaques de <span class=SpellE>Chocoleche</span>, el participante
NO &nbsp;se hará acreedor del premio. Los premios no son canjeables por dinero
ni acumulables con otras promociones.<o:p></o:p></span></p>

<p class=MsoNoSpacing style='margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;line-height:115%'><span style='font-family:Symbol;color:#B6B2B1'>·</span><span
style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:#B6B2B1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style='color:#B6B2B1'>Esta promoción no es válida para colaboradores de PRODUCTOS NATURALES DE LA SABANA S.A., ni para sus familiares hasta el segundo grado de consanguinidad, primero de afinidad y/o primero civil; tampoco es válida para sus cónyuges y/o compañeros/as permanentes<o:p></o:p></span></p>

<p class=MsoNormal><span style='color:#B6B2B1'>&nbsp;<o:p></o:p></span></p>

</div>

</body>

</html>
