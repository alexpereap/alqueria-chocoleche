<html>
<title>Contáctenos</title>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/css_game_choco.css">
    <!--link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css"-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <!--script src="js/jquery.bxslider.js"></script-->
    <script src="js/validate.js"></script>
    <script src="js/jsbasico.js"></script>
    <script src="js/backend.js"></script>
</head>

<body>
    <section id="al_chocoleche">
        <header>
            <ul class="menu_choc">
                <li><a href="">inicio</a></li>
                <li><a href="">¿cómo jugar?</a></li>
                <li><a href="">premios</a></li>
                <li><a href="">ranking</a></li>
                <li><a href="">ganadores semanales</a></li>
                <li><a href="">registro</a></li>
                <li><a href="">contáctenos</a></li>
            </ul>
        </header>

        <div id="al_contenido">
            <div id="logo"><img src="img/logo_choco.png"></div>
            <div class="al_dis_contenido al_contactenos">
                <img src="img/al_conta.png" alt="contacto">
                <p>Responderemos todas tus inquietudes en la mayor brevedad posible. Tus preguntes son muy importantes para nosotros.</p>
                <form>
                    <div id="al_form_choco" class="form-register">
                        <input class="tercio validate-alpha" type="text" name="Nombre" placeholder="Nombre Completo">
                        <input class="tercio validate-alpha" type="text" name="Apellido" placeholder="Correo Electrónico">
                        <input class="tercio validate-phone" type="text" name="Telefono" placeholder="Télefono">
                        <textarea placeholder="Mensaje" value="mensaje"></textarea>
                        <div class="centrado">
                            <input type="submit" id="al_eviar_reg">
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <footer>
            <div class="al_texto">
                <p>© 2015 todos los derechos reservados Productos Naturales de la Sabana S.A. Términos y Condiciones | Habeas Data | </p>
            </div>
            <div class="back_leche">
                <div class="al_logo_al"><img src="img/logo_alqueria_choco.png"></div>

                <div class="al_logo_choco"><img src="img/choco_leche_choco.png"></div>
            </div>
        </footer>

    </section>
</body>

</html>