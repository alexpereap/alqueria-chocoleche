<html>
<title>Registro</title>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/css_game_choco.css">
    <!--link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css"-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <!--script src="js/jquery.bxslider.js"></script-->
    <script src="js/validate.js"></script>
    <script src="js/jsbasico.js"></script>
    <script src="js/backend.js"></script>    
</head>

<body>
    <section id="al_chocoleche">
        <header>
            <ul class="menu_choc">
                <li><a href="">inicio</a></li>
                <li><a href="">¿cómo jugar?</a></li>
                <li><a href="">premios</a></li>
                <li><a href="">ranking</a></li>
                <li><a href="">ganadores semanales</a></li>
                <li><a href="">registro</a></li>
                <li><a href="">contáctenos</a></li>
            </ul>
        </header>
        <div id="al_contenido">

            <div id="logo"><img src="img/logo_choco.png"></div>

            <div class="al_dis_contenido">
                <img src="img/img_regis.png" alt="registro">
                <p>Comienza completando los siguientes datos:</p>
                
                <form id="registerForm">
                    <div id="al_form_choco" class="form-register">
                        <input class="validate-alpha" type="text" name="Nombre" placeholder="Escribe tu nombre">
                        <input class="validate-alpha" type="text" name="Apellido" placeholder="Escribe tu apellido">
                        <input class="validate-email" type="email" name="email" placeholder="Correo electrónico de uno de tus padres">
                        <div class="campo_dividido nor">
                            <input type="text" name="Fecha_de_nacimiento" placeholder="Fecha de nacimiento">
                            <label>DD/MM/AA</label>                            
                        </div>
                        <input class="validate-phone" type="text" name="Teléfono" placeholder="Telefono de uno de tus padres">
                        <input class="validate-username" type="text" name="Usuario" placeholder="Cres un usuario para identificarte">

                        <div class="campo_dividido che">
                            <label>

                                <div class="box-check">
                                    <!-- no change position of input -->
                                    <input type="checkbox" name="terminos" id="terms1">

                                    <div class="box-check-ok">
                                        <span></span>
                                    </div>
                                    <p>He leído y acepto los términos y condiciones de la actividad</p>
                                </div>
                            </label>
                        </div>

                        <div class="campo_dividido che">
                            <label>
                                <div class="box-check">
                                    <!-- no change position of input -->
                                    <input type="checkbox" name="mayor" id="terms2">

                                    <div class="box-check-ok">
                                        <span></span>
                                    </div>
                                    <p>Soy mayor de edad o mis papás saben que estoy participando en este concurso</p>
                                </div>
                            </label>
                        </div>

                        <p class="al_recuerde">Recuerda escribir los datos correctos. En caso de resultar ganador nos pondremos en contacto al correo electrónico de tus papás. *Todos los datos son obligatorios</p>

                        <div class="centrado">
                            <input type="submit" id="al_eviar_reg">
                        </div>
                    </div>
                </form>

                <section id="al_alertas"></section>
            </div>
        </div>
        <footer>

            <div class="al_texto">
                <p>© 2015 todos los derechos reservados Productos Naturales de la Sabana S.A. Términos y Condiciones | Habeas Data | </p>
            </div>
            <div class="back_leche">
                <div class="al_logo_al"><img src="img/logo_alqueria_choco.png"></div>

                <div class="al_logo_choco"><img src="img/choco_leche_choco.png"></div>
            </div>
        </footer>

    </section>

    <section id="al_alertas2">        
        <div class="contenedro_alerta">
            <div id="cerrar"><a id="cerrar_alerta" href="javascript:;">cerrar <span>X</span></a></div>
            <div class="btnface"><a href=""><img src="img/al_facebook.png"></a></div>
            <div class="imagen"><img src="img/al_flag.png"></div>
            <div class="func_text">
                <h2>partida finalizada</h2>
                <p>Recuerda que entre más códigos ingreses, más oportunidad tienes de ganar. Y no olvides que acumulas tus puntos para las siguientes partidas</p>
                <ul class="oppopu">
                    <li><img src="img/al_boton.png">
                        <p>X0 <span>Oportunidades</span></p>
                    </li>
                    <li><img src="img/al_human.png">
                        <p>Posición:<span> 128</span></p>
                    </li>
                    <li><img src="img/al_point.png">
                        <p>1245 <span>puntos</span></p>
                    </li>
                </ul>
                <ul class="savora">
                    <li><a href="">Salir del juego</a></li>
                    <li><a href="">Volver a Jugar</a></li>
                    <li><a href="">Ranking</a></li>
                </ul>
            </div>
        </div>
    </section>

</body>

</html>