<html>
<title>Ranking</title>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/css_game_choco.css">
    <!--link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css"-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <!--script src="js/jquery.bxslider.js"></script-->
    <script src="js/validate.js"></script>
    <script src="js/jsbasico.js"></script>
    <script src="js/backend.js"></script>    
</head>

<body>
    <section id="al_chocoleche">
        <header>
            <ul class="menu_choc">
                <li><a href="">inicio</a></li>
                <li><a href="">¿cómo jugar?</a></li>
                <li><a href="">premios</a></li>
                <li><a href="">ranking</a></li>
                <li><a href="">ganadores semanales</a></li>
                <li><a href="">registro</a></li>
                <li><a href="">contáctenos</a></li>
            </ul>
        </header>
        <div id="al_contenido">

            <div id="logo"><img src="img/logo_choco.png"></div>

            <div class="al_dis_contenido">
                <img src="img/al_ranking.png" alt="contacto">
            </div>
        </div>
        <div class="al_ranking">
            <table>
                <tr id="tab_main">
                    <td></td>
                    <td><strong><img src="img/al_medalla.png">Posición</strong></td>
                    <td><strong><img src="img/al_user.png">Jugador</strong></td>
                    <td><strong><img src="img/al_star.png">Puntos</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td class="tab_lef">1</td>
                    <td class="tab_cen">werty89</td>
                    <td class="tab_rig">3'400.526</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td class="tab_lef">2</td>
                    <td class="tab_cen">calvachi</td>
                    <td class="tab_rig">2'403.526</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td class="tab_lef">3</td>
                    <td class="tab_cen">julian</td>
                    <td class="tab_rig">1'200.526</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td class="tab_lef">4</td>
                    <td class="tab_cen">julian</td>
                    <td class="tab_rig">1'200.526</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td class="tab_lef">5</td>
                    <td class="tab_cen">julian</td>
                    <td class="tab_rig">1'200.526</td>
                    <td></td>
                </tr>

                <tr id="tab_tu">
                    <td></td>
                    <td>128</td>
                    <td>juanito</td>
                    <td>200.526</td>
                    <td></td>
                </tr>

            </table>


        </div>
        <footer>

            <div class="al_texto">
                <p>© 2015 todos los derechos reservados Productos Naturales de la Sabana S.A. Términos y Condiciones | Habeas Data | </p>
            </div>
            <div class="back_leche">
                <div class="al_logo_al"><img src="img/logo_alqueria_choco.png"></div>

                <div class="al_logo_choco"><img src="img/choco_leche_choco.png"></div>
            </div>
        </footer>

    </section>
</body>

</html>