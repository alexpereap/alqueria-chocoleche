<html>
<title>Ingresa tus Códigos</title>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/css_game_choco.css">
    <!--link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css"-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <!--script src="js/jquery.bxslider.js"></script-->
    <script src="js/validate.js"></script>
    <script src="js/jsbasico.js"></script>
    <script src="js/backend.js"></script>
</head>

<body>
    <section id="al_chocoleche">
        <header>
            <ul class="menu_choc">
                <li><a href="">inicio</a></li>
                <li><a href="">¿cómo jugar?</a></li>
                <li><a href="">premios</a></li>
                <li><a href="">ranking</a></li>
                <li><a href="">ganadores semanales</a></li>
                <li><a href="">registro</a></li>
                <li><a href="">contáctenos</a></li>
            </ul>
        </header>
        <div id="al_contenido">

            <div id="logo"><img src="img/logo_choco.png"></div>

            <div class="al_dis_contenido al_codigos">


                <img src="img/al_codes_ti.png" alt="contacto">
                <p>Ingresa tus códigos para comenzar</p>

                <form>
                    <div id="al_form_choco">

                        <div class="al_campo_cod">
                            <div class="nselec">
                                <select name="dia">
                                    <option value="0">Día</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>


                            </div>
                            <div class="nselec">
                                <select name="mes">
                                    <option value="mes">Mes</option>
                                    <option value="Enero">Enero</option>
                                    <option value="Febrero">Febrero</option>
                                    <option value="Marzo">Marzo</option>
                                    <option value="Abril">Abril</option>
                                    <option value="Mayo">Mayo</option>
                                    <option value="Junio">Junio</option>
                                    <option value="Julio">Julio</option>
                                    <option value="Agosto">Agosto</option>
                                    <option value="Septiembre">Septiembre</option>
                                    <option value="Octubre">Octubre</option>
                                    <option value="Noviembre">Noviembre</option>
                                    <option value="Diciembre">Diciembre</option>

                                </select>
                            </div>
                        </div>


                        <div class="al_campo_cod">
                            <input type="text" name="lote" placeholder="Lote">
                            <input type="time" name="hora" placeholder="Hora">
                        </div>

                        <div class="al_campo_cod">
                            <div class="nselec">
                                <select name="anio">
                                    <option value="anio">Año</option>
                                    <option value="2013">2013</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                </select>
                            </div>
                        </div>

                    </div>


                    <div class="der_img"><img src="img/al_codes.png"></div>
                    <div class="centrado">
                        <input type="submit" id="al_eviar_reg">
                    </div>
                </form>
            </div>
        </div>
        <footer>

            <div class="al_texto">
                <p>© 2015 todos los derechos reservados Productos Naturales de la Sabana S.A. Términos y Condiciones | Habeas Data | </p>
            </div>
            <div class="back_leche">
                <div class="al_logo_al"><img src="img/logo_alqueria_choco.png"></div>

                <div class="al_logo_choco"><img src="img/choco_leche_choco.png"></div>
            </div>
        </footer>

    </section>
</body>

</html>