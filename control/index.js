var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('./public'));

app.get('/', function(req, res){
  res.sendfile('./public/index.html');
});

app.get('/cross-domain-comunicator', function(req, res){
  res.sendfile('./public/cross-domain-comunicator.html');
});

app.get('/game', function(req, res){
  res.sendfile('./public/game/index.html');
});

app.get('/game-on', function(req,res){
    res.writeHead(301,{
        Location: 'http://chocoleche.com.co'
    }
    );
    res.end();
});

io.on('connection', function(socket){

    console.log('a user connected');

    socket.on('position', function(data){
        // io.emit('chat message', data_msg );
        console.log(data);
        // socket.broadcast.emit('receiverPosition', data);
        socket.broadcast.to(data.room_id).emit('receiverPosition', data);
    });

    socket.on('action', function(data){
        // io.emit('chat message', data_msg );
        console.log(data);
        // socket.broadcast.emit('receiverAction', data);
        socket.broadcast.to(data.room_id).emit('receiverAction', data);
    });

    socket.on('subscribe', function(room_id){

        console.log('joining room', room_id);
        socket.join(room_id);

        // console.log(io.sockets.adapter.rooms);

      });

    socket.on('loadGameOnDesktop', function(room_id){

        console.log( "emmiting to iframe: " + room_id  );
        socket.broadcast.to( room_id ).emit('loadTheGame', room_id );
    });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});