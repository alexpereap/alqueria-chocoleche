var socket = io();
var room_id = getURLParameter('id');

$(document).ready(function(){
    console.log( 'comunicator url:' + getURLParameter('comunicator_url') );
    $("#comunicationBridge").attr('src', getURLParameter('comunicator_url') );
});

$(window).load(function(){

    setTimeout(function(){
        window.history.pushState(null, "", "/game-on");
    },500);

});


console.log( "The room id is: " + room_id );

socket.emit('subscribe', room_id);

var control_x = 1110;
var control_y = 560;
var action = 'init';


socket.on('receiverPosition', function(data){
    control_x = data.x;
    control_y = data.y;
    // console.log(data);
});

socket.on('receiverAction', function(data){
    action = data.action;
    // console.log(data);
});

function getX(){
    // console.log(control_x);
    return control_x;
}

function getY(){
    // console.log(control_y);
    return control_y;
}

function getAction(){
    // console.log( 'returning action: ' + action );
    return action;
}

function actionRead(){
    action = '';
    // console.log( 'reseting action to: ' + action );
}

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

function gameSave( score, perfect_hits ){

    console.log("trying to save this data: " + score, perfect_hits);
    var pass_data = {
        action       : 'gameSave',
        score        : score,
        perfect_hits : perfect_hits
    };

    iframe = document.getElementById('comunicationBridge');

    iframe.contentWindow.postMessage( JSON.stringify(pass_data), "*");
}