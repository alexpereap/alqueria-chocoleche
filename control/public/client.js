/*
 * Controller client
 * Handles interactions between game controller and server
 */

var socket = io();
var room_id = getURLParameter('id');
console.log( "The room id is: " + room_id );
socket.emit('subscribe', room_id);

socket.emit('loadGameOnDesktop', room_id );


function position(x,y){

    data = {
        x       : x,
        y       : y,
        room_id : room_id
    };

    console.log(data);
    socket.emit( 'position', data  );
}

function action(action){

    data = {
        action  : action,
        room_id : room_id
    };

    console.log(data);
    socket.emit( 'action', data  );
}

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}
